import sbt._
import Keys._
import play.Project._

object ApplicationBuild extends Build {

  val appName = "sunsheriffs"
  val appVersion = "0.31"

  val appDependencies = Seq(
    //group % artifact % revision
    //use double % to ask for compatible version, for special cases where there is a library for each scala version
    //"org.scalatest" %% "scalatest" % "1.8"
    //"net.debasishg" %% "sjson" % "0.17"
    //"com.typesafe.akka" % "akka-remote" % "2.0.2"
    jdbc,
    anorm,
    "mysql" % "mysql-connector-java" % "5.1.25",
    "org.jodd" % "jodd-mail" % "3.4.4",
    "com.googlecode.libphonenumber" % "libphonenumber" % "5.5",
    "com.googlecode.libphonenumber" % "geocoder" % "2.6",
    "com.twilio.sdk" % "twilio-java-sdk" % "3.3.15",
    "com.thoughtworks.xstream" % "xstream" % "1.4.4",
    "jfree" % "jcommon" % "1.0.16",
    "jfree" % "jfreechart" % "1.0.13",

    "org.slf4j" % "slf4j-simple" % "1.6.4",

    //"com.typesafe.akka" %% "akka-actor" % "2.2.1",

    //test
    "com.typesafe.akka" %% "akka-testkit" % "2.2.1" % "test"
  )

  val main = play.Project(appName, appVersion, appDependencies).settings(
    // Add your own project settings here
    scalacOptions ++= Seq("-deprecation", "-unchecked", "-feature", "-language:postfixOps", "-language:implicitConversions")
  )

}
