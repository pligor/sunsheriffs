package models

import java.util.Date

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
case class InstantOutput(t: Date, power: BigDecimal, income: BigDecimal);