package models

import components.{MyModelObject, AutoIncrement, MyModel}
import components.helpers.UrlHelper._
import java.sql.Connection
import anorm.SqlParser._
import java.util.Date
import generic.TimeHelper._
import java.io.{FileOutputStream, File}
import java.awt.BasicStroke
import anorm.Pk
import org.jfree.chart.renderer.xy.{StandardXYItemRenderer, XYAreaRenderer}
import org.jfree.data.time.{TimeSeries, TimeSeriesCollection}
import org.jfree.chart.axis.NumberAxis
import org.jfree.chart.{ChartUtilities, ChartFactory}
import anorm.SQL

object MonitoringSystem extends AutoIncrement {
  val anormParser = get[Pk[Long]]("id") ~ get[String]("serverUrl") ~ get[String]("password") ~ get[Date]("timeRegistered");

  /*val parser = anormParser.map {
    //partial function
    case parsedId ~ parsedServerUrl ~ parsedPassword => new MonitoringSystem(parsedServerUrl, parsedPassword, parsedId.toOption)
  }*/
  private val childrenObjects = Seq[MyModelObject](Webbox, Solarlog);

  def getById(id: Long)(implicit connection: Connection): Option[MonitoringSystem] = {
    //val webbox = Webbox.getById(id);

    val childObjectOption = childrenObjects.find(childObject => childObject.getById(id).isDefined);
    if (childObjectOption.isDefined) {
      val childObject = childObjectOption.get;
      childObject.getById(id) match {
        case Some(x: MonitoringSystem) => Some(x);
        case None => None;
        case _ => throw new ClassCastException("check to see if all of the childrenObjects are actually referring to monitoring systems");
      }
    }
    else {
      //none was found!
      None;
    }
  }
}

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
abstract class MonitoringSystem(
                                 val serverUrl: String,
                                 val password: String,
                                 val timeRegistered: Date,
                                 id: Option[Long] = None
                                 ) extends MyModel(id) {
  require(isValidUrl(serverUrl))

  def deleteData(fromTime: Date, untilTime: Date)(implicit connection: Connection): Boolean

  def getTotalEnergy(implicit connection: Connection): BigDecimal

  def getDailyEnergy(unpureDate: Date = new Date)(implicit connection: Connection): BigDecimal

  def getDailyPowerAndIncomeSeries(unpureDate: Date = new Date)(implicit connection: Connection): (TimeSeries, TimeSeries)

  def getDailyChartImageFile(chartTitle: String, unpureDate: Date = new Date)(implicit connection: Connection): File = {
    val (powerSeries, incomeSeries) = getDailyPowerAndIncomeSeries(unpureDate)

    //chart code starts here
    val powerDataset = new TimeSeriesCollection()
    powerDataset.addSeries(powerSeries)

    val user: User = getUser.get
    implicit val lang = user.getLang

    val chart = ChartFactory.createTimeSeriesChart(
      //DailyReport.getChartTitle, //title
      chartTitle, //title
      DailyReport.getChartTimeAxisLabel, //timeAxisLabel
      DailyReport.getChartValueAxisLabel, //valueAxisLabel
      powerDataset, //dataset
      false, //legend
      false, //tooltips
      false //urls
    );

    val plot = chart.getXYPlot;
    val areaRenderer = new XYAreaRenderer();
    areaRenderer.setSeriesPaint(0, DailyReport.chartPowerAreaColor);
    plot.setRenderer(0, areaRenderer);

    //second axis
    val incomeDataset = new TimeSeriesCollection();
    incomeDataset.addSeries(incomeSeries);

    val incomeAxis = new NumberAxis(DailyReport.getChartRightAxisLabel);
    incomeAxis.setAutoRangeIncludesZero(false);

    plot.setRangeAxis(1, incomeAxis);
    plot.setDataset(1, incomeDataset);
    plot.mapDatasetToRangeAxis(1, 1);

    val standardRenderer = new StandardXYItemRenderer();
    standardRenderer.setSeriesStroke(0, new BasicStroke(DailyReport.chartIncomeLineWidth));
    standardRenderer.setSeriesPaint(0, DailyReport.chartIncomeLineColor);
    standardRenderer.setPlotLines(true);
    plot.setRenderer(1, standardRenderer);

    plot.setForegroundAlpha(DailyReport.chartTransparency.toFloat);

    val path = DailyReport.genNewTemporaryPngPath;

    //generate image
    ChartUtilities.writeChartAsPNG(
      new FileOutputStream(path),
      chart,
      DailyReport.chartWidth, //width
      DailyReport.chartHeight //height
    );

    val imageFile = new File(path);

    assert(imageFile.exists(), "we just wrote the chart on a png file, therefore it must exist");

    imageFile
  }

  def getUser(implicit connection: Connection): Option[User] = {
    if (id.isDefined) {
      SQL( """SELECT *
             |FROM User
             |WHERE User.id = (
             |	SELECT User_id
             |	FROM PvPlant
             |	WHERE PvPlant.id = (
             |		SELECT PvPlant_id
             |		FROM MonitoringSystem
             |		WHERE MonitoringSystem.id = {MonitoringSystem_id}
             |	)
             |)""".stripMargin).on(
        "MonitoringSystem_id" -> id.get
      ).as(User.parser.singleOpt);
    }
    else {
      None;
    }
  }

  def getPvPlant(implicit connection: Connection): Option[PvPlant] = {
    if (id.isDefined) {
      SQL( """SELECT * FROM PvPlant
             |WHERE PvPlant.id = (
             |    SELECT PvPlant_id FROM MonitoringSystem
             |    WHERE MonitoringSystem.id={MonitoringSystem_id}
             |)""".stripMargin).on(
        "MonitoringSystem_id" -> id.get
      ).as(PvPlant.parser.singleOpt);
    }
    else {
      None;
    }
  }

  def getForwardEmail(implicit connection: Connection): Option[String] = {
    assert(id.isDefined);

    val column = "mail";

    val rowOption = SQL("SELECT " + column + " FROM ForwardEmail WHERE MonitoringSystem_id={MonitoringSystem_id}").on(
      "MonitoringSystem_id" -> id.get
    ).singleOpt();

    //Some("pligor@sunsheriffs.com");
    rowOption.map(row => row[String](column));
  }
}
