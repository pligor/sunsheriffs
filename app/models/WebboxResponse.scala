package models

import play.api.libs.json._;

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
class WebboxResponse(val response: JsObject) {
  //the drop and dropRight are to remove the quotation marks
  val format: String = (response \ "format").toString().drop(1).dropRight(1);
  //we also get the format
  val version: String = (response \ "version").toString().drop(1).dropRight(1);
  val proc: String = (response \ "proc").toString().drop(1).dropRight(1);
  val id: String = (response \ "id").toString().drop(1).dropRight(1);

  val result: Option[JsObject] = (response \ "result") match {
    case y: JsObject => Some(y);
    case _ => None;
  };

  val error: Option[String] = (response \ "error") match {
    case z: JsObject => Some((z \ "message").toString().drop(1).dropRight(1));
    case x: JsString => Some(x.value);
    case JsNull => None;
    case _ => None;
  };
}
