package models

import play.api.libs.json.{JsObject, JsString, JsArray}

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 * Advantages / Disadvantages of case classes: http://stackoverflow.com/questions/4653424/what-are-the-disadvantages-to-declaring-scala-case-classes
 */
class WebboxSetParamRequest(val deviceKey: String, val shortChannels: Seq[WebboxShortChannel]) {
  require(!shortChannels.isEmpty, "you want to set some values. it doesnt make sense to set using no values!");

  def toJsObject = JsObject(Seq(
    "key" -> JsString(deviceKey),
    "channels" -> JsArray(shortChannels.map(_.toJsObject))
  ));
}
