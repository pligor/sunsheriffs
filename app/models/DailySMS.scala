package models

import play.api.i18n.Lang
import components.{TwilioClient}
import myplay.M

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
object DailySMS {
  def generate(dailyProfit: BigDecimal, dailyEnergy: BigDecimal)(implicit lang: Lang): String = {
    val smsString =
      M("Daily Energy") + ":" + dailyEnergy.toDouble.formatted("%.1f") + "kWh & " +
      M("Daily Profit") + ":" + dailyProfit.toDouble.formatted("%.2f") + "€ by Sunsheriffs";
    assert(smsString.length <= TwilioClient.maxTextLength(smsString));
    smsString;
  }
}
