package models

import play.api.libs.json.{JsValue, JsObject}
import components.WebboxJsonRpcClient
import scala.concurrent.Future

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
trait WebboxClient {
  protected def grabIdAndPromiseJson(
                                      serverUrl: String,
                                      password: String,
                                      proc: String,
                                      params: JsObject = JsObject(Seq())
                                      ) = {
    val client = new WebboxJsonRpcClient(serverUrl, password, debug = false);

    val (id, promiseJsonOption) = client.execute(proc, params);
    val promiseJson: Future[JsValue] = promiseJsonOption.getOrElse({
      throw new Exception(proc + " is not a notification, fix client's execution");
    });

    (id, promiseJson)
  }

  protected def grabResultFromJson(json: JsValue, id: String): JsObject = {
    val response = new WebboxResponse(json match {
      case x: JsObject => x
      case _ => throw new ClassCastException("we should get a js object")
    })
    assert(response.error == None, s"response has an error: " + response.error.get)
    assert(response.id == id, "the response id is not the same as the request id!")

    response.result.getOrElse(throw new Exception("we should have got a result!"))
  }
}
