package models

import collection.mutable.ListBuffer
import akka.actor.Cancellable
import components.events.{Event, CallbackHolder}

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
trait OnAddedToSleepingWebboxes;

class OnAddedToSleepingWebboxesEvent(webboxId: Long, cancellor: Cancellable) extends Event with OnAddedToSleepingWebboxes {
  val callbackHolderClass = classOf[OnAddedToSleepingWebboxesCallbackHolder];
}

class OnAddedToSleepingWebboxesCallbackHolder extends CallbackHolder with OnAddedToSleepingWebboxes {
  def getCallbacks: ListBuffer[(Long, Cancellable) => Unit] = ListBuffer.empty[(Long, Cancellable) => Unit];
}
