package models

import play.api.libs.json.{JsValue, JsArray, JsString, JsObject}

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
case class WebboxDeviceDataResponse(deviceKey: String, channels: Seq[WebboxChannel]) {
  /**
   * Auxiliary Constructors in Scala must either call the primary constructor, or
   * another auxiliary constructor from the same class, as their first action
   * @param obj
   */
  def this(obj: JsObject) = {
    this(
      (obj \ "key") match {
        case JsString(x) => x;
        case _ => throw new Exception("the corresponding js object must contain a key");
      }
      ,
      (obj \ "channels") match {
        case JsArray(y) => y.map((k: JsValue) => new WebboxChannel(k match {
          case z: JsObject => z;
          case _ => throw new Exception("each channel must be a js object");
        }));
        case _ => throw new Exception("the channels must be a Js Array");
      }
    );
  };
}
