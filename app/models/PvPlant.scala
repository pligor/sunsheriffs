package models

import components.{MyModelObject, AutoIncrement, MyModel}
import generic.TimeHelper._
import anorm.SqlParser._
import java.sql.Connection
import anorm._
import java.util.{Calendar, Date}
import scala.concurrent.duration.Duration

object PvPlant extends MyModelObject with AutoIncrement {
  val anormParser = get[Pk[Long]]("id") ~ get[String]("name") ~ get[String]("timezone") ~ get[java.math.BigDecimal]("moneyYield");

  val parser = anormParser.map {
    case parsedId ~ parsedName ~ parsedTimezone ~ parsedMoneyYield =>
      new PvPlant(
        name = parsedName,
        timezone = parsedTimezone,
        moneyYield = parsedMoneyYield,
        id = parsedId.toOption
      );
  }
  val tableName: String = "PvPlant";

  def getById(id: Long)(implicit connection: Connection): Option[PvPlant] = super.getById(id, parser);
}

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
class PvPlant(val name: String, val timezone: String, val moneyYield: BigDecimal, id: Option[Long] = None) extends MyModel(id) {
  require(isValidTimezone(timezone));

  def deleteDailyData(date: Date)(implicit connection: Connection) {
    val dayBegin = getDayBegin(date);
    val dayEnd = getDayEnd(date);
    getMonitoringSystems.foreach(system => system.deleteData(dayBegin, dayEnd));
  }

  def getTotalEnergy(implicit connection: Connection): BigDecimal = {
    val seqBigDecimal = getMonitoringSystems.map(_.getTotalEnergy)
    seqBigDecimal.sum;
  }

  def getDailyEnergy(date: Date = new Date)(implicit connection: Connection): BigDecimal = {
    val seqBigDecimal = getMonitoringSystems.map(_.getDailyEnergy(date));
    seqBigDecimal.sum;
  };

  def getUser(implicit connection: Connection): Option[User] = {
    if (id.isDefined) {
      SQL( """SELECT *
             |FROM User
             |WHERE User.id = (
             |	SELECT User_id
             |	FROM PvPlant
             |	WHERE PvPlant.id = {PvPlant_id}
             |)""".stripMargin).on(
        "PvPlant_id" -> id.get
      ).as(User.parser.singleOpt);
    }
    else {
      None;
    }
  }

  def getDurationUntilNextDayBegin(timeNdate: Date)(implicit connection: Connection): Duration = {
    val dayBegin = getDayBegin(timeNdate);
    val dayBeginCal = Calendar.getInstance();
    dayBeginCal.setTime(dayBegin);

    getDurationOfNextUTCtime(
      hourOfDay = dayBeginCal.get(Calendar.HOUR_OF_DAY),
      minute = dayBeginCal.get(Calendar.MINUTE),
      date = timeNdate
    );
  }

  def getDayBegin(unpureDate: Date)(implicit connection: Connection): Date = {
    require(id.isDefined, "ask for when the day begins only on old pv plant models");

    val date = clearTime(unpureDate);

    //val sunriseOption = getUTCsunrise(getGeoPosition.get, date);
    val sunriseOption = getGeoPosition.get.geoPosition.itsUTCsunrise(date);

    if (sunriseOption.isDefined) {
      getUTCtimeAfterDuration(-SunsheriffsConfig.hurryBeforeSunrise, sunriseOption.get);
    }
    else {
      //sun is not rising!
      getUTCmidnight(date, timezone);
    }
  }

  def getDayEnd(unpureDate: Date)(implicit connection: Connection): Date = {
    require(id.isDefined, "ask for when the day ends only on old pv plant models");

    val date = clearTime(unpureDate);

    //val sunsetOption = getUTCsunset(getGeoPosition.get, date);
    val sunsetOption = getGeoPosition.get.geoPosition.itsUTCsunset(date);

    if (sunsetOption.isDefined) {
      getUTCtimeAfterDuration(SunsheriffsConfig.waitAfterSunset, sunsetOption.get);
    } else {
      //sun is not setting!
      getUTCtomorrowMidnight(date, timezone)
    }
  }

  def getMonitoringSystems(implicit connection: Connection): Seq[MonitoringSystem] = {
    require(id.isDefined, "try to retrieve monitoring systems from pv plant only if id is defined");

    val systemIds = SQL( """SELECT MonitoringSystem.id AS MonitoringSystem_id
                           |FROM MonitoringSystem
                           |WHERE PvPlant_id = {PvPlant_id}""".stripMargin).on(
      "PvPlant_id" -> id.get
    ).as(scalar[Long] *);

    systemIds.map {
      systemId =>
        val systemOption = MonitoringSystem.getById(systemId);
        assert(systemOption.isDefined, "since we just retrieved all its ids from the database, they must be valid");
        systemOption.get;
    };
  }

  def getGeoPosition(implicit connection: Connection): Option[GeoPositionModel] = {
    SQL( """SELECT * FROM GeoPosition
           |WHERE GeoPosition.id = (
           |	SELECT GeoPosition_id FROM PvPlant
           |	WHERE PvPlant.id = {PvPlant_id}
           |)""".stripMargin).on(
      "PvPlant_id" -> this.id.getOrElse(PvPlant.invalidId)
    ).as(GeoPositionModel.parser.singleOpt); //no option because is not possible NOT to have a position
  }
}
