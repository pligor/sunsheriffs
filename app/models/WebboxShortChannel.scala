package models

import play.api.libs.json.{JsString, JsObject}

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
case class WebboxShortChannel(meta: String, value: String) {
  def this(obj: JsObject) {
    this(
      (obj \ "meta") match {
        case x: JsString => x.value;
        case _ => throw new ClassCastException(obj.toString());
      },
      (obj \ "value") match {
        case x: JsString => x.value;
        case _ => throw new ClassCastException(obj.toString());
      }
    )
  }

  def toJsObject = JsObject(Seq(
    "meta" -> JsString(meta),
    "value" -> JsString(value)
  ));
}
