package models

import play.api.libs.json._
import play.api.libs.json.JsObject
import play.api.libs.json.JsString
import play.api.libs.json.JsArray
import scala.Some

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 * Describes a process data or parameter channel of a device
 */
/*
{
  "meta": "string",
  "name": "string" (optional),
  "value": "string",
  "unit": "string" (optional),
  "min": "string" (optional),
  "max": "string" (optional),
  "options": [array] (optional)
}
*/
class WebboxChannel(val obj: JsObject) {
  private val shortChannel = new WebboxShortChannel(obj);

  val meta = shortChannel.meta;
  val value = shortChannel.value;

  //The translated display name (e.g. "External radiation"). Specification of the element is optional
  val name: Option[String] = obj \ "name" match {
    case x: JsString => Some(x.value);
    case _ => None;
  }

  //The unit of the channel (e.g. "W/m^2). A null string is entered for channels without units
  val unit: Option[String] = obj \ "unit" match {
    case x: JsString => Some(x.value);
    case _ => None;
  }

  //The minimum value a channel can assume. Specification of the element is optional
  val min: Option[String] = obj \ "min" match {
    case x: JsString => Some(x.value);
    case _ => None;
  }

  //The maximum value a channel can assume. Specification of the element is optional
  val max: Option[String] = obj \ "max" match {
    case x: JsString => Some(x.value);
    case _ => None;
  }

  //A list of possible values a parameter channel can assume. Specification of the element is optional
  val options: Option[Seq[JsValue]] = obj \ "options" match {
    case x: JsArray => Some(x.value);
    case _ => None;
  }
}
