package models

import collection.mutable
import akka.actor.{ActorSystem, Cancellable, Props}
import play.api.libs.concurrent.Akka
import play.api.Play.current
import components.helpers.AkkaHelper._
import play.api.db.DB
import play.api.Logger
import java.sql.Connection
import java.util.concurrent.TimeUnit
import generic.TimeHelper._
import actorMessages.RetrieveOverview
import components.{EventfulMap, EventfulSet}
import scala.concurrent.ExecutionContext
import ExecutionContext.Implicits.global
import scala.concurrent.duration.{Duration, FiniteDuration}
import actors.OverviewMaster
import java.util.Date

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
object OverviewObject {
  val frequencyTimeUnit = TimeUnit.SECONDS
  val frequency = FiniteDuration(60, frequencyTimeUnit) //60.seconds;

  //TODO exoume valei f/2 authaireta
  def getTimeout(f: Int = frequency.toSeconds.toInt) = (f / 2.0).toInt

  def getTimeoutDuration(f: Int = frequency.toSeconds.toInt) = Duration(getTimeout(f), frequencyTimeUnit);

  val monitoredWebboxes = new mutable.HashSet[Webbox] with EventfulSet[Webbox] {
    val onAdded = (webbox: Webbox) => {
      EnergySheriff.monitoredSystems += webbox
    }
    val onRemoved = (webbox: Webbox) => {
      EnergySheriff.monitoredSystems -= webbox
    }
    val onBeforeClear = (theMonitoredWebboxes: EventfulSet[Webbox]) => {
      theMonitoredWebboxes.forall {
        webbox =>
          val prevSize = EnergySheriff.monitoredSystems.size
          EnergySheriff.monitoredSystems -= webbox
          prevSize - EnergySheriff.monitoredSystems.size == 1
      }
    }
  }

  val sleepingWebboxes = new mutable.HashMap[Long, Cancellable] with EventfulMap[Long, Cancellable] {
    val onAdded = (webboxId: Long, cancellor: Cancellable) => {
      EnergySheriff.sleepingSystems += webboxId -> cancellor;
    };
    val onRemoved = (webboxId: Long) => {
      EnergySheriff.sleepingSystems -= webboxId;
    }
  };

  //val overviewAkkaSystem = ActorSystem("OverviewSystem");
  //val overviewMaster = overviewAkkaSystem.actorOf(Props(
  val overviewMaster = Akka.system.actorOf(Props(
    new OverviewMaster(numberOfWorkers = 8 /*, webboxes = monitoredWebboxes*/)
  ), name = "overviewMaster")

  var recordingCancellor: Cancellable = null

  def scheduleWakeup(monitor: Webbox, system: ActorSystem)(implicit connection: Connection): Duration = {
    val durationUntilDayBegin = monitor.getDurationUntilNextDayBegin(new Date)
    Logger info "scheduling for utc time: " + getUTCtimeAfterDuration(durationUntilDayBegin)
    val webboxId = monitor.id.get

    val durationUntilDayBeginFinite = new FiniteDuration(durationUntilDayBegin.toMillis, TimeUnit.MILLISECONDS)

    val cancellor: Cancellable = system.scheduler.scheduleOnce(durationUntilDayBeginFinite)({
      //TODO maybe a proper actor here later
      DB.withConnection(implicit connection => OverviewObject.includeRecording(webboxId))
      OverviewObject.sleepingWebboxes -= webboxId
    })

    OverviewObject.sleepingWebboxes += (webboxId -> cancellor)
    durationUntilDayBegin
  }

  /**
   * @return the previous length
   */
  def includeRecording(webboxId: Long)(implicit connection: Connection): Int = {
    if (isScheduled(recordingCancellor)) {
      //val prevLen = monitoredWebboxes.length;
      val prevLen = monitoredWebboxes.size;

      val webboxOption = Webbox.getById(webboxId);
      if (webboxOption.isDefined
      //this check can be avoided because we use a Set instead of an ArrayBuffer
      //&& !monitoredWebboxes.find(webbox => (webbox.id.isDefined) && (webbox.id.get == webboxId)).isDefined
      ) {
        monitoredWebboxes += webboxOption.get;
      }
      Logger info "including webbox with id: " + webboxId;
      prevLen;
    }
    else {
      throw new Exception;
    }
  }

  def excludeRecording(webboxId: Long): Int = {
    if (isScheduled(recordingCancellor)) {
      //val prevLen = monitoredWebboxes.length;
      val prevLen = monitoredWebboxes.size;

      monitoredWebboxes -= monitoredWebboxes.find(webbox => (webbox.id.isDefined) && (webbox.id.get == webboxId)).getOrElse(null);
      Logger info "excluding webbox with id: " + webboxId;
      prevLen;
    }
    else {
      throw new Exception;
    }
  }

  def scheduleToRecordAllWebboxes(): Boolean = {
    if (isScheduled(recordingCancellor)) {
      false;
    }
    else {
      monitoredWebboxes.clear();
      DB.withConnection {
        implicit connection =>
          val allWebboxes = Webbox.getAll;
          allWebboxes.foreach(webbox => scheduleWakeup(webbox, Akka.system));
      }
      startRecording();
      true;
    }
  }

  private def startRecording() {
    recordingCancellor = Akka.system.scheduler.schedule(
      initialDelay = Duration.Zero,
      interval = frequency,
      receiver = overviewMaster,
      message = RetrieveOverview(f = frequency.toSeconds.toInt /*, webboxes2monitor = monitoredWebboxes.toList*/)
    );
  }

  /**
   * @return whether started successfully or it was already started so it was wrong to try to start again
   */
  def startRecordingAllWebboxes(): Boolean = {
    if (isScheduled(recordingCancellor)) {
      false;
    }
    else {
      val allWebboxes = DB.withConnection(implicit connection => Webbox.getAll);
      monitoredWebboxes.clear();
      allWebboxes.foreach(webbox => monitoredWebboxes += webbox);
      startRecording()
      true;
    }
  }

  def cancelRecording(): Boolean = {
    if (isScheduled(recordingCancellor)) {
      recordingCancellor.cancel();
      assert(recordingCancellor.isCancelled);
      true;
    }
    else {
      false;
    }
  }

  //val overviewStartuper = Akka.system.actorOf(Props[OverviewStartup], name = "overviewStartuper");
  //val overviewShutdowner = Akka.system.actorOf(Props[OverviewShutdown], name = "overviewShutdowner");

  //var overviewStartupCancellor: Cancellable = null;
  //var overviewShutdownCancellor: Cancellable = null;

  /*def scheduleStartupOrShutdown(system: ActorSystem) {
    val (duration, isCloserToStartup) = getDurationCloserToStartupOrShutdown();

    if (isCloserToStartup) {
      overviewStartupCancellor = system.scheduler.scheduleOnce(
        duration,
        overviewStartuper,
        StartupOverviewNow
      );
      Logger info "startup scheduled with duration in minutes: " + duration.toMinutes;
    }
    else {
      overviewShutdownCancellor = system.scheduler.scheduleOnce(
        duration,
        overviewShutdowner,
        ShutdownOverviewNow
      );
      Logger info "shutdown scheduled with duration in minutes: " + duration.toMinutes;
    }
  }*/

  //maybe make this time type safe with a case class
  //remember is UTC and we consider only Greece
  //val startupTime = (15, 30);
  //remember is UTC and we consider only Greece
  //val shutdownTime = (15, 32);

  //deprecated
  //param date a date to compare to
  //return the duration the whether is closer to startup or not (which means closer to shutdown)
  /*private def getDurationCloserToStartupOrShutdown(date: Date = convDate2GMT()): (Duration, Boolean) = {

    val durationUntilStartup: Duration = getDurationOfNextUTCtime(startupTime._1, startupTime._2, date = date);

    val durationUntilShutdown: Duration = getDurationOfNextUTCtime(shutdownTime._1, shutdownTime._2, date = date);

    if (durationUntilStartup < durationUntilShutdown) {
      (durationUntilStartup, true)
    }
    else {
      (durationUntilShutdown, false)
    }
  }*/
}
