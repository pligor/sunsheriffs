package models

import jodd.mail._
import components.Authenticator

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 * Default non ssl ports
 * POP3: 110
 * IMAP: 143
 * SMTP: 25
 */
class EmailServer(
                   val smtpServer: String,
                   val imapServer: Option[String] = None,
                   val popServer: Option[String] = None,
                   val smtpPort: Int = 465,
                   val imapPort: Int = 993,
                   val popPort: Int = 995,
                   val isSmtpSSL: Boolean = true,
                   val isImapSSL: Boolean = true,
                   val isPopSSL: Boolean = true
                   ) {
  //at least one receiving server is defined
  require(imapServer.isDefined || popServer.isDefined);

  def getPopServer(auth: Option[Authenticator] = None) = {
    require(popServer.isDefined);
    if (isPopSSL) {
      require(auth.isDefined);
      new Pop3SslServer(popServer.get, popPort, auth.get.user, auth.get.pass);
    }
    else {
      if (auth.isDefined) {
        /*new Pop3Server( //For some bizarre reason this does not compile
          popServer.get,
          new SimpleAuthenticator(auth.get.user, auth.get.pass)
        );*/
        new Pop3Server(popServer.get, popPort, auth.get.user, auth.get.pass);
      }
      else {
        new Pop3Server(popServer.get, popPort);
      }
    }
  }

  def getSmtpServer(auth: Option[Authenticator] = None) = {
    if (isSmtpSSL) {
      require(auth.isDefined);
      new SmtpSslServer(smtpServer, smtpPort, auth.get.user, auth.get.pass);
    }
    else {
      if (auth.isDefined) {
        new SmtpServer(smtpServer, smtpPort, auth.get.user, auth.get.pass);
      }
      else {
        new SmtpServer(smtpServer, smtpPort)
      }
    }
  }
}
