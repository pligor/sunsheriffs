package models

import components.MyModel
import java.util.{TimeZone, Date}
import java.text.SimpleDateFormat
import generic.TimeHelper._
import scala.Some
import java.sql.Connection

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
object WebboxAlertEmailError {
  val timedatePattern = WebboxAlertEmail.timedatePattern;
  val timedateRegex = WebboxAlertEmail.timedateRegex;

  def apply(lines: List[String], webbox: Webbox)(implicit connection: Connection): Option[WebboxAlertEmailError] = {
    try {
      assert(lines.length == 2)

      val firstLine = lines.head

      //throws MatchError exception
      val Some(timeOccuredStr) = timedateRegex.findFirstIn(firstLine);

      val timedateFormat = new SimpleDateFormat(timedatePattern);
      //throws NoSuchElementException
      timedateFormat.setTimeZone(TimeZone.getTimeZone(webbox.getPvPlant.get.timezone));
      val timeDate = timedateFormat.parse(timeOccuredStr);

      val restFirstLine = WebboxAlertEmailError.timedateRegex.replaceFirstIn(firstLine, "");

      val deviceKey = restFirstLine.tail; //drop first element (here character)

      val secondLine = lines.tail.head;

      //throws NoSuchElementException
      Some(new WebboxAlertEmailError(errorCode = secondLine, timeOccured = timeDate, device = webbox.getDeviceByKey(deviceKey).get))
    }
    catch {
      case e: MatchError => None;
      case e: NoSuchElementException => None;
      case e: Throwable => None;
    }
  }
}

//TODO errorCode later must be one of the valid error codes of SMA
class WebboxAlertEmailError(
                             val errorCode: String,
                             val timeOccured: Date,
                             val device: WebboxDevice,
                             id: Option[Long] = None) extends MyModel(id) {

}
