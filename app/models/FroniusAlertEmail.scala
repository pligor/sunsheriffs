package models

import jodd.mail.ReceivedEmail
import java.sql.Connection
import components.AlertEmailObject

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
object FroniusAlertEmail extends AlertEmailObject {
  //TODO actually unimplemented
  def isEmailMine(email: ReceivedEmail) = false;

  def apply(email: ReceivedEmail)(implicit connection: Connection): Option[AlertEmail] = {
    println("now I am parsing a webbox alert email"); //TODO implement
    Some(new FroniusAlertEmail(null));
  }
}

//TODO here maybe a new class is required for fronius
class FroniusAlertEmail(fronius: MonitoringSystem) extends AlertEmail(fronius) {

}
