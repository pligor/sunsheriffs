package models

import play.api.i18n.{Messages, Lang}
import java.io.File
import java.awt.Color
import java.util.UUID
import myplay.M

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object DailyReport {
  val chartTransparency = 0.5;

  val chartPowerAreaColor = Color.green;

  val chartIncomeLineColor = Color.black;
  val chartIncomeLineWidth = 5;

  val datePattern = "EEEE, dd MMMM yyyy";
  val timePattern = "h:mm a";

  private val chartRatio: Double = 16.0 / 10.0;
  val chartWidth = 648;
  val chartHeight: Int = (chartWidth / chartRatio).toInt;

  def getEmailSubject(dateToday: String)(implicit lang: Lang) = {
    M("Daily Report") + " " + M("from") + " Sunsheriffs - " + dateToday;
  }

  def getChartTitle(implicit lang: Lang) = {
    Messages("Daily Report");
  }

  def getChartTimeAxisLabel(implicit lang: Lang) = {
    Messages("Time");
  }

  def getChartValueAxisLabel(implicit lang: Lang) = {
    Messages("Power") + " (kW)";
  }

  def getChartRightAxisLabel(implicit lang: Lang) = {
    Messages("Income") + " (€)";
  }

  def genNewTemporaryPngPath = {
    val path = System.getProperty("java.io.tmpdir") + File.separator + UUID.randomUUID().toString + ".png";
    assert(!new File(path).exists());
    path;
  };
}
