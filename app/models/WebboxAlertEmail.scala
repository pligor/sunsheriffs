package models

import jodd.mail.ReceivedEmail
import generic.TimeHelper._
import java.util.{TimeZone, Date}
import collection.JavaConversions._
import components.helpers.StringHelper._
import java.text.SimpleDateFormat
import java.sql.Connection
import components.helpers.AnormHelper._
import scala.Predef._
import scala.Some
import components.AlertEmailObject

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
object WebboxAlertEmail extends AlertEmailObject {
  val namePrefix = "Sunny WebBox error report for ";
  val serialNumPrefix = "Sunny WebBox serial number: ";
  val delimiterLineChar = '-';
  val timeCreatedPrefix = "created on";

  //29.07.2012 09:35
  val timedatePattern = "dd.MM.yyyy HH:mm";
  val timedateRegex = """\d{2}\.\d{2}\.\d{4} \d{2}:\d{2}""".r;

  def isEmailMine(email: ReceivedEmail): Boolean = {
    email.getFrom contains "noreply@Sunny-WebBox.com";
  }

  def apply(email: ReceivedEmail)(implicit connection: Connection): Option[AlertEmail] = {
    //println("now I am parsing a webbox alert email");
    try {
      val messages = email.getAllMessages.toList;
      assert(messages.length == 1); //TODO check if this is in fact true for all possible emails sent from webbox

      val message = messages.head;
      val lines = message.getContent.lines.toList;

      val nameLine = lines.head;
      assert(nameLine.startsWith(namePrefix));
      val name = nameLine.substring(namePrefix.length);

      val serialNumLine = lines.tail.head;
      assert(serialNumLine.startsWith(serialNumPrefix));
      val serialNum = serialNumLine.substring(serialNumPrefix.length).toLong;
//println("here");
      val webbox = Webbox.getBySerialNum(serialNum.toString).get; //throws NoSuchElementException
//println("here");
      //find the first delimiter line
      val errorLines = lines.dropWhile(line => !line.forall(_ == delimiterLineChar) || line.isEmpty);

      val blocks = splitLinesByDelimiterLine(errorLines, delimiterLineChar);

      //by definition the last error block is NOT an error
      val (errorBlocks, List(lastBlock)) = blocks.splitAt(blocks.length - 1);

      val Some(timeCreatedLine) = lastBlock.find(_.startsWith(timeCreatedPrefix)); //throws MatchError exception

      val Some(timeCreatedStr) = timedateRegex.findFirstIn(timeCreatedLine); //throws MatchError exception

      val timedateFormat = new SimpleDateFormat(timedatePattern)
      //Europe/Copenhagen, Europe/Athens, ...
      timedateFormat.setTimeZone(TimeZone.getTimeZone(webbox.getPvPlant.get.timezone))
      //throws NoSuchElementException
      val timeDate = timedateFormat.parse(timeCreatedStr)

      val webboxAlertEmail = new WebboxAlertEmail(
        webbox = webbox,
        webboxName = name,
        timeCreated = timeDate,
        errors = errorBlocks.map(errorBlock => WebboxAlertEmailError(errorBlock, webbox).get)
      )

      Some(webboxAlertEmail)
    }
    catch {
      case e: MatchError => {
        //println("MatchError");
        None;
      }
      case e: NoSuchElementException => {
        //println("NoSuchElementException");
        None;
      }
      case e: Throwable => None;
    }
  }
}

class WebboxAlertEmail(
                        webbox: Webbox,
                        val webboxName: String,
                        val timeCreated: Date,
                        val errors: Seq[WebboxAlertEmailError]
                        ) extends AlertEmail(webbox) {

  def insert(implicit connection: Connection) = insertInto("WebboxAlertEmail", Map(
    "webboxName" -> webboxName,
    "timeCreated" -> timeCreated,
    "Webbox_MonitoringSystem_id" -> webbox.id.get
  ));

}
