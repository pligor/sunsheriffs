package models

import java.util.Date
import anorm.SqlParser._
import anorm.{~, Pk}
import generic.Hash

object EmailAccount {
  private val anormParser = get[Pk[Long]]("user_id") ~
    get[String]("mail") ~
    get[String]("pass") ~
    get[String]("hashedPass") ~
    get[Date]("timePasswordSet") ~
    get[Option[String]]("activationKey") ~
    get[Boolean]("isActivated")

  val parser = anormParser.map {
    case parsedUserId ~
      parsedMail ~
      parsedPass ~
      parsedHashedPass ~
      parsedTimePasswordSet ~
      parsedActivationKey ~
      parsedIsActivated => {
      val emailAccount = new EmailAccount(user_id = parsedUserId.get,
        mail = parsedMail,
        pass = parsedPass,
        timePasswordSet = parsedTimePasswordSet,
        isActivated = parsedIsActivated,
        activationKey = parsedActivationKey)

      assert(emailAccount.hashedPass.toString == parsedHashedPass, "calculated pass: " +
        emailAccount.hashedPass
        + " and stored hashed password: " + parsedHashedPass + " must be the same")

      emailAccount
    }
  }

  val hashAlgo = "SHA1"
}

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
class EmailAccount(
                    val user_id: Long,
                    val mail: String,
                    val pass: String,
                    val timePasswordSet: Date,
                    val isActivated: Boolean,
                    val activationKey: Option[String]
                    ) {
  val hashedPass = Hash(pass, EmailAccount.hashAlgo)
}
