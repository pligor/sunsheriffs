package models

import play.api.libs.json._
import play.api.libs.json.JsString
import play.api.libs.json.JsArray
import play.api.libs.json.JsObject

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
case class WebboxDeviceDataRequest(deviceKey: String, channels: Seq[String] = Seq()) {
  def toJsObject = {
    val channelsJson = if (channels.isEmpty)
      JsNull
    else
      JsArray(channels.map((channel: String) => JsString(channel)));

    JsObject(Seq(
      "key" -> JsString(deviceKey),
      "channels" -> channelsJson
    ));
  }
}
