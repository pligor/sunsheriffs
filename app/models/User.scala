package models

import java.util.{TimeZone, Locale, Date}
import anorm.SqlParser._
import anorm._
import java.sql.Connection
import generic.TimeHelper._
import java.text.SimpleDateFormat
import java.io.File
import collection.mutable.ListBuffer
import anorm.~
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber
import com.google.i18n.phonenumbers.PhoneNumberUtil
import components.{MyModelObject, MyModel, AutoIncrement}
import play.api.i18n.Lang

object User extends MyModelObject with AutoIncrement {
  private val anormParser = get[Pk[Long]]("id") ~
    get[String]("name") ~
    get[String]("language") ~
    get[Int]("rating") ~
    get[Date]("timeCreated") ~
    get[Option[Date]]("timeVisited") ~
    get[Option[String]]("cookieKey") ~
    get[String]("timezone") ~
    get[java.math.BigInteger]("cellphone");

  val parser = anormParser.map {
    case parsedId ~
      parsedName ~
      parsedLanguage ~
      parsedRating ~
      parsedTimeCreated ~
      parsedTimeVisited ~
      parsedCookieKey ~
      parsedTimezone ~
      parsedCellphone =>
      new User(name = parsedName,
        language = parsedLanguage,
        rating = parsedRating,
        timeCreated = parsedTimeCreated,
        timeVisited = parsedTimeVisited,
        cookieKey = parsedCookieKey,
        timezone = parsedTimezone,
        cellphone = new BigInt(parsedCellphone),
        id = parsedId.toOption);
  }

  val tableName: String = "User";

  def getById(id: Long)(implicit connection: Connection): Option[User] = super.getById(id, parser);
}

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
class User(val name: String,
           private val language: String,
           val rating: Int,
           val timeCreated: Date,
           val timeVisited: Option[Date],
           val cookieKey: Option[String],
           val timezone: String,
           private val cellphone: BigInt,
           id: Option[Long] = None) extends MyModel(id) {
  require(isValidTimezone(timezone))

  def getLocale = new Locale(language)

  def getLang = Lang(language)

  //TODO internationalize cellphone entry (probably a new table with two columns, number and region)
  //private def getCellphone: String = "+30" + cellphone.toInt;
  def getCellphone: PhoneNumber = PhoneNumberUtil.getInstance().parse(cellphone.toString(), "GR")

  def getUsernameKlitikh = {
    //TODO provide the correct implementation
    name
  }

  def deleteAllPlantsDailyData(date: Date)(implicit connection: Connection) {
    getPvPlants.foreach(_.deleteDailyData(date))
  }

  private def getDateFormat(pattern: String): SimpleDateFormat = {
    val sdf = new SimpleDateFormat(pattern, new Locale(language))
    sdf.setTimeZone(TimeZone.getTimeZone(timezone))
    sdf
  }

  def getDateToday(date: Date = new Date) = {
    getDateFormat(DailyReport.datePattern).format(date)
  }

  def getTimeNow(date: Date = new Date) = {
    getDateFormat(DailyReport.timePattern).format(date)
  }

  def getTotalEnergy(implicit connection: Connection): BigDecimal = {
    val seqBigDecimal = getPvPlants.map(_.getTotalEnergy)
    seqBigDecimal.sum
  }

  def getDailyEnergyAndProfit(date: Date = new Date)(implicit connection: Connection) = {
    val energiesAndMoneyYields = getPvPlants.map(plant => (plant.getDailyEnergy(date), plant.moneyYield))

    val dailyEnergy = energiesAndMoneyYields.map(_._1).sum
    val dailyProfit = energiesAndMoneyYields.map(energyAndMoneyYield => energyAndMoneyYield._1 * energyAndMoneyYield._2).sum;
    (dailyEnergy, dailyProfit)
  }

  def getDailyChartImageFiles(date: Date = new Date)(implicit connection: Connection): Seq[File] = {
    val chartImageFiles = ListBuffer.empty[File]

    getPvPlants.foreach {
      (plant: PvPlant) =>
        val systems: Seq[MonitoringSystem] = plant.getMonitoringSystems

        val plantName = plant.name
        val systemsLen = systems.length

        var counter = 0
        systems.foreach {
          (system: MonitoringSystem) =>
            val chartTitle: String = if (systemsLen == 1) {
              plantName
            }
            else {
              counter += 1
              plantName + " (" + counter + "/" + systemsLen + ")"
            }
            chartImageFiles += system.getDailyChartImageFile(chartTitle, date)
        }
    }

    chartImageFiles.toSeq
  }

  private def getDailyEnergy()(implicit connection: Connection) = {
    val systemsDailyEnergies = getMonitoringSystems.map(system => system.getDailyEnergy())
    systemsDailyEnergies.sum
  }

  /**
   * @return NO option here because an old User model must ALWAYS have an Email Account
   *         Other account login methods, like facebook, google, etc. are optional
   */
  def getEmailAccount(implicit connection: Connection): EmailAccount = {
    require(id.isDefined, "please ask for the email account of this user only if id is defined");
    SQL( """SELECT *
           |FROM EmailAccount
           |WHERE user_id = {user_id}""".stripMargin).on(
      "user_id" -> id.get
    ).as(EmailAccount.parser.single);
  }

  def getPvPlants(implicit connection: Connection): Seq[PvPlant] = {
    require(id.isDefined, "please ask for a users pv plant only if id is defined");
    SQL( """SELECT *
           |FROM PvPlant
           |WHERE User_id = {User_id}""".stripMargin).on(
      "User_id" -> id.get
    ).as(PvPlant.parser *);
  }

  def getMonitoringSystems(implicit connection: Connection): Seq[MonitoringSystem] = {
    require(id.isDefined, "try to retrieve monitoring systems only if id is defined");

    val systemIds = SQL( """SELECT MonitoringSystem.id AS MonitoringSystem_id
                           |FROM MonitoringSystem
                           |WHERE PvPlant_id IN (
                           |	SELECT PvPlant.id
                           |	FROM PvPlant
                           |	WHERE User_id = {User_id}
                           |)""".stripMargin).on(
      "User_id" -> id.get
    ).as(scalar[Long] *);

    systemIds.map {
      systemId =>
        val systemOption = MonitoringSystem.getById(systemId);
        assert(systemOption.isDefined, "since we just retrieved all its ids from the database, they must be valid");
        systemOption.get;
    };
  }
}
