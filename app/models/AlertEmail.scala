package models

import jodd.mail.ReceivedEmail
import collection.mutable.ListBuffer
import java.sql.Connection
import components.{InverterCompany, AlertEmailObject}

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
object AlertEmail {
  def apply(email: ReceivedEmail)(implicit connection: Connection): Option[AlertEmail] = {
    val alertEmailBuffer = new ListBuffer[AlertEmailObject];

    //for each inverter company test if email is owned by it
    for (alertEmailObject <- getAll) {
      if (alertEmailObject.isEmailMine(email)) {
        alertEmailBuffer += alertEmailObject;
      }
    }
    assert(alertEmailBuffer.length <= 1); //at most one should match

    val alertEmailObjectOption = alertEmailBuffer.headOption;
    if (alertEmailObjectOption.isDefined) {
      val finalAlertEmailObject = alertEmailObjectOption.get;
      finalAlertEmailObject.apply(email);
    }
    else {
      None
    }
  }

  def getAll = {
    val list = List[AlertEmailObject](
      WebboxAlertEmail,
      FroniusAlertEmail
    );
    assert(list.length == InverterCompany.values.size);
    list
  };
}

abstract class AlertEmail(
                           val monitoringSystem: MonitoringSystem
                           ) {
}
