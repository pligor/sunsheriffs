package models

import collection.mutable
import akka.actor.Cancellable
import java.sql.Connection

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
object EnergySheriff {

  val monitoredSystems = mutable.Set.empty[MonitoringSystem];

  /**
   * Long is the id of the monitoring system being to sleep
   * The scheduled actor to wake up this monitoring system can be cancelled with the Cancellable
   */
  val sleepingSystems = mutable.Map.empty[Long, Cancellable];

  def isTheOnlyMonitoredSystemForItsUser(monitoringSystem: MonitoringSystem)(implicit connection: Connection): Boolean = {
    require(monitoredSystems.exists(_ == monitoringSystem),
      "the system you are trying to check with id: " + monitoringSystem.id.get +
        " must be one of the systems being currently monitored. ids: " + monitoredSystems.map(_.id.get));

    val userOption = monitoringSystem.getUser;
    assert(userOption.isDefined, "a monitored system must have its own user");
    val user = userOption.get;

    //Rephrase the question: Is its user the only one from the rest of the monitored systems?
    val restMonitoredSystems = monitoredSystems - monitoringSystem; //we do NOT want to compare with ourself of course
    //remember forall returns true if the collection is empty, but require above makes sure it's not ;)
    restMonitoredSystems.forall(_.getUser.get != user);
  }
}
