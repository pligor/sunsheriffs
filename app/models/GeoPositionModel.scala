package models

import components.{MyModel, AutoIncrement}
import anorm.SqlParser._
import anorm.{~, Pk}
import java.util.Date
import generic.{GeoPosition, TimeHelper}

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */

object GeoPositionModel extends AutoIncrement {
  val anormParser = get[Pk[Long]]("id") ~ get[Double]("lat") ~ get[Double]("lon");

  val parser = anormParser.map {
    case parsedId ~ parsedLat ~ parsedLon =>
      new GeoPositionModel(GeoPosition(parsedLat, parsedLon), id = parsedId.toOption);
  }
}

class GeoPositionModel(val geoPosition: GeoPosition, id: Option[Long] = None) extends MyModel(id) {
  /**
   * @return Google Maps format
   */
  override def toString: String = geoPosition.toString;
}
