package models

import components.helpers.AkkaHelper._
import play.api.libs.json._
import anorm.SqlParser._
import anorm._
import components.{MyModelObject, AutoIncrement}
import java.sql.Connection
import java.math.BigInteger
import java.util.{TimeZone, Date}
import generic.TimeHelper._
import components.helpers.AnormHelper._
import play.api.Logger
import play.api.libs.json.JsArray
import play.api.libs.json.JsNumber
import anorm.~
import play.api.libs.json.JsObject
import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.duration.Duration
import ExecutionContext.Implicits.global
import org.jfree.data.time.{Second, TimeSeries}

object Webbox extends MyModelObject with AutoIncrement {
  val tableName: String = "Webbox"

  val anormParser = MonitoringSystem.anormParser ~ get[String]("name") ~ get[BigInteger]("serialNum")

  val parser = anormParser.map {
    case parsedId ~ parsedServerUrl ~ parsedPassword ~ parsedTimeRegistered ~ parsedName ~ parsedSerialNum =>
      new Webbox(parsedName, parsedSerialNum, parsedTimeRegistered, parsedServerUrl, parsedPassword, parsedId.toOption)
  }

  def getAll(implicit connection: Connection): List[Webbox] = {
    SQL(
      """SELECT *
        |FROM Webbox, MonitoringSystem
        |WHERE id = MonitoringSystem_id
      """.stripMargin).as(parser *)
  }

  def getBySerialNum(serialCode: String)(implicit connection: Connection): Option[Webbox] = {
    SQL( """SELECT *
           |FROM Webbox, MonitoringSystem
           |WHERE id = MonitoringSystem_id AND serialNum={serialNum}""".stripMargin).on(
      "serialNum" -> serialCode.toLong
    ).as(parser.singleOpt)
  }

  def getById(id: Long)(implicit connection: Connection): Option[Webbox] = {
    SQL( """SELECT *
           |FROM Webbox, MonitoringSystem
           |WHERE id = MonitoringSystem_id AND MonitoringSystem_id = {MonitoringSystem_id}""".stripMargin).on(
      "MonitoringSystem_id" -> id
    ).as(parser.singleOpt)
  }
}

/**
 * Created by pligor
 */
class Webbox(
              val name: String,
              val serialNum: BigInt,
              timeRegistered: Date,
              serverUrl: String,
              password: String = "sma",
              id: Option[Long] = None)
  extends MonitoringSystem(serverUrl, password, timeRegistered, id)
  with WebboxClient {

  private val maxDevicesPerProcessDataRequest = 5;

  def getDurationUntilNextDayBegin(timeNdate: Date)(implicit connection: Connection): Duration = {
    require(id.isDefined, "ask for the duration until the beginning of recording of the next day only for old models");
    getPvPlant.get.getDurationUntilNextDayBegin(timeNdate);
  }

  def insertNewWebboxPlantOverviewModel(model: WebboxPlantOverviewModel)(implicit connection: Connection): Option[Long] = {
    require(id.isDefined, "you cannot insert a new webbox plant overview row if the current webbox model is a new one");
    insertInto("WebboxPlantOverview", model.toMap ++ Map("Webbox_MonitoringSystem_id" -> id.get));
  }

  /**
   * @return value is always false for some bizarre reason but deletion is successful
   */
  def deleteData(fromTime: Date, untilTime: Date)(implicit connection: Connection): Boolean = {
    require(id.isDefined, "delete data only on old models");

    SQL( """DELETE
           |FROM WebboxPlantOverview
           |WHERE Webbox_MonitoringSystem_id = {Webbox_MonitoringSystem_id}
           |AND timeRecorded >= {fromTime}
           |AND timeRecorded <= {untilTime}""".stripMargin).on(
      "Webbox_MonitoringSystem_id" -> id.get,
      "fromTime" -> fromTime,
      "untilTime" -> untilTime
    ).execute();
  }

  def getDailyPowerAndIncomeSeries(unpureDate: Date = new Date)(implicit connection: Connection): (TimeSeries, TimeSeries) = {
    assert(id.isDefined, "require power and income series only for stored webbox models")

    val date = clearTime(unpureDate)

    val plant = getPvPlant.get

    val dayBegin: Date = plant.getDayBegin(date)
    val dayEnd: Date = plant.getDayEnd(date)
    val instantOutputs = SQL( """SELECT timeRecorded,
                                |instantaneousPower,""".stripMargin +
      //|dailyEnergy * {moneyYield} AS income
      "\n" + "dailyEnergy * " + plant.moneyYield + " AS income" + "\n" +
      """|FROM WebboxPlantOverview
        |WHERE Webbox_MonitoringSystem_id = {Webbox_MonitoringSystem_id}
        |AND timeRecorded > {dayBegin}
        |AND timeRecorded < {dayEnd}""".stripMargin).on(
      //"moneyYield" -> plant.moneyYield, //TODO find out why it didn't work
      "Webbox_MonitoringSystem_id" -> id.get,
      "dayBegin" -> dayBegin,
      "dayEnd" -> dayEnd
    ).as(WebboxPlantOverview.parser *)

    val powerSeries = new TimeSeries("power_series", classOf[Second])
    val incomeSeries = new TimeSeries("income_series", classOf[Second])

    instantOutputs.foreach {
      output =>
        val instance = new Second(output.t, TimeZone.getTimeZone(plant.timezone), plant.getUser.get.getLocale)
        powerSeries.add(instance, output.power)
        incomeSeries.add(instance, output.income)
    }

    (powerSeries, incomeSeries)
  }

  /**
   * retrieving daily energy with three ways
   * first check db for daily energy within logical hours
   * second check db within the entire day
   * thirdly do a check to
   * @param unpureDate
   * @return
   */
  def getDailyEnergy(unpureDate: Date = new Date)(implicit connection: Connection): BigDecimal = {
    assert(id.isDefined, "ask for daily energy only for an old webbox model");

    val date = clearTime(unpureDate);

    val plant = getPvPlant.get;

    val todayBegin: Date = plant.getDayBegin(date);

    val todayEnd: Date = plant.getDayEnd(date);

    val sqlQuery = """SELECT dailyEnergy
                     |FROM WebboxPlantOverview AS p
                     |WHERE p.timeRecorded = (
                     |	SELECT MAX(timeRecorded) AS lastRecordedTime
                     |	FROM WebboxPlantOverview AS t
                     |	WHERE timeRecorded > {todayBegin}
                     |	AND timeRecorded < {todayEnd}
                     |)
                     |AND p.Webbox_MonitoringSystem_id = {id}""".stripMargin;

    val dailyEnergyOptionBySunriseSunset = SQL(sqlQuery).on(
      "todayBegin" -> todayBegin,
      "todayEnd" -> todayEnd,
      "id" -> id.get
    ).as(scalar[java.math.BigDecimal].singleOpt);

    val dailyEnergyOption: Option[java.math.BigDecimal] = if (dailyEnergyOptionBySunriseSunset.isDefined) {
      dailyEnergyOptionBySunriseSunset
    }
    else {
      //In case of not found it means that we had some communication errors or something

      val timezoneId = plant.timezone;

      val todayMidnight = getUTCmidnight(date, timezoneId);

      val tomorrowMidnight = getUTCtomorrowMidnight(date, timezoneId);

      SQL(sqlQuery).on(
        "todayBegin" -> todayMidnight,
        "todayEnd" -> tomorrowMidnight,
        "id" -> id.get
      ).as(scalar[java.math.BigDecimal].singleOpt);
    }

    val option = try {
      dailyEnergyOption.map(BigDecimal(_));
    }
    catch {
      case e: NumberFormatException => None;
    }

    option.getOrElse({
      //TODO send error report
      val isDefined = dailyEnergyOptionBySunriseSunset.isDefined;
      Logger info "the daily energy option of " + id.get + " is: " + dailyEnergyOption +
        " and we used the " + (if (isDefined) "first" else "second") + " method";
      val timezoneId = plant.timezone;
      val todayMidnight = getUTCmidnight(date, timezoneId);
      Logger info "today midnight is: " + todayMidnight;
      val tomorrowMidnight = getUTCtomorrowMidnight(date, timezoneId);
      Logger info "tomorrow midnight is: " + tomorrowMidnight;

      val overviewPromiseResult = fetchPromise(
        GetPlantOverview(),
        OverviewObject.getTimeoutDuration(),
        (msg) => WebboxPlantOverview.invalidOverview,
        (thrown) => WebboxPlantOverview.invalidOverview
      );

      if (overviewPromiseResult.isOk) {
        BigDecimal(overviewPromiseResult.obj.dailyEnergy);
      }
      else {
        //TODO send another error report
        0;
      }
    });
  }

  def getTotalEnergy(implicit connection: Connection): BigDecimal = {
    val overviewPromiseResult = fetchPromise(
      GetPlantOverview(),
      OverviewObject.getTimeoutDuration(),
      (msg) => WebboxPlantOverview.invalidOverview,
      (thrown) => WebboxPlantOverview.invalidOverview
    );

    if (overviewPromiseResult.isOk) {
      BigDecimal(overviewPromiseResult.obj.totalEnergy);
    }
    else {
      //TODO send error report
      0;
    }
  }

  def getDeviceByKey(key: String)(implicit connection: Connection): Option[WebboxDevice] = {
    SQL( """SELECT *
           |FROM WebboxDevice
           |WHERE Webbox_MonitoringSystem_id = {Webbox_MonitoringSystem_id}
           |AND deviceKey = {deviceKey}""".stripMargin).on(
      "Webbox_MonitoringSystem_id" -> id.getOrElse(Webbox.invalidId),
      "deviceKey" -> key
    ).as(WebboxDevice.parser.singleOpt);
  }

  /*
  for device "SENS0700:26680" we will test with param: "DevNam" which is read-write
  and for device "WRTP2S42:2110281158" we will test with param: "LCD.SerNum" which is read only! (original value: 288419)
  sample request:
  RPC={
    "version": "1.0",
    "proc": "SetParameter",
    "id": "1001",
    "format": "JSON",
    "passwd": "a289fa4252ed5af8e3e9f9bee545c172",
    "params": {
        "devices":[
           {
              "key":"SENS0700:26680",
              "channels": [
                   {
                      "meta":"DevNam",
                      "value":"sensoras"
                   }
               ]
           }
        ]
    }
  }
  sample response:
  {
    "format":"JSON",
    "result":{
      "devices":[
        {
          "key":"SENS0700:26680",
          "channels":[
            {
              "min":"0",
              "unit":"",
              "meta":"DevNam",
              "name":"DevNam",
              "value":"sensoras",
              "max":"67"
            }
          ]
        }
      ]
    },
    "proc":"SetParameter",
    "version":"1.0",
    "id":"1001"
  }
  */
  def SetParameter(requests: Seq[WebboxSetParamRequest]): Future[Seq[WebboxDeviceDataResponse]] = {
    require(requests.length <= maxDevicesPerProcessDataRequest);

    val params = JsObject(Seq(
      "devices" -> JsArray(requests.map(_.toJsObject))
    ));

    val (id, promiseJson) = grabIdAndPromiseJson(serverUrl, password, "SetParameter", params);

    promiseJson.map[Seq[WebboxDeviceDataResponse]](grabJson2dataResponses(id));
  }

  /*
  sample request: from SENS0700:26680 get everything, from WRTP2S42:2110281158 get LCD.SerNum
  RPC={
    "version": "1.0",
    "proc": "GetParameter",
    "id": "102",
    "format": "JSON",
    "passwd": "a289fa4252ed5af8e3e9f9bee545c172",
    "params": {
        "devices":[
           {
              "key":"SENS0700:26680",
              "channels":null
           },
           {
              "key":"WRTP2S42:2110281158",
              "channels":[
                 "LCD.SerNum"
              ]
           }
        ]
    }
  }
  sample response:
  {"format":"JSON","result":{"devices":[{"key":"SENS0700:26680","channels":[{"min":"0","unit":"","meta":"DevNam","name":"DevNam","value":"","max":"67"},{"min":"0","unit":"","meta":"DevRs","name":"DevRs","value":"0","max":"255"},{"min":"0","unit":"mV","meta":"ExlSolIrrCal","name":"ExlSolIrrCal","value":"100","max":"1000"},{"min":"0","options":["ExlPv 0...300mV","ExlPv 0...1V"],"unit":"","meta":"ExlSolIrrFnc","name":"ExlSolIrrFnc","value":"ExlPv 0...300mV","max":"1"},{"min":"0","unit":"","meta":"FwVer","name":"FwVer","value":"1.51.00","max":"11"},{"min":"0","unit":"","meta":"HwVer","name":"HwVer","value":"C2","max":"33"},{"min":"0","unit":"ms","meta":"RS485Dl","name":"RS485Dl","value":"200","max":"1000"},{"min":"0","options":["300 Baud","600 Baud","1200 Baud","2400 Baud","4800 Baud","9600 Baud","19200 Baud","38400 Baud","57600 Baud","115200 Baud","230400 Baud"],"unit":"","meta":"SmaNetBd","name":"SmaNetBd","value":"1200 Baud","max":"10"},{"min":"0","unit":"","meta":"SN","name":"SN","value":"26680","max":"4294967000"},{"min":"0","options":["°C","K","°F"],"unit":"","meta":"TmpUnit","name":"TmpUnit","value":"°C","max":"2"},{"min":"0","options":["m/s","km/h","mph"],"unit":"","meta":"WindUnit","name":"WindUnit","value":"m/s","max":"2"}]},{"key":"WRTP2S42:2110281158","channels":[{"min":"0","unit":"","meta":"LCD.SerNum","name":"LCD.SerNum","value":"288419","max":"4294960000"}]}]},"proc":"GetParameter","version":"1.0","id":"102"}
  */
  def GetParameter(devices: List[String]): Future[Seq[WebboxDeviceDataResponse]] = {
    GetParameter(devices.map(WebboxDeviceDataRequest(_)));
  }

  def GetParameter(requests: Seq[WebboxDeviceDataRequest]): Future[Seq[WebboxDeviceDataResponse]] = {
    require(requests.length <= maxDevicesPerProcessDataRequest);

    val params = JsObject(Seq(
      "devices" -> JsArray(requests.map(_.toJsObject))
    ));

    val (id, promiseJson) = grabIdAndPromiseJson(serverUrl, password, "GetParameter", params);

    promiseJson.map[Seq[WebboxDeviceDataResponse]](grabJson2dataResponses(id));
  }

  private def grabJson2dataResponses(id: String) = {
    val json2dataResponses: (JsValue) => Seq[WebboxDeviceDataResponse] = (json: JsValue) => {
      val result: JsObject = grabResultFromJson(json, id);
      //println(json.toString());
      val devices: Seq[JsObject] = (result \ "devices") match {
        case JsArray(x) => x.map(_ match {
          case y: JsObject => y;
          case _ => throw new Exception("each device must be a JsObject")
        });
        case _ => throw new Exception("devices must be a JsArray")
      };

      val dataResponses: Seq[WebboxDeviceDataResponse] = devices.map(new WebboxDeviceDataResponse(_));

      dataResponses;
    }

    json2dataResponses;
  }

  /*
  From device WRTP2S42:2110281158 get only this channel "Serial Number"
  and from device SENS0700:26680 get everything
  RPC={
     "version":"1.0",
     "proc":"GetProcessData",
     "id":"100",
     "format":"JSON",
     "params":{
        "devices":[
           {
              "key":"SENS0700:26680",
              "channels":null
           },
           {
              "key":"WRTP2S42:2110281158",
              "channels":[
                 "Serial Number"
              ]
           }
        ]
     }
  }
  sample response:
  {
     "format":"JSON",
     "result":{
        "devices":[
           {
              "key":"SENS0700:26680",
              "channels":[
                 {
                    "unit":"W/m^2",
                    "meta":"ExlSolIrr",
                    "name":"ExlSolIrr",
                    "value":"0"
                 },
                 {
                    "unit":"W/m^2",
                    "meta":"IntSolIrr",
                    "name":"IntSolIrr",
                    "value":"571"
                 },
                 {
                    "unit":"h",
                    "meta":"SMA-h-On",
                    "name":"SMA-h-On",
                    "value":"2588.6999293182"
                 },
                 {
                    "unit":"°C",
                    "meta":"TmpAmb C",
                    "name":"TmpAmb C",
                    "value":"33.73"
                 },
                 {
                    "unit":"°C",
                    "meta":"TmpMdul C",
                    "name":"TmpMdul C",
                    "value":"38.73"
                 },
                 {
                    "unit":"m/s",
                    "meta":"WindVel m/s",
                    "name":"WindVel m/s",
                    "value":"1.4"
                 }
              ]
           },
           {
              "key":"WRTP2S42:2110281158",
              "channels":[
                 {
                    "unit":"",
                    "meta":"Serial Number",
                    "name":"Serial Number",
                    "value":"2110281158"
                 }
              ]
           }
        ]
     },
     "proc":"GetProcessData",
     "version":"1.0",
     "id":"100"
  }
  */
  def GetProcessData(devices: List[String]): Future[Seq[WebboxDeviceDataResponse]] = {
    GetProcessData(devices.map(WebboxDeviceDataRequest(_)));
  }

  def GetProcessData(requests: Seq[WebboxDeviceDataRequest]): Future[Seq[WebboxDeviceDataResponse]] = {
    require(requests.length <= maxDevicesPerProcessDataRequest);

    val params = JsObject(Seq(
      "devices" -> JsArray(requests.map(_.toJsObject))
    ));

    val (id, promiseJson) = grabIdAndPromiseJson(serverUrl, password, "GetProcessData", params);

    promiseJson.map[Seq[WebboxDeviceDataResponse]](grabJson2dataResponses(id));
  }

  /*
  sample request:
  RPC={"version": "1.0","proc": "GetDevices","id": "43825","format": "JSON"}
  sample response:
  {
     "format":"JSON",
     "result":{
        "totalDevicesReturned":11,
        "devices":[
           {
              "key":"SENS0700:26680",
              "name":"SENS0700:26680"
           },
           {
              "key":"WRTP2S42:2110281158",
              "name":"WRTP2S42:2110281158"
           },
           //exoume svisei ta ypoloipa devices
        ]
     },
     "proc":"GetDevices",
     "version":"1.0",
     "id":"43825"
    }
   */
  def GetDevices(): Future[Seq[WebboxDevice]] = {
    val (id, promiseJson) = grabIdAndPromiseJson(serverUrl, password, "GetDevices");

    val json2devices: (JsValue) => Seq[WebboxDevice] = (json: JsValue) => {
      val result: JsObject = grabResultFromJson(json, id);

      val totalDevicesReturned: Int = result \ "totalDevicesReturned" match {
        case x: JsNumber => x.value.toIntExact;
        case _ => throw new Exception("totalDevicesReturned must be a Json Number");
      }

      //val devices = new Array[WebboxDevice](totalDevicesReturned);

      val deviceJsArray: Seq[JsValue] = result \ "devices" match {
        case x: JsArray => x.value;
        case _ => throw new Exception("devices must be a json array");
      }

      val devices: Seq[WebboxDevice] = deviceJsArray.map((x: JsValue) => new WebboxDevice(x match {
        case y: JsObject => y;
        case _ => throw new Exception("each device must be a js object");
      }, this));

      assert(devices.length == totalDevicesReturned, "devices number must be the same as the mentioned number");

      devices;
    }

    promiseJson.map[Seq[WebboxDevice]](json2devices);
  }

  /*
  sample response:
  {
   "format":"JSON",
   "result":{
      "overview":[
         {
            "unit":"W",
            "meta":"GriPwr",
            "name":"GriPwr",
            "value":"0"
         },
         {
            "unit":"kWh",
            "meta":"GriEgyTdy",
            "name":"GriEgyTdy",
            "value":"0"
         },
         {
            "unit":"kWh",
            "meta":"GriEgyTot",
            "name":"GriEgyTot",
            "value":"113955.795"
         },
         {
            "unit":"",
            "meta":"OpStt",
            "name":"OpStt",
            "value":""
         },
         {
            "unit":"",
            "meta":"Msg",
            "name":"Msg",
            "value":""
         }
      ]
   },
   "proc":"GetPlantOverview",
   "version":"1.0",
   "id":"12551"
  }
  */
  def GetPlantOverview(): Future[WebboxPlantOverview] = {
    val (id, promiseJson) = grabIdAndPromiseJson(serverUrl, password, "GetPlantOverview");

    val json2plantOverview = (json: JsValue) => {
      val result: JsObject = grabResultFromJson(json, id);

      val overview: Seq[JsValue] = result \ "overview" match {
        case array: JsArray => array.value;
        case _ => throw new Exception("overview must be a JsArray !!");
      };

      val channels: Seq[WebboxChannel] = overview.map((jsVal: JsValue) => new WebboxChannel(jsVal match {
        case x: JsObject => x;
        case _ => throw new Exception("each jsval is expected to be an object");
      }));

      val instantaneousPower = channels(0);
      assert(
        instantaneousPower.meta == "GriPwr",
        "The meta of channel instantaneous power is not valid"
      );
      assert(
        instantaneousPower.unit.get == "W",
        "the unit of instantaneous power is expected to be in Watt: " + json.toString()
      );

      val dailyEnergy = channels(1);
      assert(
        dailyEnergy.meta == "GriEgyTdy",
        "The meta of channel daily energy is not valid"
      );
      assert(
        dailyEnergy.unit.get == "kWh",
        "the unit of daily energy is expected to be in kWh"
      );

      val totalEnergy = channels(2);
      assert(
        totalEnergy.meta == "GriEgyTot",
        "The meta of channel total energy is not valid"
      );
      assert(
        totalEnergy.unit.get == "kWh",
        "the unit of daily total is expected to be in kWh"
      );

      val status = channels(3);
      assert(
        status.meta == "OpStt",
        "The meta of channel status is not valid"
      );

      val errorMessage = channels(4);
      assert(
        errorMessage.meta == "Msg",
        "The meta of channel error message is not valid"
      );

      new WebboxPlantOverview(
        instantaneousPower.value,
        dailyEnergy.value,
        totalEnergy.value,
        status.value,
        errorMessage.value
      );
    };

    promiseJson.map[WebboxPlantOverview](json2plantOverview);
  }
}
