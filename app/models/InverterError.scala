package models


import play.api.Play.current
import io.Source
import components.{InverterErrorSeverity, InverterCompany}

object InverterError {
  val errorsPath = "private/errors";
  val delimiter = ";";

  def getErrors(company: InverterCompany.Value): List[InverterError] = {
    val path = current.path.getCanonicalPath + "/" + errorsPath + "/" + company + ".csv";

    val lines = Source.fromFile(path).getLines().toList;

    //this is neglected
    val titleLine = lines.head;

    val dataLines = lines.tail.map(_.trim);

    val dataArrays: List[Array[String]] = dataLines.map(dataLine => dataLine.split(delimiter));

    company match {
      case InverterCompany.SMA => dataArrays.map(
        dataArray => new SmaInverterError(
          code = dataArray(1),
          description = dataArray(2),
          severity = InverterErrorSeverity.withName(dataArray(3)),
          correctiveMeasure = dataArray(4)
        )
      );
    }
  }
}

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
sealed abstract class InverterError(
                                     val code: String,
                                     val description: String,
                                     val severity: InverterErrorSeverity.Value
                                     ) {
  require(!code.isEmpty && !description.isEmpty);
}

class SmaInverterError(
                        code: String,
                        description: String,
                        severity: InverterErrorSeverity.Value,
                        val correctiveMeasure: String
                        ) extends InverterError(code, description, severity);