package models

import scala.concurrent.duration._
import components.DreamhostPligorAuth

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
object SunsheriffsConfig {
  //val waitAfterSunset = 10.minutes;
  val waitAfterSunset = 1.hour

  //val hurryBeforeSunrise = Duration.Zero;
  val hurryBeforeSunrise = 1.hour

  val defaultLang = "en"

  val generalEmail = "pligor@sunsheriffs.com"

  //val monitoringEmail = "monitoring@sunsheriffs.com";
  val monitoringEmail = DreamhostPligorAuth.user

  val recommenedEmailClientLink = "http://www.mozilla.org/thunderbird/"

  //TODO get the hostname (recursively) from request (request.host) and get rid of this static variable
  val hostname = "162.13.123.133"
}
