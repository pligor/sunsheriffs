package models

import components.AutoIncrement
import anorm.SqlParser._
import java.util.Date
import anorm.~

object WebboxPlantOverview extends AutoIncrement {
  private val anormParser = get[Date]("timeRecorded") ~ get[java.math.BigDecimal]("instantaneousPower") ~ get[java.math.BigDecimal]("income");
  val parser = anormParser.map {
    case parsedT ~ parsedPower ~ parsedIncome => new InstantOutput(
      t = parsedT,
      power = parsedPower,
      income = parsedIncome
    );
  }

  private val invalidEnergyOrPower = -1.0.toString;

  val invalidOverview = WebboxPlantOverview(
    instantaneousPower = invalidEnergyOrPower,
    dailyEnergy = invalidEnergyOrPower,
    totalEnergy = invalidEnergyOrPower,
    status = "",
    errorMessage = ""
  );
}

class WebboxPlantOverviewModel(
                                val timeRecorded: Date,
                                instantaneousPower: String, //by default these are vals and NOT vars
                                dailyEnergy: String,
                                totalEnergy: String,
                                status: String,
                                errorMessage: String,
                                val id: Option[Long] = None
                                ) extends WebboxPlantOverview(instantaneousPower, dailyEnergy, totalEnergy, status, errorMessage) {
  def this(timeRecorded: Date, overview: WebboxPlantOverview, id: Option[Long] = None) = this(
    timeRecorded,
    overview.instantaneousPower,
    overview.dailyEnergy,
    overview.totalEnergy,
    overview.status,
    overview.errorMessage,
    id
  );

  override def toMap = super.toMap ++ Map("timeRecorded" -> timeRecorded);
}

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
case class WebboxPlantOverview(
                                instantaneousPower: String, //by default these are vals and NOT vars
                                dailyEnergy: String,
                                totalEnergy: String,
                                status: String,
                                errorMessage: String
                                ) {
  def toMap = Map[String, Any](
    "instantaneousPower" -> instantaneousPower,
    "dailyEnergy" -> dailyEnergy,
    "totalEnergy" -> totalEnergy,
    "status" -> status,
    "errorMessage" -> errorMessage
  );
}
