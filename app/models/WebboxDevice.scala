package models

import play.api.libs.json._
import anorm.SqlParser._
import play.api.libs.json.JsArray
import play.api.libs.json.JsUndefined
import play.api.libs.json.JsObject
import play.api.libs.json.JsString
import scala.Some
import anorm.{~, Pk}
import components.MyModel
import java.sql.Connection
import scala.concurrent.{ExecutionContext, Future}
import ExecutionContext.Implicits.global

object WebboxDevice {
  val anormParser = get[Pk[Long]]("id") ~
    get[Long]("Webbox_MonitoringSystem_id") ~
    get[String]("deviceKey") ~
    get[Option[String]]("name") ~ //FOR NULLABLE COLUMNS ALWAYS USE OPTION[type]
    get[Option[Int]]("type");

  def parser(implicit connection: Connection) = anormParser.map {
    case parsedId ~ parsedWebboxId ~ parsedDeviceKey ~ parsedName ~ parsedType =>
      new WebboxDevice(
        webbox = Webbox.getById(parsedWebboxId).get,
        key = parsedDeviceKey,
        name = parsedName,
        id = parsedId.toOption
      ); //TODO parsed type is not used yet, please include in the future (the column is nullable)
  }
}

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 * Describes a device within the system (e.g. Sunny Boy, Sunny Sensor Box)
 */
/*
{
  "key": "string",
  "name": "string" or null (optional),
  "channels": [array] or null (optional),
  "children": [array] or null (optional),
}
*/
//class WebboxDevice(val obj: JsObject, val webbox: Webbox) extends WebboxClient {
//TODO change channels and children devices to real objects
class WebboxDevice(
                    val webbox: Webbox,

                    val key: String, //A unique device key (e.g. "SB21TL06:2000106925")

                    //The user-defined name of the device (e.g. "Inverter left"). Specification of the element is
                    //optional. If the element is specified but no name is assigned, null is entered here
                    val name: Option[String] = None,

                    //An array of channel objects of the device. Specification of the element is optional. If the
                    //element is specified but no channels exist, null is entered here
                    val channels: Option[Seq[JsValue]] = None,

                    //An array of objects of hierarchically subordinate devices. Specification of the element is
                    //optional. If the element is specified but no sub-devices exist, null is entered here
                    val children: Option[Seq[JsValue]] = None,

                    id: Option[Long] = None
                    ) extends MyModel(id) with WebboxClient {
  require(webbox != null);

  def this(obj: JsObject, itsWebbox: Webbox) = this(
    itsWebbox,
    obj \ "key" match {
      case x: JsString => x.value;
      case _ => throw new ClassCastException(obj.toString());
    },
    obj \ "name" match {
      case x: JsString => Some(x.value);
      case JsNull => None;
      case y: JsUndefined => None; //if not found
      case _ => throw new ClassCastException(obj.toString());
    },
    obj \ "channels" match {
      case x: JsArray => Some(x.value);
      case JsNull => None;
      case y: JsUndefined => None; //if not found
      case _ => throw new ClassCastException(obj.toString());
    },
    obj \ "children" match {
      case x: JsArray => Some(x.value);
      case JsNull => None;
      case y: JsUndefined => None; //if not found
      case _ => throw new ClassCastException(obj.toString());
    }
  );

  /*
  sample request:
  RPC={
    "version": "1.0",
    "proc": "GetParameterChannels",
    "id": "101",
    "format": "JSON",
    "passwd": "a289fa4252ed5af8e3e9f9bee545c172",
    "params": {
      "device": "SENS0700:26680"
    }
  }
  sample response:
  {
   "format":"JSON",
   "result":{
      "SENS0700:26680":[
         "DevNam",
         "DevRs",
         "ExlSolIrrCal",
         "ExlSolIrrFnc",
         "FwVer",
         "HwVer",
         "RS485Dl",
         "SmaNetBd",
         "SN",
         "TmpUnit",
         "WindUnit"
      ]
   },
   "proc":"GetParameterChannels",
   "version":"1.0",
   "id":"101"
  }
  */
  def GetParameterChannels: Future[Seq[String]] = {
    val (id, promiseJson) = grabIdAndPromiseJson(
      webbox.serverUrl,
      webbox.password,
      "GetParameterChannels",
      JsObject(Seq("device" -> JsString(key)))
    );

    val json2strings: (JsValue) => Seq[String] = (json: JsValue) => {
      val result: JsObject = grabResultFromJson(json, id);

      val paramChannels: Seq[String] = result \ key match {
        case x: JsArray => x.value.map(_ match {
          case x: JsString => x.value;
          case _ => throw new Exception("each parameter channel must be a string");
        })
        case _ => throw new Exception("for the device " + key + " we should have got an array of parameter channels");
      }

      paramChannels;
    }

    promiseJson.map[Seq[String]](json2strings);
  }

  def GetParameter: Future[Seq[WebboxDeviceDataResponse]] = {
    webbox.GetParameter(List[String](key));
  }

  def GetParameter(channelStrings: Seq[String]): Future[Seq[WebboxDeviceDataResponse]] = {
    webbox.GetParameter(Seq(WebboxDeviceDataRequest(key, channelStrings)));
  }

  def GetProcessData: Future[Seq[WebboxDeviceDataResponse]] = {
    webbox.GetProcessData(List[String](key));
  }

  def GetProcessData(channelStrings: Seq[String]): Future[Seq[WebboxDeviceDataResponse]] = {
    webbox.GetProcessData(Seq(WebboxDeviceDataRequest(key, channelStrings)));
  }

  /*
  sample requests:
  RPC={"version": "1.0","proc": "GetProcessDataChannels","id": "1","format": "JSON","params":{"device": "SENS0700:26680"}}
  RPC={"version": "1.0","proc": "GetProcessDataChannels","id": "1","format": "JSON","params":{"device": "WRTP2S42:2110281158"}}
   */
  def GetProcessDataChannels() = {
    /*
    {
       "format":"JSON",
       "result":{
          "SENS0700:26680":[
             "ExlSolIrr",
             "IntSolIrr",
             "SMA-h-On",
             "TmpAmb C",
             "TmpMdul C",
             "WindVel m/s"
          ]
       },
       "proc":"GetProcessDataChannels",
       "version":"1.0",
       "id":"1"
    }
    */
    val (id, promiseJson) = grabIdAndPromiseJson(
      webbox.serverUrl,
      webbox.password,
      "GetProcessDataChannels",
      JsObject(Seq("device" -> JsString(key)))
    );

    val json2strings: (JsValue) => Seq[String] = (json: JsValue) => {
      val result: JsObject = grabResultFromJson(json, id);

      val channelJsons: Seq[JsValue] = (result \ key) match {
        case x: JsArray => x.value;
        case _ => throw new Exception("for the device " + key + " we should have got an array of data channels");
      }

      channelJsons.map(_ match {
        case x: JsString => x.value;
        case _ => throw new Exception("each process data channel must be a string");
      });
    }
    promiseJson.map[Seq[String]](json2strings);
  }
}
