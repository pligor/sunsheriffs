package controllers

import play.api.mvc._
import akka.actor.{Props, ActorSystem, Cancellable}
import play.api.db.DB
import play.api.Play.current
import java.io.File
import generic.FileHelper
import FileHelper._
import components.helpers.EmailHelper._
import components._
import actorMessages.PopEmails
import models.OverviewObject
import components.helpers.AnormHelper._
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext
import ExecutionContext.Implicits.global

object Application extends Controller {

  def initializeDBfromFixtures() = Action {
    withNoForeignKeys {
      implicit connection =>
        FixtureLoad();
    }
    Ok("DB has been reset according to Fixtures");
  }

  def includeRecording(webboxId: Long) = Action {
    try {
      val prevLen: Int = DB.withConnection {
        implicit connection =>
          OverviewObject.includeRecording(webboxId);
      }
      Ok("tried to add so monitored webboxes prev length is: " + prevLen + ", and now is: " +
        //OverviewObject.monitoredWebboxes.length
        OverviewObject.monitoredWebboxes.size
      );
    }
    catch {
      case e: Exception => BadRequest("why stop monitoring a webbox when you are not recording it at all?");
    }
  }

  def excludeRecording(webboxId: Long) = Action {
    try {
      val prevLen = OverviewObject.excludeRecording(webboxId);
      Ok("tried to remove so monitored webboxes prev length is: " + prevLen + ", and now is: " +
        //OverviewObject.monitoredWebboxes.length
        OverviewObject.monitoredWebboxes.size
      );
    }
    catch {
      case e: Exception => BadRequest("why stop monitoring a webbox when we are not recording at all?");
    }
  }

  def recordingStart = Action {
    implicit request =>
      if (OverviewObject.scheduleToRecordAllWebboxes()) {
        Ok("recording started and all are scheduled");
      }
      else {
        BadRequest("you tried to start a procedure that is already started");
      }
  }

  def recordingStartImmediate = Action {
    implicit request =>
      if (OverviewObject.startRecordingAllWebboxes()) {
        Ok("recording started immediately");
      }
      else {
        BadRequest("you tried to start a procedure that is already started");
      }
  }

  def recordingCancel = Action {
    if (OverviewObject.cancelRecording()) {
      Ok("recording cancelled");
    }
    else {
      BadRequest("you tried to cancel a procedure that is not started")
    }
  }

  var emailCancellor: Cancellable = null;

  def startEmailFetching = Action {
    implicit request =>
    ///*
      if (emailCancellor == null || emailCancellor.isCancelled) {
        val curAkkaSystem = ActorSystem("EmailSystem");

        val emailWorker = curAkkaSystem.actorOf(Props(
          new EmailFetcher(DreamhostPligor, Some(DreamhostPligorAuth)) with Forward
        ), name = "emailWorker");

        //val frequency = 60.seconds;
        val frequency = 5.minutes;
        val initialDelay = Duration.Zero;

        emailCancellor = curAkkaSystem.scheduler.schedule(
          initialDelay,
          frequency,
          emailWorker,
          PopEmails()
        );
      }
      Ok("email fetching started");
  }

  def cancelEmailFetching = Action {
    if (emailCancellor != null && !emailCancellor.isCancelled) {
      emailCancellor.cancel();
      assert(emailCancellor.isCancelled);
      Ok("email fetching cancelled");
    }
    else {
      BadRequest("you tried to cancel a procedure that is not started");
    }
  }

  def index = Action {
    //jsonResponse.value.get.toString();
    /*
        Async {
          // Async { } is an helper method that builds an AsyncResult from a Promise[Result]
          promiseJson.map[Result](json => {
            if (error != JsNull) {
              ExpectationFailed("there was an error in the response: " + error.toString());
            }
            else {
              Ok(res toString);
            }
          });
        }*/
    Ok(views.html.index("hello babe"));
  }

  def compressEmails(delete: Boolean) = Action {
    val emailPath = current.path.getCanonicalPath + "/" + emailsRelativePath;

    val emailFolder = new File(emailPath);

    val emailFiles = emailFolder.listFiles((dir: File, name: String) => name.takeRight(4) == ".xml");

    if (emailFiles.isEmpty) {
      NotFound("I could not find any xml files in the predefined folder");
    }
    else {
      val zipFilename = "emails.zip";
      val zipFilepath = emailPath + "/" + zipFilename;

      compress(zipFilepath, emailFiles.toList);

      if (delete) {
        emailFiles.foreach {
          emailFile =>
            assert(emailFile.delete());
        }
      }

      Ok.sendFile(
        content = new File(zipFilepath),
        fileName = (_) => zipFilename
      );
    }
  }

  /**
   * @deprecated
   * @return
   */
  /*def recordingLimitsEnable = Action {
    //cancel both just to be sure that we start a normal procedure since the one schedules the other
    cancelScheduleSafely(OverviewObject.overviewStartupCancellor);
    cancelScheduleSafely(OverviewObject.overviewShutdownCancellor);

    if (isScheduled(OverviewObject.overviewStartupCancellor) ||
      isScheduled(OverviewObject.overviewShutdownCancellor)
    ) {
      InternalServerError("!you are trying to enable recording limits while they are already enabled");
    }
    else {
      OverviewObject.scheduleStartupOrShutdown(Akka.system);

      Ok("the recording limits are enabled, now we record from morning until certain hour at night");
    }
  }*/

  /**
   * @deprecated
   * @return
   */
  /*def recordingLimitsDisable = Action {
    cancelScheduleSafely(OverviewObject.overviewStartupCancellor);
    cancelScheduleSafely(OverviewObject.overviewShutdownCancellor);
    Ok("the recording limits are disabled, we now stay to the state where we are left, either recording or not");
  }*/
}