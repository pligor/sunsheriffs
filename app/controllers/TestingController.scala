package controllers

import play.api.mvc._
import components._
import play.api.db.DB
import anorm._
import play.api.Play.current
import models.PvPlant
import anorm.SqlParser._
import java.util.Date
import generic.TimeHelper._
import java.io.File
import generic.FileHelper
import FileHelper._
import java.text.SimpleDateFormat
import play.api.i18n.Lang
import myplay.M
import play.api.libs.ws.WS
import com.ning.http.client.Realm
import akka.actor.{Actor, ActorRef, Props, ActorSystem}
import play.api.Logger
import play.api.libs.ws.WS.WSRequestHolder
import akka.pattern.{AskTimeoutException, ask}
import akka.util.Timeout
import scala.concurrent.duration._
import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
object TestingController extends Controller {
  def testakka = Action {
    implicit request =>
      implicit val timeout = Timeout(5.seconds)

      val system = ActorSystem("TheSystem")
      val actorRef = system.actorOf(Props(new Actor {
        def receive = {
          case _ => //nop
        }
      }), "AnonymousActor")

      Logger debug actorRef.path.toString //prints: akka://TheSystem/user/AnonymousActor

      Logger debug actorRef.path.elements.mkString(start = "/", sep = "/", end = "")

      Logger debug actorRef.path.name

      /*val system = ActorSystem("PlaneSimulation")

      val plane = system.actorOf(Props[Plane], "Plane")

      val control = Await.result((plane ? Plane.GiveMeControl).mapTo[ActorRef], 5.seconds)

      //take off
      system.scheduler.scheduleOnce(200.millis) {
        control ! ControlSurfaces.StickBack(1f)
      }

      //level out
      system.scheduler.scheduleOnce(1.seconds) {
        control ! ControlSurfaces.StickBack(0f)
      }

      //climb
      system.scheduler.scheduleOnce(3.seconds) {
        control ! ControlSurfaces.StickBack(0.5f)
      }

      //level out
      system.scheduler.scheduleOnce(4.seconds) {
        control ! ControlSurfaces.StickBack(0f)
      }

      //shut down
      system.scheduler.scheduleOnce(5.seconds) {
        system.shutdown()
      }*/

      /*val system = ActorSystem("MyActors")

      implicit val askTimeout = Timeout(1.second)

      //Props gives us a way to modify certain aspects of an Actor's structure
      val actorRef = system.actorOf(Props[MyActor])

      val message = Alpha(Beta("beta 1", Gamma("rays")), Beta("beta rays", Gamma("Dangerous")))

      //actorRef ! message
      try {
        val question = actorRef ? message

        val answer = Await.result(question, 5.seconds)
      } catch {
        case e: AskTimeoutException => Logger info "the ask has timed out"
      }


      Logger debug "Hi jaime"*/

      /*val system = ActorSystem("BadShakespearean")
      val actor = system.actorOf(Props[BadShakespeareanActor], "Shake")

      def send(msg: String) {
        Logger info s"Me: $msg"
        actor ! msg
        Thread.sleep(100)
      }

      send("Good Morning")
      send("You're terrible")
      system.shutdown()*/

      Ok("check logs")
  }

  def bingsearchapi = Action {
    implicit request =>
      def writeToFile(str: String) {
        printToFile(new File("text.text.txt"))({
          writer =>
            writer.write(str)
        })
      }

      /*future {
        Thread.sleep(5000)
        writeToFile("hello")
      }*/

      /*val myFuture = future {
        Thread.sleep(5000)
        val str = "inside future"
        println(str)
        Logger error str
        writeToFile(str)
      }

      myFuture.map {
        dummy =>
          val str = "inside map"
          println(str)
          Logger error str
          writeToFile(str)
      }.onComplete {
        case scala.util.Success(dummyUnit) => {
          val str = "inside success"
          println(str)
          Logger error str
          writeToFile(str)
        }
        case Failure(ex) => {
          val str = "inside failure"
          println(str)
          Logger error str
          writeToFile(str)
        }
      }*/

      object BingSearchApiEntities extends Enumeration {
        val Web = Value
        val Image = Value
        //might be more but I dont care right now
      }

      val rootUri = "https://api.datamarket.azure.com/Bing/Search"

      val accountKey = "6dPMoIcQiXj5OW025j7ctezlVox60Kv6t7F8myWf7wU="

      val url = rootUri + "/" + BingSearchApiEntities.Image.toString

      val wsRequestHolder: WS.WSRequestHolder = WSRequestHolder(
        url = url,
        headers = Map.empty[String, Seq[String]],
        queryString = Map[String, Seq[String]](
          "Query" -> Seq("\'vodafone\'"),
          "$format" -> Seq("json"),
          "$top" -> Seq("2")
        ),
        calc = None,
        auth = Some((accountKey, accountKey, Realm.AuthScheme.BASIC)),
        followRedirects = None, //configures the client to follow 301 and 302 redirects
        timeout = None, //sets both the connection and request timeout in milliseconds
        virtualHost = None //???
      ) /*.withQueryString(
        "Query" -> "\'vodafone\'",
        "$format" -> "json"
      )*/

      /*wsRequestHolder.get().onComplete {
        case scala.util.Success(response) => {
          val successString = "on success"
          println(successString)
          Logger error successString

          val body = response.body
          Logger debug body

          //val json = response.json.toString()
          //println(json)
          //Logger debug json

          writeToFile(body)
        }
        case Failure(ex) => {
          val failString = s"failed with  message ${ex.getMessage}"
          println(failString)
          Logger error failString
        }
      }*/

      Ok(wsRequestHolder.url)
  }

  def justtest = Action {
    implicit request =>
      val curdate = DB.withConnection {
        implicit connection =>
          SQL("SELECT NOW()").as(scalar[Date].single)
      }

      Ok("MySQL NOW(): " + curdate.toString + "\nGMT: " + new Date + "\n" + M("this is my home"))
  }

  def emailpreview = Action {
    implicit request =>
      val sdf = new SimpleDateFormat("EEEE, dd 'of' MMMM yyyy")

      import components.helpers.AnormHelper._

      withNoForeignKeys("test") {
        implicit connection =>
          FixtureLoad();
      }

      val plant1 = DB.withConnection("test") {
        implicit connection =>
          PvPlant.getById(1);
      }
      val plant2 = DB.withConnection("test") {
        implicit connection =>
          PvPlant.getById(2);
      }

      val images = Seq[String]("sample_image_chart1.jpeg", "sample_image_chart2.png", "sample_image_chart3.png");

      //val chartImages = images.map(image => Hash.genUUID -> image).toMap;
      val chartImages = images.map(image => image -> image).toMap;

      val lang = Lang("el");

      val html = DB.withConnection("test") {
        implicit connection =>
          views.html.dailyEmail(
            absoluteLinkPrefix = "http://" + request.host, //TODO later check for https
            dateToday = sdf.format(new Date),
            usernameKlitikh = "Κουμουτσάκο",
            timeSent = "18:30",
            plants = Seq[PvPlant](
              plant1.get,
              plant2.get
            ),
            dailyEnergy = 1500.241,
            totalEnergy = 591534.12526,
            dailyProfit = 25.04,
            embeddedImages = chartImages
          )(debug = true);
      }

      Ok(html);
  }

  def imgconv = Action {
    implicit request =>
      Ok(views.html.imgconv());
  }

  def rickshaw = Action {
    implicit request =>
      Ok(views.html.rickshawEmail());
  }

  def displaypng = Action {
    implicit request =>
      val byteArray: Array[Byte] = fileToByteArray(new File("/home/pligor/Desktop/tiger_eye.png"));
      Ok(byteArray).as("image/png")
  }

  def testCurl = Action {
    implicit request =>
      val res = new StringBuffer

      res append "\n\n"
      res append "query string: " + request.queryString
      res append "\n\n"
      res append "method: " + request.method
      res append "\n\n"
      res append "host: " + request.host
      res append "\n\n"


      res append "body as raw: " + request.body.asRaw
      res append "\n\n"

      res append "body as text: " + request.body.asText
      res append "\n\n"

      res append "body as form data: " + request.body.asFormUrlEncoded
      res append "\n\n"

      Ok(res toString)
  }
}
