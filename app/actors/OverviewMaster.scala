package actors

import akka.actor.{ActorLogging, Props, Actor}
import akka.routing.RoundRobinRouter
import collection.mutable
import models.{OverviewObject, Webbox}
import actorMessages.{ResultOverview, StoreDataOverview, RetrieveOverview}
import scala.concurrent.duration._

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
class OverviewMaster(numberOfWorkers: Int /*, webboxes: List[Webbox]*/
                     //, listener: ActorRef //this is used to report to the outside world
                      ) extends Actor with ActorLogging {

  //context means that the actors are being created as children of the current actor (there is a hierarchy of actors)
  val overviewLogWorkerRouter = context.actorOf(
    Props[OverviewLogWorker].withRouter(RoundRobinRouter(numberOfWorkers)), name = "overviewLogWorkerRouter"
  );

  var ids = mutable.Set.empty[Long];
  var timeStarted: Long = _;
  var webboxes: List[Webbox] = _;

  def receive = {
    //case RetrieveOverview(f) => {
    case RetrieveOverview(f /*, webboxes2monitor*/) => {
      //sender ! List[Long](1L,38L,-223L);
      //initialize webboxes
      webboxes = OverviewObject.monitoredWebboxes.toList;

      timeStarted = System.currentTimeMillis();
      webboxes.foreach(overviewLogWorkerRouter ! StoreDataOverview(_, f, sender));
    }
    case ResultOverview(id, origSender) => {
      //log info "new row with id: " + id;

      ids += id; //remember if the id already exists then it wont be added to the Set

      if (webboxes.length == ids.size) {
        //TODO probably this calculation does not makes much sense because the webboxes List could change any time!
        log info "Calculation time:\t%s".format((System.currentTimeMillis() - timeStarted).millis);
        origSender ! ids.toList //convert to list because we must send something immutable!!
        ids = mutable.Set.empty[Long];
        //context.stop(self);
        //context.system.shutdown();
      }
    }
  }
}
