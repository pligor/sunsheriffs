package actors

import akka.actor._
import akka.pattern.ask
import generic.TimeHelper._
import play.api.db.DB
import play.api.Play.current
import components.helpers.AkkaHelper._
import play.api.libs.concurrent._
import java.sql.Connection
import java.util.Date
import jodd.mail.Email
import jodd.mail.EmailAttachment.attachment
import components.helpers.EmailHelper._
import play.api.i18n.Lang
import actorMessages._
import models._
import components.{DreamhostPligor, DreamhostPligorAuth, TwilioClient}
import scala.concurrent.ExecutionContext
import ExecutionContext.Implicits.global
import akka.util.Timeout
import java.util.concurrent.TimeUnit
import myplay.M
import scala.Some
import actorMessages.ResultOverview
import actorMessages.StoreDataOverview
import actorMessages.CountingDown

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
class OverviewLogWorker extends Actor with ActorLogging {
  def receive = {
    case StoreDataOverview(monitor, f, origSender) => {
      val (overviewId, instantaneousPower) = DB.withConnection(implicit connection => grabOverview(monitor, f));

      val afterSunset = DB.withConnection(implicit connection => isAfterSunset(monitor));

      if (afterSunset) {
        //TODO to issue edw einai pws exontas ena ektos syndesis monitoring system exoume ws apotelesma
        //TODO na mhn telionei pote to sistima epeidi den vlepoume midenika (stin isxy), para vlepoume errors
        implicit val timeout = Timeout(PowerOffChecker.timeout.toMillis, TimeUnit.MILLISECONDS);
        if (instantaneousPower == 0) {
          //create or get and count down until zero
          val powerOffChecker = getOrCreatePowerOffChecker(monitor);
          val messagePromise = (powerOffChecker ? ZeroPowerRecorded).mapTo[PowerOffCheckerMessage];
          val message = messagePromise.await.get; //normally there is nothing that will delay this

          message match {
            case CountingDown(count) => log info "counting down..." + count;
            case PowerOffLongEnough => {
              //cache this value here before excluding
              val isTheOnlyMonitoredSystemForItsUser = DB.withConnection(
                implicit connection =>
                  EnergySheriff.isTheOnlyMonitoredSystemForItsUser(monitor)
              )

              excludeWebbox(monitor);
              DB.withConnection(implicit connection => OverviewObject.scheduleWakeup(monitor, context.system));

              if (isTheOnlyMonitoredSystemForItsUser) {
                DB.withConnection {
                  implicit connection =>
                  //TODO maybe make this a method of Energy Sheriff object
                    val user: User = monitor.getUser.get

                    implicit val lang = user.getLang

                    val nowGMT = new Date

                    val (dailyEnergy, dailyProfit) = user.getDailyEnergyAndProfit(nowGMT)

                    val cellphone = user.getCellphone

                    log info "sending sms to phone: " + cellphone.getNationalNumber

                    val sms = TwilioPligor.sendSMS(
                      TwilioPligor.primaryPhoneNumber,
                      cellphone,
                      DailySMS.generate(dailyProfit, dailyEnergy)
                    )

                    if (sms.getStatus != TwilioClient.successSentStatus) {
                      //TODO report sms not sent or failure to send
                      log.info("sms not sent to phone: " + cellphone.getNationalNumber);
                    }

                    emailDailyReport(
                      date = nowGMT,
                      user = user,
                      dailyEnergy = dailyEnergy,
                      dailyProfit = dailyProfit
                    );

                    //TODO later we should save the data in files instead of deleting them
                    user.deleteAllPlantsDailyData(nowGMT);

                  /*val dailyEnergyOption = monitor.getDailyEnergy();
                  if (dailyEnergyOption.isDefined) {
                    val dailyEnergy = dailyEnergyOption.get;
                    val targetEmailOption = monitor.getForwardEmail;
                    if (targetEmailOption.isDefined) {
                      val email = Email.create().
                        from(DreamhostPligorAuth.user).
                        to(targetEmailOption.get).
                        subject("Daily Energy").
                        addText("Daily Energy is: " + dailyEnergy + " kWh");
                      val smtpServer = DreamhostPligor.getSmtpServer(Some(DreamhostPligorAuth));
                      sendSingleMail(email, smtpServer);
                    }
                    val statusPromise = TwilioPligor.sendSMS_old(
                      TwilioPligor.primaryPhoneNumber,
                      PhoneNumberUtil.getInstance().parse("6956775925", "GR"),
                      DailySMS.generate(dailyEnergy)
                    );
                    val status: String = statusPromise.orTimeout("timeout after " + TwilioClient.timeout.toSeconds,
                      TwilioClient.timeout.toSeconds, TimeUnit.SECONDS
                    ).map(
                      eitherStatusOrTimeout =>
                        eitherStatusOrTimeout.fold(
                          (status: String) => status,
                          (timeout: String) => {
                            //TO DO log error: timeout
                            TwilioClient.invalidSentStatus
                          }
                        )
                    ).value.fold(
                      (onError: Throwable) => {
                        //TO DO log error: connection or someother kind
                        TwilioClient.invalidSentStatus
                      },
                      (onSuccess: String) => {
                        if (onSuccess != TwilioClient.successSentStatus) {
                          //TO DO log error: the status is not what was expected
                        }
                        onSuccess
                      }
                    );
                  }*/
                }
                //TODO implement more!
              }
            }
            case _ => throw new ClassCastException("we are not expecting any other messages");
          }
        }
        else {
          //kill if exists
          val powerOffChecker = getPowerOffChecker(monitor);
          if (isActorExistant(powerOffChecker)) {
            val messagePromise = (powerOffChecker ? TerminateEarly).mapTo[PowerOffCheckerMessage];
            val message = messagePromise.await.get;
            assert(message == VerifyingEarlyTermination);
          }
        }
      }

      sender ! ResultOverview(overviewId, origSender); //reply to sender
    }
  }

  private def emailDailyReport(date: Date, user: User, dailyEnergy: BigDecimal, dailyProfit: BigDecimal)(implicit connection: Connection, lang: Lang) {
    val chartImageFiles = user.getDailyChartImageFiles(date);

    val embeddedImages = chartImageFiles.map(file => {
      val name = file.getName;
      name -> name
    }).toMap;

    val dateToday = user.getDateToday(date);

    val totalEnergy = user.getTotalEnergy;

    val html = views.html.dailyEmail(
      absoluteLinkPrefix = "http://" + SunsheriffsConfig.hostname, //TODO later check for https
      dateToday = dateToday,
      usernameKlitikh = user.getUsernameKlitikh,
      timeSent = user.getTimeNow(date),
      plants = user.getPvPlants,
      dailyEnergy = dailyEnergy.toDouble,
      totalEnergy = totalEnergy.toDouble,
      dailyProfit = dailyProfit.toDouble,
      embeddedImages = embeddedImages
    )();

    val targetEmail = user.getEmailAccount.mail;
    log info "sending email to target mail: " + targetEmail;

    val email = Email.create().
      from(SunsheriffsConfig.monitoringEmail).
      to(targetEmail).
      subject(DailyReport.getEmailSubject(dateToday)).
      addText(M("please use an email client that supports html") + ": " + SunsheriffsConfig.recommenedEmailClientLink).
      addHtml(html.toString());

    for (chartImageFile <- chartImageFiles) {
      val fullpath = chartImageFile.getCanonicalPath;
      assert(chartImageFile.exists(), "all files we are about to embed must of course exist first");
      email.embed(attachment().setInline(true).file(fullpath));
    }

    sendSingleMail(email, DreamhostPligor.getSmtpServer(Some(DreamhostPligorAuth)));
  }

  private def excludeWebbox(monitor: Webbox) {
    val prevLen = OverviewObject.excludeRecording(monitor.id.get);
    assert(prevLen - OverviewObject.monitoredWebboxes.size == 1,
      "since only one webbox is excluded the length must be reduced by one");
  }

  def isAfterSunset(monitor: Webbox)(implicit connection: Connection): Boolean = {
    require(monitor.id.isDefined);
    val pos = monitor.getPvPlant.get.getGeoPosition.get;

    //TODO obviously this can be cached and NOT recalculated every fucking single time
    val sunset = getUTCsunset(pos.geoPosition);

    if (sunset.isDefined) {
      new Date().getTime > sunset.get.getTime + SunsheriffsConfig.waitAfterSunset.toMillis
    }
    else {
      //yes there is the possibility the sun will never set on some particular locations on the given date
      false; //keep recording since the sun never sets
    }
  }

  def getPowerOffChecker(monitor: Webbox): ActorRef = {
    //isActorExistant(context.system.actorFor(self.path / PowerOffChecker.getActorNameByMonitoringSystem(monitor)));  //alternative way mesw lamias
    //isActorExistant(context.actorFor(PowerOffChecker.getActorNameByMonitoringSystem(monitor)));

    //context.actorFor(PowerOffChecker.getActorNameByMonitoringSystem(monitor));
    val name = PowerOffChecker.getActorNameByMonitoringSystem(monitor);
    //path here is just a string
    //println(path);
    val path = Akka.system / name;
    Akka.system.actorFor(path);
    //PAROTI EPITREPETAI NA EXOUME actorFor( ena string ) tha prepei auto to string na einai valid path!
    //KAI OXI APLA TO ONOMA tou child tou actor
    //diladi autes oi dyo grammes einai ok:
    // val path = Akka.system / name;
    // Akka.system.actorFor(path)
    //auth h grammh einai lathos:
    // Akka.system.actorFor(name);
  }

  /**
   * BEWARE: What is happening here is that we have the 4 or more workers, and each one of them is a seperate actor....
   * and with RoundRobin they each take a turn. So if you create a child actor then you have.. 4 child actors :P
   * We need to fix this by creating the actor as a child of the parent which we know is only one! the OverviewMaster
   * Using the Akka.
   * @param monitor the webbox
   * @return
   */
  def getOrCreatePowerOffChecker(monitor: Webbox): ActorRef = getOrCreateActor(
    //context,
    Akka.system, //TODO change this to parent (this is only possible within parent. maybe send a message to parent or something..)
    name = PowerOffChecker.getActorNameByMonitoringSystem(monitor),
    props = Props(new PowerOffChecker(PowerOffChecker.commonPeriod))
  );

  private def grabOverview(monitor: Webbox, f: Int)(implicit connection: Connection): (Long, Double) = {
    val timeoutAwait = OverviewObject.getTimeout(f);
    val timeoutTimeout = timeoutAwait - 1;
    val timeunit = OverviewObject.frequencyTimeUnit;

    //practice has shown that timeout must be smaller than await duration. more info in PromiseTest.scala
    val overview: WebboxPlantOverview = monitor.GetPlantOverview().orTimeout("could not get overview", timeoutTimeout, timeunit).map(
      eitherModelOrString =>
        eitherModelOrString.fold(
          (overview: WebboxPlantOverview) => overview,
          (timeoutMessage: String) => {
            //TODO maybe log timeout later
            log info "reached timeout while getting overview";
            WebboxPlantOverview.invalidOverview;
          }
        )
    ).await(timeoutAwait, timeunit).fold(
      (onError: Throwable) => {
        //TODO log error
        log info "there was an exception thrown while getting overview";
        WebboxPlantOverview.invalidOverview
      },
      (onSuccess: WebboxPlantOverview) => {
        if (onSuccess == WebboxPlantOverview.invalidOverview) {
          //TODO log error
          log info "while overview is supposed to be successful, it equals with an invalid one. Impossible";
        }
        onSuccess
      }
    );

    //TODO MAYBE try to refactor that bitch to be able to set the invalid overview as zero energy power etc.

    val timeRecorded = new Date
    val overviewModel: WebboxPlantOverviewModel = new WebboxPlantOverviewModel(timeRecorded, overview)

    val newIdOption = monitor.insertNewWebboxPlantOverviewModel(overviewModel);
    assert(newIdOption.isDefined, "the insertion of a new webbox plant overview has no reason to fail at this point");

    (newIdOption.get, overviewModel.instantaneousPower.toDouble);
  }
}
