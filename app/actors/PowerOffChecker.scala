package actors

import akka.actor.{ActorLogging, Actor}
import models.{OverviewObject, Webbox}
import actorMessages._
import actorMessages.CountingDown
import scala.concurrent.duration._

object PowerOffChecker {
  //val commonPeriod = 1.minutes;
  val commonPeriod = 30.minutes

  val timeout = 1.seconds

  /**
   *
   * @param webbox we currently only use the webbox id
   * @return something like that: poweroffcheckerwebbox123
   */
  def getActorNameByMonitoringSystem(webbox: Webbox): String = {
    assert(webbox.id.isDefined)
    "poweroffcheckerwebbox" + webbox.id.get
  }
}

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
class PowerOffChecker(val consecutiveZerosPeriod: Duration) extends Actor with ActorLogging {
  //Rounded
  private var countdown: Int = (consecutiveZerosPeriod / OverviewObject.frequency).toInt;

  def receive = {
    case ZeroPowerRecorded => {
      //log.info("countdown before decrease: " + countdown);
      countdown -= 1; //decrease countdown
      //log.info("countdown after decrease: " + countdown);

      if (countdown <= 0) {
        sender ! PowerOffLongEnough;
        context.stop(self);
      }
      else {
        sender ! CountingDown(countdown);
      }
    }
    case TerminateEarly => {
      sender ! VerifyingEarlyTermination;
      context.stop(self);
    }
  }
}
