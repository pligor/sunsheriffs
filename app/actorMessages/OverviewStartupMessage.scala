package actorMessages

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
sealed trait OverviewStartupMessage;

case object StartupOverviewNow extends OverviewStartupMessage;