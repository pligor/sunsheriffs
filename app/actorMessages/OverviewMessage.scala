package actorMessages

import models.Webbox
import akka.actor.ActorRef

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
sealed trait OverviewMessage;

//case class RetrieveOverview(f: Int) extends OverviewMessage;
case class RetrieveOverview(f: Int/*, webboxes2monitor: List[Webbox]*/) extends OverviewMessage;

case class StoreDataOverview(monitor: Webbox, f: Int, origSender: ActorRef) extends OverviewMessage;

case class ResultOverview(id: Long, origSender: ActorRef) extends OverviewMessage;
