package actorMessages

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
sealed trait EmailMessage;

//case object PopEmails extends EmailMessage;
case class PopEmails(delete: Boolean = true) extends EmailMessage;

case class EmailsFetched(number: Option[Int]) extends EmailMessage;
