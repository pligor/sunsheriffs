package actorMessages

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
sealed trait OverviewShutdownMessage;

case object ShutdownOverviewNow extends OverviewShutdownMessage;
