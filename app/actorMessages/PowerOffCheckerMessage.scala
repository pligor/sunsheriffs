package actorMessages

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
sealed trait PowerOffCheckerMessage

//incoming
case object ZeroPowerRecorded extends PowerOffCheckerMessage

case object TerminateEarly extends PowerOffCheckerMessage

//outgoing
case class CountingDown(count: Int) extends PowerOffCheckerMessage

case object PowerOffLongEnough extends PowerOffCheckerMessage

case object VerifyingEarlyTermination extends PowerOffCheckerMessage;