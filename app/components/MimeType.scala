package components

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
object MimeType extends Enumeration {
  val TEXT_PLAIN = Value("text/plain");
}
