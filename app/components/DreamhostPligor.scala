package components

import models.EmailServer

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
object DreamhostPligor extends EmailServer(
  smtpServer = "mail.pligor.com",
  imapServer = Some("mail.pligor.com"),
  popServer = Some("mail.pligor.com"),
  imapPort = 143,
  popPort = 110,
  smtpPort = 587,
  isSmtpSSL = false,
  isImapSSL = false,
  isPopSSL = false
);
