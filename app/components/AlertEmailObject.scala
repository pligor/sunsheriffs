package components

import jodd.mail.ReceivedEmail
import models.AlertEmail
import java.sql.Connection

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
trait AlertEmailObject {
  def isEmailMine(email: ReceivedEmail): Boolean;

  def apply(email: ReceivedEmail)(implicit connection: Connection): Option[AlertEmail];
}
