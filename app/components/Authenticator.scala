package components

import javax.mail.PasswordAuthentication

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
abstract class Authenticator(val user: String, val pass: String) extends javax.mail.Authenticator {
  override def getPasswordAuthentication = new PasswordAuthentication(user, pass);
}
