package components.helpers

import javax.mail
import mail._
import components.Authenticator
import internet._
import javax.activation.{DataHandler, FileDataSource}
import java.io.File


/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 * http://www.javaworld.com/jw-10-2001/jw-1026-javamail.html?page=1
 * http://www.dzone.com/snippets/authenticate-smtp-server
 * http://www.jguru.com/faq/view.jsp?EID=30251
 * http://helpdesk.objects.com.au/java/how-to-embed-images-in-html-mail-using-javamail
 */
object JavaMailHelper {

  private object PropKeys extends scala.Enumeration {
    val host = Value("mail.smtp.host");
    val submitter = Value("mail.smtp.submitter");
    val auth = Value("mail.smtp.auth");
  }

  def sendHtmlMailWithEmbeddedImage(
                                     smtpServer: String,
                                     fromEmail: String,
                                     toEmail: String,
                                     subject: String,
                                     body: (String) => String,
                                     imagepath: String,
                                     auth: Option[Authenticator] = None
                                     ): Boolean = {
    val imagefile = new File(imagepath);
    require(imagefile.exists(), "the image file must already exist");

    val session = getSession(smtpServer, auth);
    val msg = initMimeMessage(session, fromEmail, toEmail, subject);

    val textPart = new MimeBodyPart();
    textPart.setText("please use an email client with html support in order to read this email");
    textPart.addHeader("Content-Type", "text/plain; charset=UTF-8; format=flowed");

    val imageFilename = imagefile.getName;

    val htmlPart = new MimeBodyPart();

    //val contentId = Hash.genUUID + "@pligor.com";
    //val contentId = "part1.03070908.07020202@pligor.com";
    //val contentId = imageFilename + "@pligor.com";
    val contentId = imageFilename + '@' + fromEmail.split('@').apply(1);

    //htmlPart.setText(body(contentId), "utf-8", "html");
    htmlPart.setText(body(contentId), "UTF-8", "html");

    val imagePart = new MimeBodyPart();
    val source = new FileDataSource(imagepath);
    imagePart.setDataHandler(new DataHandler(source));

    //headers for embedded images
    //TODO ti isxuei me ta autakia? mpainoun telika h oxi?
    imagePart.setHeader("Content-Type", "image/jpeg; name=\"" + imageFilename + "\"");
    imagePart.setContentID("<" + contentId + ">");
    //imagePart.setHeader("Content-Disposition", "inline; filename=\"" + imageFilename + "\"");
    imagePart.setDisposition(Part.INLINE);
    imagePart.setFileName(imageFilename);

    //The MIME definition for multipart/alternative recommends ordering the alternatives from the least sophisticated to the most sophisticated
    /*val alternativeMultiPartWithPlainTextAndHtml = new MimeMultipart("alternative");
    alternativeMultiPartWithPlainTextAndHtml.addBodyPart(textPart);
    alternativeMultiPartWithPlainTextAndHtml.addBodyPart(htmlPart);*/

    val rootContainer = new MimeMultipart();
    //rootContainer.addBodyPart(textPart);
    rootContainer.addBodyPart(htmlPart);
    //rootContainer.setSubType("related");
    rootContainer.addBodyPart(imagePart);

    msg.setContent(rootContainer);

    //true;
    sendMail(msg);
  }

  private def createInlineImagePart(base64EncodedImageContentByteArray: Array[Byte]): BodyPart = {
    val headers = new InternetHeaders();
    headers.addHeader("Content-Type", "image/jpeg");
    headers.addHeader("Content-Transfer-Encoding", "base64");

    val imagePart = new MimeBodyPart(headers, base64EncodedImageContentByteArray);
    imagePart.setDisposition(Part.INLINE);
    imagePart.setContentID("<image>");
    imagePart.setFileName("image.jpg");

    imagePart;
  }

  def sendHtmlMailWithAttachment(
                                  smtpServer: String,
                                  fromEmail: String,
                                  toEmail: String,
                                  subject: String,
                                  body: String,
                                  filepath: String,
                                  auth: Option[Authenticator] = None): Boolean = {
    val session = getSession(smtpServer, auth);
    val msg = initMimeMessage(session, fromEmail, toEmail, subject);

    //part one is the body
    val messageBodyPart = new MimeBodyPart();
    messageBodyPart.setText(body, "utf-8", "html");

    //part two is the attachment
    val attachmentPart = new MimeBodyPart();
    val source = new FileDataSource(filepath);
    attachmentPart.setDataHandler(new DataHandler(source));
    attachmentPart.setFileName(filepath);

    //combine the two above
    val multipart = new MimeMultipart();
    multipart.addBodyPart(messageBodyPart);
    multipart.addBodyPart(attachmentPart);

    //Put parts in message
    msg.setContent(multipart);

    sendMail(msg);
  }

  def sendHtmlMail(
                    smtpServer: String,
                    fromEmail: String,
                    toEmail: String,
                    subject: String,
                    body: String,
                    auth: Option[Authenticator] = None): Boolean = {

    val session = getSession(smtpServer, auth);
    val msg = initMimeMessage(session, fromEmail, toEmail, subject);
    //msg.setContent(body, "text/html; charset=utf-8");
    msg.setText(body, "utf-8", "html");
    sendMail(msg);
  }

  def sendPlainTextMail(
                         smtpServer: String,
                         fromEmail: String,
                         toEmail: String,
                         subject: String,
                         body: String,
                         auth: Option[Authenticator] = None): Boolean = {
    val session = getSession(smtpServer, auth);
    val msg = initMimeMessage(session, fromEmail, toEmail, subject);
    msg.setText(body);
    sendMail(msg);
  }

  private def initMimeMessage(
                               session: Session,
                               fromEmail: String,
                               toEmail: String,
                               subject: String
                               ): MimeMessage = {
    //Create new message
    val msg = new MimeMessage(session);

    //set 'from' and 'to' fields
    msg.setFrom(new InternetAddress(fromEmail));
    msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail, false).map(_.asInstanceOf[Address]));

    //set 'subject and 'body'
    msg.setSubject(subject);

    msg;
  }

  private def sendMail(msg: MimeMessage): Boolean = {
    try {
      //msg.setHeader("X-Mailer", "LOTONtechEmail");
      //msg.setSentDate(new Date());

      //send the message
      Transport.send(msg);

      true;
    }
    catch {
      case e: Exception => {
        e.printStackTrace();
        false;
      }
    }
  }

  private def getSession(smtpServer: String, authenticator: Option[Authenticator] = None): Session = {
    val props = System.getProperties;

    if (authenticator.isDefined) {
      val auth = authenticator.get;
      props.setProperty(PropKeys.submitter.toString, auth.user);
      props.setProperty(PropKeys.auth.toString, true.toString);
    }

    //props.put(PropKeys.host.toString, smtpServer);
    props.setProperty(PropKeys.host.toString, smtpServer);

    //properties.setProperty("mail.smtp.port", "25");

    Session.getDefaultInstance(props, authenticator.getOrElse(null));
  }
}