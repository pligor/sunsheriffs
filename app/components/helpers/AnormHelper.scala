package components.helpers

import anorm._
import java.sql.Connection
import play.api.db.DB
import play.api.Application

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
object AnormHelper {
  val MySQLTimestampFormat = "yyyy-MM-dd HH:mm:ss";

  def withNoForeignKeys[A](block: (Connection) => A)(implicit application: Application): A = {
    DB.withConnection {
      implicit connection =>
        noForeignKeys(block);
    }
  }

  def withNoForeignKeys[A](name: String)(block: (Connection) => A)(implicit application: Application): A = {
    DB.withConnection(name) {
      implicit connection =>
        noForeignKeys(block);
    }
  }

  private def noForeignKeys[A](block: (Connection) => A)(implicit connection: Connection): A = {
    disableAndStoreStateOfForeignKeys;
    val result = block(connection);
    restoreStateOfForeignKeys;
    result
  }

  private def disableAndStoreStateOfForeignKeys(implicit connection: Connection) {
    SQL("SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;").execute();
    SQL("SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;").execute();
    SQL("SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';").execute();
  }

  private def restoreStateOfForeignKeys(implicit connection: Connection) {
    SQL("SET SQL_MODE=@OLD_SQL_MODE;").execute();
    SQL("SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;").execute();
    SQL("SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;").execute();
  }

  /**
   * @param tableName no problem to alter a table which does not have an auto increment column, nothing happens
   */
  def resetAutoIncrement(tableName: String, based: Int = 1)(implicit connection: Connection) {
    SQL("ALTER TABLE " + tableName + " AUTO_INCREMENT = {based}").on("based" -> based).execute()(connection);
  }

  /**
   * @return Option[T] where T is the type of the primary key
   */
  def insertInto(tableName: String, row: Map[String, _])(implicit connection: Connection) = {
    //convert from Iterable to Sequence because otherwise the order of the elements is not consistent
    val keys = row.keys.toSeq;
    val columns = keys.mkString(",");
    val placeholders = keys.map("{" + _ + "}").mkString(",");

    val params: Seq[(Any, ParameterValue[_])] = row.map(
      (x: (String, _)) => x._1 -> toParameterValue(x._2)
    ).toSeq;

    val sqlString = "INSERT INTO " + tableName + "(" + columns + ") VALUES(" + placeholders + ")";
    //println(sqlString);
    SQL(sqlString).on(params: _*).executeInsert();
  }
}
