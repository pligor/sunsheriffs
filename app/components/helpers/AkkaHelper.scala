package components.helpers

import akka.actor._
import play.api.libs.concurrent._
import java.util.concurrent.TimeUnit
import components.{PromiseResultStatus, PromiseResult}
import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.duration.Duration
import ExecutionContext.Implicits.global

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
object AkkaHelper {
  /**
   * Be very aware of the assumption that adding 1 second to the timeout period is meaningless
   */
  def fetchPromise[A](future: Future[A],
                      timeoutDuration: Duration,
                      onTimeout: (String) => A,
                      onError: (Throwable) => A,
                      timeoutMessage: String = "") = {
    val oneSecMore = timeoutDuration + Duration(1, TimeUnit.SECONDS);
    future.orTimeout(timeoutMessage, timeoutDuration.toMillis, TimeUnit.MILLISECONDS).map(
      _.fold(
        (obj: A) => PromiseResult(obj, PromiseResultStatus.OK),
        (msg: String) => PromiseResult(onTimeout(msg), PromiseResultStatus.Timeout)
      )
    ).await(oneSecMore.toMillis, TimeUnit.MILLISECONDS).fold(
      (thrown: Throwable) => PromiseResult(onError(thrown), PromiseResultStatus.Timeout),
      (obj: PromiseResult[A]) => obj
    );
  }

  def isScheduled(cancellor: Cancellable): Boolean = cancellor != null && !cancellor.isCancelled;

  def cancelScheduleSafely(cancellor: Cancellable) {
    if (isScheduled(cancellor)) {
      cancellor.cancel();
    }
  }

  //only to be used inside an actor
  //remember props is by-name parameter
  def getOrCreateActor(context: ActorContext, name: String, props: => Props): ActorRef = {
    //    val childPath = context.self.path / name;
    //    val testRef: ActorRef = context.system.actorFor(childPath);
    val childPath = name;
    val testRef: ActorRef = context.actorFor(childPath);
    if (isActorExistant(testRef)) {
      testRef;
    }
    else {
      context.actorOf(props, name);
    }
  }

  //remember props is by-name parameter
  def getOrCreateActor(system: ActorSystem, name: String, props: => Props): ActorRef = {
    val childPath = system / name;
    val testRef: ActorRef = system.actorFor(childPath);
    //val childPath = "";
    //val testRef: ActorRef = system.actorFor(name);

    if (isActorExistant(testRef)) {
      //println("go");
      testRef;
    }
    else {
      //println("else " + childPath);
      system.actorOf(props, name);
    }
  }

  def isActorExistant(actorRef: ActorRef): Boolean = {
    !actorRef.isTerminated;
  }
}
