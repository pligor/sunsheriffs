package components.helpers

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
object StringHelper {
  /**
   * Get blocks of lines which are seperated from a delimiter line like many dashes --------------------
   * @param lines a list of strings
   * @return
   */
  def splitLinesByDelimiterLine(lines: List[String], delimiterLineChar: Char): List[List[String]] = {
    if (lines.isEmpty) {
      Nil;
    }
    else {
      assert(lines.head.forall(_ == delimiterLineChar));
      val (errorBlock, restLines) = lines.tail.span(line => !line.forall(_ == delimiterLineChar) || line.isEmpty);
      errorBlock :: splitLinesByDelimiterLine(restLines, delimiterLineChar);
    }
  }
}
