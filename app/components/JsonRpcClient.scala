package components

import play.api.libs.json._
import play.api.Logger
import play.api.libs.ws.WS
import play.api.libs.{json, ws}
import play.api.libs.ws.WS.WSRequestHolder
import play.api.libs.json.JsObject
import play.api.libs.json.JsString
import scala.concurrent.{ExecutionContext, Future}
import ExecutionContext.Implicits.global
import generic.Hash

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
class JsonRpcClient(protected val url: String, protected val debug: Boolean = false) {
  protected val name = "method";
  protected val maxIdLen = 32;

  protected def generateId(): String = {
    scala.util.Random.alphanumeric.take(maxIdLen).mkString;
  }

  protected def buildRequest(procedure: String, params: JsValue = null, notification: Boolean = false): JsObject = {
    new JsObject(Seq(
      "id" -> (if (notification) json.JsNull else JsString(generateId())),
      name -> JsString(procedure),
      //"params" -> (if (params == null) JsArray() else params)
      "params" -> (if (params == null) JsObject(Seq()) else params)
    ));
  }

  protected def grabRequestString(procedure: String, params: JsValue = null, notification: Boolean = false) = {
    val jsObj = buildRequest(procedure, params, notification);
    val id = jsObj \ "id" match {
      case x: JsString => x.value;
      case y: JsNull.type => null;
      case _ => null;
    };

    (id, jsObj.toString()) //returns a tuple
  }


  def execute(procedure: String, params: JsValue = null, notification: Boolean = false): (String, Option[Future[JsValue]]) = {

    val (id, request) = grabRequestString(procedure, params, notification);

    if (debug) {
      Logger info "***** Request *****" + "\n" +
        request + "\n" +
        "***** End Of request *****";
    }

    val wsRequest: WS.WSRequestHolder = WSRequestHolder(
      url,
      Map[String, Seq[String]]("Content-Type" -> Seq[String]("application/json")),
      Map.empty[String, Seq[String]],
      None,
      None,

      None,
      None,
      None
    );

    val promiseResponse: Future[ws.Response] = wsRequest.post(request);

    val responseOption = if (notification) {
      None
    }
    else {
      Some(
        promiseResponse.map[JsValue]((response: ws.Response) => {
          if (debug) {
            Thread.sleep(1000);
            Logger info "***** Server response *****" + "\n" +
              response.body + "\n" +
              "***** End of server response *****";
            Thread.sleep(1000);
          }
          response.json;
        })
      );
    }

    (id, responseOption);
  }
}
