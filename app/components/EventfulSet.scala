package components

import collection.mutable

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
trait EventfulSet[A] extends mutable.Set[A] {
  val onAdded: (A) => _;
  val onRemoved: (A) => _;
  /**
   * if this is false then the clear will not occur
   */
  val onBeforeClear: (EventfulSet[A]) => Boolean;

  abstract override def +=(item: A) = {
    val result = super.+=(item);
    onAdded(item);
    this;
  }

  abstract override def -=(item: A) = {
    val result = super.-=(item);
    onRemoved(item);
    this;
  }

  abstract override def clear() {
    if (onBeforeClear(this)) {
      super.clear();
    }
  }
}
