package components

import akka.actor.{Actor, ActorLogging}
import models.{AlertEmail, EmailServer}
import jodd.mail.{ReceivedEmail, ReceiveMailSession}
import actorMessages.PopEmails
import com.thoughtworks.xstream.XStream
import generic.FileHelper
import FileHelper._
import java.io.File
import generic.TimeHelper._
import play.api.Play._
import scala.Some
import helpers.EmailHelper._
import actorMessages.EmailsFetched
import java.sql.Connection
import play.api.db.DB

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
class EmailFetcher(
                    val server: EmailServer,
                    val auth: Option[Authenticator] = None
                    ) extends Actor with ActorLogging with EmailParser {
  def receive = {
    //val hello = if(true) new Pop3Server("sssss") else new Pop3SslServer("aaa","aaa","aaa");
    case PopEmails(delete) => {
      log.info("we are poping emails with the deletion being " + (if (delete) "enabled" else "disabled"));

      val popServer = server.getPopServer(auth);
      val session: ReceiveMailSession = popServer.createSession();
      val emailArray = try {
        session.open();
        session.receiveEmail(delete);
      } finally {
        session.close();
      }

      if (emailArray == null) {
        log.info("there are no emails now");
        sender ! EmailsFetched(None);
      }
      else {
        val emails = emailArray.toList;
        sender ! EmailsFetched(Some(emails.length));

        DB.withConnection {
          implicit connection =>

          val alertEmails = emails.map(parse).filter(_.isDefined).map(_.get); //drop all spam messages
          //TODO do something more maybe ... like instantiate a new actor!
        }
      }
    }
  }

  def parse(email: ReceivedEmail)(implicit connection: Connection): Option[AlertEmail] = {
    val alertEmail = AlertEmail(email);

    if (!alertEmail.isDefined) {
      val playPath = current.path.getCanonicalPath;
      val emailsPath = playPath + "/" + emailsRelativePath;
      val filePath = emailsPath + "/" + getDateInValidFilenameFormat() + ".xml";

      printToFile(new File(filePath)) {
        pw => new XStream().toXML(email, pw);
      }
      println("we are dropping the email with the subject: " + email.getSubject);
    }

    alertEmail;
  }
}
