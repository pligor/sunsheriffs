package googleImageChart

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
case class GoogleImageChartAxisRange(axisIndex: Int, startVal: Double, endVal: Double) {
  private val seperator = ",";
  override def toString = axisIndex + seperator + startVal + seperator + endVal;
}
