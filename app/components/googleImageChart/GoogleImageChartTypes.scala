package googleImageChart

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
object GoogleImageChartTypes extends Enumeration {
  val pie = Value("p");
  val venn = Value("v");
  val lineWithDefaultAxes = Value("lc");
  val lineWithoutDefaultAxes = Value("lc:nda");
}
