package googleImageChart

import components.helpers.UrlHelper

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
sealed trait GoogleImageChartParam {
  val paramName: String;

  protected def convToString: String;

  override def toString = paramName + "=" + UrlHelper.encode(convToString);
}

case class GoogleImageChartAxisRanges(ranges: Seq[GoogleImageChartAxisRange]) extends GoogleImageChartParam {
  val paramName: String = "chxr";

  private val axesRangesSeperator = "|";

  protected def convToString: String = ranges.mkString(axesRangesSeperator);
}

case class GoogleImageChartVisibleAxes(axes: Seq[GoogleImageChartAxes.Value]) extends GoogleImageChartParam {
  val paramName: String = "chxt";

  private val visibleAxesSeperator = ",";

  protected def convToString: String = axes.mkString(visibleAxesSeperator);
}

case class GoogleImageChartTitle(title: String) extends GoogleImageChartParam {
  val paramName: String = "chtt";

  protected def convToString: String = title;
}

case class GoogleImageChartLineThickness(thicknesses: Seq[Int]) extends GoogleImageChartParam {
  thicknesses.foreach {
    thickness =>
      require(thickness > 0, "obviously thickness should have a positive value larger than zero")
  };

  val paramName: String = "chls";

  private val lineThicknessSeperator = "|";

  protected def convToString: String = thicknesses.mkString(lineThicknessSeperator);
}

case class GoogleImageChartSize(width: Int, height: Int) extends GoogleImageChartParam {
  require(width > 0, "make sure the width of chart has a positive value which makes it visible");
  require(height > 0, "make sure the height of chart has a positive value which makes it visible");

  val paramName: String = "chs";

  protected def convToString: String = width + "x" + height;
}

case class GoogleImageChartType(itsType: GoogleImageChartTypes.Value) extends GoogleImageChartParam {
  val paramName: String = "cht";

  protected def convToString: String = itsType.toString;
}

case class GoogleImageChartData(datas: Seq[Seq[Double]]) extends GoogleImageChartParam {
  val paramName: String = "chd";

  private val dataSetSeperator = ",";

  //protected def convToString: String = GoogleImageChartDataEncoder.simpleEncode(data);
  protected def convToString: String = {
    GoogleImageChartDataEncoder.extendedEncodingFormatPrefix +
      datas.map(GoogleImageChartDataEncoder.extendedEncode).mkString(dataSetSeperator);
  };
}