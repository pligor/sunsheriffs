package googleImageChart

import scala.math._

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
object GoogleImageChartDataEncoder {
  // Same as simple encoding, but for extended encoding.
  val EXTENDED_MAP = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-.";
  val EXTENDED_MAP_LENGTH = EXTENDED_MAP.length;

  val extendedEncodingFormatPrefix = "e:";

  val simpleEncodingFormatPrefix = "s:";

  val defaultMaxFactor = 1.5;

  val simpleEncoding = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  def simpleEncode(values: Seq[Double]): String = simpleEncode(values, values.max * defaultMaxFactor);

  /**
   * This function scales the submitted values so that maxVal becomes the highest value.
   */
  def simpleEncode(values: Seq[Double], maxValue: Double): String = {
    val chartData: Seq[Char] = values.map {
      currentValue =>
        if (currentValue >= 0) {
          simpleEncoding(round((simpleEncoding.length - 1) * currentValue / maxValue).toInt);
        }
        else {
          '_';
        }
    }

    /*simpleEncodingFormatPrefix + */chartData.mkString;
  }


  def extendedEncode(arrVals: Seq[Double]): String = extendedEncode(arrVals, arrVals.max * defaultMaxFactor);

  def extendedEncode(arrVals: Seq[Double], maxVal: Double): String = {
    val chartDatas: Seq[String] = arrVals.map {
      //arrVal =>
      numericVal =>
        val scaledVal = floor(EXTENDED_MAP_LENGTH * EXTENDED_MAP_LENGTH * numericVal / maxVal); // Scale the value to maxVal.

        if (scaledVal > (EXTENDED_MAP_LENGTH * EXTENDED_MAP_LENGTH) - 1) {
          "..";
        } else if (scaledVal < 0) {
          "__";
        } else {
          // Calculate first and second digits and add them to the output.
          val quotient = floor(scaledVal / EXTENDED_MAP_LENGTH);
          val remainder = scaledVal - EXTENDED_MAP_LENGTH * quotient;
          EXTENDED_MAP(quotient.toInt).toString + EXTENDED_MAP(remainder.toInt).toString;
        }
    }

    /*extendedEncodingFormatPrefix + */chartDatas.mkString;
  }
}
