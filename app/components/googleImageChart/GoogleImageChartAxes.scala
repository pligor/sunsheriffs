package googleImageChart

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
object GoogleImageChartAxes extends Enumeration {
  val bottom = Value("x");
  val top = Value("t");
  val left = Value("y");
  val right = Value("r");
}
