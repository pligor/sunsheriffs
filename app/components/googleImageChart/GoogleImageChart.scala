package googleImageChart

import collection.mutable.ListBuffer

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
object GoogleImageChart {
  //val urlPrefix = "//chart.googleapis.com/chart?";
  val urlPrefix = "http://chart.googleapis.com/chart?";
  //TODO remove http later, just put it now for easy copy paste
  val seperator = "&";

  /**
   * For our use we will consider left data to be the power and right data the money
   */
  /**
   *
   * @param title string of the title
   * @param width width in pixels
   * @param height height in pixels
   * @param leftData sequence of power data
   * @param rightData sequence of money
   * @param timeline hours in 24h of day
   * @return
   */
  def createTwoLineChart(title: String,
                         width: Int, height: Int,
                         leftData: Seq[Double],
                         rightData: Seq[Double],
                         timeline: Seq[Double]
                          ): String = {
    require((leftData.length == rightData.length) && (rightData.length == timeline.length), "lengths of datas and timeline must be equal");

    val paramListBuffer = ListBuffer.empty[GoogleImageChartParam];

    paramListBuffer += GoogleImageChartTitle(title);

    paramListBuffer += GoogleImageChartSize(width, height);

    paramListBuffer += GoogleImageChartType(GoogleImageChartTypes.lineWithDefaultAxes);

    paramListBuffer += GoogleImageChartData(Seq(leftData, rightData));

    paramListBuffer += GoogleImageChartLineThickness(Seq(1, 1));

    paramListBuffer += GoogleImageChartVisibleAxes(Seq(
      GoogleImageChartAxes.bottom,
      GoogleImageChartAxes.left,
      GoogleImageChartAxes.right
    ));

    import GoogleImageChartDataEncoder.defaultMaxFactor;
    paramListBuffer += GoogleImageChartAxisRanges(Seq(
      GoogleImageChartAxisRange(0, timeline.min, timeline.max),
      GoogleImageChartAxisRange(1, leftData.min, leftData.max * defaultMaxFactor),
      GoogleImageChartAxisRange(2, rightData.min, rightData.max * defaultMaxFactor)
    ));

    urlPrefix + paramListBuffer.mkString(seperator);
  }
}
