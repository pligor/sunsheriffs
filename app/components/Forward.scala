package components

import jodd.mail.ReceivedEmail
import models.{Webbox, AlertEmail}
import helpers.EmailHelper._
import java.sql.Connection
import java.util.Date

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
trait Forward extends EmailParser {
  //stackable is not applicable here after all ...
  ///*
  abstract override def parse(email: ReceivedEmail)(implicit connection: Connection): Option[AlertEmail] = {
    //webbox serial number -> webbox -> webbox relay email
    //forward(email, );
    val alertEmailOption = super.parse(email);
    if (alertEmailOption.isDefined) {
      val monitoring = new Webbox("", 0, new Date, "http://www.in.gr", id = Some(1)); //TODO implement getBy ... whatever the email tells us

      val destEmailOption = monitoring.getForwardEmail;

      if (destEmailOption.isDefined) {
        forward(email, destEmailOption.get);
      }
      //println("we have forwarded the message");
    }
    //else println("we are not forwarding anything");
    alertEmailOption
  }

  //*/

  def forward(recEmail: ReceivedEmail, destinationEmail: String) {
    require(isValidEmailAddress(destinationEmail));

    //println("Now I am going to forward the email with subject: " + recEmail.getSubject + " to destination: " + destinationEmail);

    val email = receivedEmail2Email(recEmail);

    email.setFrom(DreamhostPligorAuth.user); //TODO maybe change this later
    email.setTo(destinationEmail);

    val smtpServer = DreamhostPligor.getSmtpServer(Some(DreamhostPligorAuth));
    val session = smtpServer.createSession();
    try {
      session.open();
      session.sendMail(email);
    } finally {
      session.close();
    }
  }
}
