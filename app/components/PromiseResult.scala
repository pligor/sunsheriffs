package components

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
case class PromiseResult[A](obj: A, status: PromiseResultStatus.Value) {
  def isOk: Boolean = {
    status == PromiseResultStatus.OK
  }
}

object PromiseResultStatus extends Enumeration {
  val OK = Value;
  val Timeout = Value;
  val Error = Value;
}