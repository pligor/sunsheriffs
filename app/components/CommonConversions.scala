package components

import java.math.BigInteger

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
object CommonConversions {
  implicit def bigInteger2bigInt(num: BigInteger): BigInt = BigInt(num.toString);
}
