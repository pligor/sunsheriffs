package components

import collection.mutable

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
trait EventfulMap[A, B] extends mutable.HashMap[A, B] {
  val onAdded: (A, B) => _;
  val onRemoved: (A) => _;

  abstract override def +=(kv: (A, B)) = {
    val result = super.+=(kv);
    onAdded(kv._1, kv._2);
    this;
  }

  abstract override def -=(key: A) = {
    val result = super.-=(key);
    onRemoved(key);
    this;
  }
}