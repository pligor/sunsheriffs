package components.events

import collection.mutable.ListBuffer

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 *
 * A should be a closure type
 */
trait CallbackHolder {
  def getCallbacks: ListBuffer[_];
}
