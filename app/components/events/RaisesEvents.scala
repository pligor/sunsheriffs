package components.events

//TODO see if it is possible to have generic events without using too much reflection
/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
trait RaisesEvents {
  val callbackHolderClasses: Set[Class[_]];

  private val callbackHolders: Set[CallbackHolder] = callbackHolderClasses.map(callbackHolderClass => callbackHolderClass.newInstance().asInstanceOf[CallbackHolder]);

  /*def register(callback: _, callbackHolderClass: Class[_]) {
    val callbackHolderOption = callbackHolders.find(callbackHolder => callbackHolder.getClass == callbackHolderClass);
    if (callbackHolderOption.isDefined) {
      val callbackHolder = callbackHolderOption.get;
      callbackHolder.getCallbacks += callback;
    }
  }*/

  def raiseEvent(event: Event) {
    //find CallbackHolder By Event

    val currentClass = event.callbackHolderClass;
    val callbackHolderOption = callbackHolders.find(callbackHolder => callbackHolder.getClass == currentClass);
    if(callbackHolderOption.isDefined) {
     val callbackHolder = callbackHolderOption.get

      //callbackHolder.getCallbacks.foreach( callback => callback.apply() )
    }
  }
}
