package components

import play.api.libs.json._
import play.api.libs.json.JsObject
import play.api.libs.json.JsString
import generic.Hash

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
final class WebboxJsonRpcClient(url: String, private val passwd: String, debug: Boolean = false)
  extends JsonRpcClient(url, debug) {

  //the only supported format currently
  private val format = "JSON";

  private val version = "1.0";

  //breaks JSON protocol !!!
  private val requestPrefix = "RPC=";


  override protected val maxIdLen = 16;
  override protected val name = "proc";

  def encodedPass = Hash(passwd).toString.toLowerCase;  //NOTE: Webbox requires password to be in lowercase letters :P

  override protected def buildRequest(procedure: String, params: JsValue = null, notification: Boolean = false): JsObject = {
    //println("password is: " + passwd + " and hashed value: " + Hash(passwd));
    super.buildRequest(procedure, params, notification) ++
      new JsObject(Seq(
        "version" -> JsString(version),
        "format" -> JsString(format),
        "passwd" -> JsString(encodedPass)
      ));
  }

  override protected def grabRequestString(procedure: String, params: JsValue = null, notification: Boolean = false) = {
    val jsObj = buildRequest(procedure, params, notification);
    val id = (jsObj \ "id") match {
      case x: JsString => x.value;
      case y: JsNull.type => null;
      case _ => null;
    };

    (id, requestPrefix + jsObj) //returns a tuple
  }
}

object WebboxJsonRpcClient {
  val pollingIntervalSecs = 30;
}
