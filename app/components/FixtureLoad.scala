package components

import play.api.libs.json._

import play.api.Play.current
import java.sql.Connection
import anorm._
import play.api.libs.json.JsObject
import helpers.AnormHelper._
import java.io.File
import generic.FileHelper
import FileHelper._

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
object FixtureLoad {
  val fixturesPath = "private/fixtures";
  val fixtureExtension = "json";

  /**
   * @deprecated this does not work anymore
   * @param connection db connection
   */
  def apply()(implicit connection: Connection) {
    val path = new File(current.path.getCanonicalPath + "/" + fixturesPath);

    val files = path.list().map(filename => filename.substring(0, filename.length - ("." + fixtureExtension).length));

    files.foreach(tableName => SQL("DELETE FROM " + tableName).execute());

    files.foreach(FixtureLoad(_, delete = false));
  }

  def apply(tableName: String, delete: Boolean = true)(implicit connection: Connection) {
    //TODO check why TRUNCATE does not work with foreign keys!
    if (delete) {
      SQL("DELETE FROM " + tableName).execute();
    }

    /*val bufferedSource = Source.fromFile(current.path.getCanonicalPath + "/" + fixturesPath + "/" + tableName + "." + fixtureExtension);
    val fileContents = bufferedSource.getLines().foldLeft("")(_ + _);*/
    val fileContents = getFileContents(current.path.getCanonicalPath + "/" + fixturesPath + "/" + tableName + "." + fixtureExtension);

    val json = Json.parse(fileContents) match {
      case x: JsObject => x;
      case _ => throw new ClassCastException;
    }

    def jsObjectToMap(jsObj: JsObject) = {
      val columns = jsObj.keys;

      //the toList is critical because the set implies that values are unique which is obviously not true
      val jsValues = columns.toList.map(jsObj \ _);

      val theMap = columns.zip(jsValues).toMap;

      val filteredMap = theMap.filter((x: (String, JsValue)) => x match {
        case (_, JsNull) => false;
        case _ => true;
      });

      filteredMap.map((x: (String, JsValue)) => x match {
        case (z, y: JsString) => z -> y.value;
        case (i, j: JsBoolean) => i -> j.value;
        case (k, l) => k -> l.toString();
      });
    }

    val keys = json.keys;
    for (key <- keys) {
      val curRow = json \ key match {
        case x: JsObject => x;
        case _ => throw new ClassCastException;
      };
      val curMap = jsObjectToMap(curRow);
      //println(curMap.mkString("\n"));
      //println(curMap("language").length);
      insertInto(tableName, curMap);
    }
  }
}
