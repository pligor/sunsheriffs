package components

import jodd.mail.ReceivedEmail
import models.AlertEmail
import java.sql.Connection

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
trait EmailParser {
  def parse(email: ReceivedEmail)(implicit connection: Connection): Option[AlertEmail];
}
