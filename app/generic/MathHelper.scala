package generic

import scala.math._

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
object MathHelper {
  def rad2deg(x: Double): Double = x * 180 / Pi;

  def deg2rad(x: Double): Double = x * Pi / 180;
}
