package generic

import java.io._
import java.util.zip.{ZipEntry, ZipOutputStream}
import io.Source
import java.nio.charset.Charset

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
object FileHelper {
  //implicits
  implicit def func2filenameFilter(func: (File, String) => Boolean) = new FilenameFilter {
    def accept(dir: File, name: String) = func(dir, name);
  };

  def getFileContents(filepathOrUrl: String): String = {
    val file = new File(filepathOrUrl);
    if (file.exists()) {
      Source.fromFile(file).mkString;
    }
    else {
      Source.fromURL(filepathOrUrl).mkString;
    }
  }

  /**
   * http://stackoverflow.com/questions/7598135/how-to-read-a-file-as-a-byte-array-in-scala
   * @param file some file (not folder)
   * @return
   */
  def fileToByteArray(file: File/*, codec: Charset*/): Array[Byte] = {
    require(file.isFile);
    /*val source = Source.fromFile(file)(codec);
    val bytes = source.map(_.toByte).toArray;
    source.close();
    bytes;*/
    val bis = new BufferedInputStream(new FileInputStream(file));
    //Stream.continually(bis.read).takeWhile(-1 !=).map(_.toByte).toArray //too much scala magic
    Stream.continually(bis.read).takeWhile(_ != -1).map(_.toByte).toArray;
  }

  /**
   * http://stackoverflow.com/questions/4604237/how-to-write-to-a-file-in-scala
   * @param f file
   * @param op operation
   * @return
   */
  def printToFile(f: java.io.File)(op: java.io.PrintWriter => Unit) {
    val p = new java.io.PrintWriter(f)
    try {
      op(p)
    } finally {
      p.close()
    }
  }

  def compress(zipFilepath: String, files: List[File]) {
    def readByte(bufferedReader: BufferedReader): Stream[Int] = {
      bufferedReader.read() #:: readByte(bufferedReader);
    }
    val zip = new ZipOutputStream(new FileOutputStream(zipFilepath));
    try {
      for (file <- files) {
        //add zip entry to output stream
        zip.putNextEntry(new ZipEntry(file.getName));

        val in = Source.fromFile(file.getCanonicalPath).bufferedReader();
        try {
          readByte(in).takeWhile(_ > -1).toList.foreach(zip.write(_));
        }
        finally {
          in.close();
        }

        zip.closeEntry();
      }
    }
    finally {
      zip.close();
    }
  }

  def append2file(file: File)(op: PrintWriter => Unit) {
    require(file.exists(), "you may append data only to already existing files");
    val outStream = new FileOutputStream(file, true);
    val p = new PrintWriter(outStream);
    try {
      op(p);
    } finally {
      p.close();
    }
  }

  /**
   * imperative
   */
  def useEachBuffer(inputStream: InputStream, bufferBytesLen: Int)(bufferExploit: (Array[Byte]) => _) {
    val bis = new BufferedInputStream(inputStream, bufferBytesLen);
    val buffer = new Array[Byte](bufferBytesLen);

    var noOfBytesOrMinusOne = -1;
    while ( {
      noOfBytesOrMinusOne = bis.read(buffer);
      noOfBytesOrMinusOne != -1
    }) {
      bufferExploit(buffer.take(noOfBytesOrMinusOne));
    }
  }
}
