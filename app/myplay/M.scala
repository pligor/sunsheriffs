package myplay

import play.api.i18n.{Lang, Messages}
import models.SunsheriffsConfig

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object M {
  def apply(key: String, args: Any*)(implicit lang: Lang): String = {
    if (lang.language == SunsheriffsConfig.defaultLang) {
      Messages(key, args);
    }
    else {
      Messages(key.replace(' ', '_'), args)
    }
  }
}
