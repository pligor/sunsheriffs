package myplay

import java.io.File
import play.Application

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object PlayHelper {
  //just to cache it
  private val s = File.separator;

  def getPlayPath(implicit application: Application) = application.path.getCanonicalPath;

  def isDebuggable(implicit current: Application) = current.configuration.getBoolean("debuggable");

  /**
   * there is no point of using these methods because upon `play dist` the `public` and `managed` no longer exists
   */
  //def getPublicImagesPath = getPublicPath + s + "images";
  //def getPublicCssPath = getPublicPath + s + "stylesheets";
  //def getManagedCssPath = getPlayPath + s + managedCssRelativePath;
  //private def getPublicPath = getPlayPath + s + "public";
  //private val managedCssRelativePath = "target" + s + "scala-2.9.1" + s + "resource_managed" + s + "main" + s + "public" + s + "stylesheets";
}
