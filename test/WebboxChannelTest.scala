import models.{WebboxChannel, WebboxDevice}
import org.specs2.mutable.Specification
import play.api.libs.json.{JsNull, JsString, JsObject}
;

/**
 * Created by pligor
 */
class WebboxChannelTest extends Specification {
  sequential;

  "WebboxChannel" should {
    "parse correctly the json object received" in {
      val obj: JsObject = JsObject(Seq(
        "meta" -> JsString("ExtSolIrr"),
        "name" -> JsString("External radiation"),
        "value" -> JsString("843"),
        "unit" -> JsString("W/m^2"),
        //we exclude min
        //we exclude max
        "options" -> JsNull
      ));

      val channel = new WebboxChannel(obj);

      channel.meta must beEqualTo("ExtSolIrr");
      channel.name must beSome("External radiation");
      channel.value must beEqualTo("843");
      channel.unit must beSome("W/m^2");
      channel.min must beNone;
      channel.max must beNone;
      channel.options must beNone;
    }
  }
}