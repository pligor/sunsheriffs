import java.util.concurrent.TimeUnit
import java.util.Date
import models._
import models.WebboxDeviceDataRequest
import models.WebboxShortChannel
import mydata.MyFakeApp
import org.specs2.mutable.Specification
import play.api.test.Helpers._
import play.api.libs.concurrent._

/**
 * Created by pligor
 */
class WebboxTestRPC extends Specification {
  sequential

  "Webbox of parko mouria at port 5555" should {
    val webbox = new Webbox("", 0, new Date, "http://aliartospark.dyndns.org:8888/rpc")
    "be able to get its plant overview" in {
      implicit val application = MyFakeApp()
      running(application) {
        val overview = webbox.GetPlantOverview().await.get

        //"32542".toInt must not throwAn(new NumberFormatException);
        //"sfganuw".toDouble must throwAn(new NumberFormatException);

        overview.instantaneousPower.toInt must not throwA new NumberFormatException
        overview.dailyEnergy.toDouble must not throwA new NumberFormatException
        overview.totalEnergy.toDouble must beGreaterThan(0.0)
        overview.status must beEmpty
        overview.errorMessage must beEmpty

        success
      }
    }
    "be able to retrieve all of its 11 devices" in {
      implicit val application = MyFakeApp()
      running(application) {
        val devices = webbox.GetDevices().await.get

        devices.length must beEqualTo(30)

        success
      }
    }
    "not allow to get process data from more than 5 devices as in specifications" in {
      implicit val application = MyFakeApp()
      running(application) {
        val dummyReq = WebboxDeviceDataRequest("dummy", Seq())
        val requests = Seq(dummyReq, dummyReq, dummyReq, dummyReq, dummyReq, dummyReq)

        //REMEMBER EXCEPTIONS MUST BE SET EXACTLY! EVEN WITH THEIR STRING
        webbox.GetProcessData(requests) must throwAn(new IllegalArgumentException("requirement failed"))

        success
      }
    }
    "return six values for solar irradiation device and also return the correct serial number for the other device" in {
      implicit val application = MyFakeApp()
      running(application) {
        val key1 = "WRTP2S75:2110433112"
        val key2 = "WRTP4Q75:2110456810"

        val firstRequest = WebboxDeviceDataRequest(key1)
        val secondRequest = WebboxDeviceDataRequest(key2, Seq("Serial Number"))
        //println(secondRequest.toJsObject.toString());
        val reqs = Seq(firstRequest, secondRequest)

        val devResponses = webbox.GetProcessData(reqs).await.get

        val firstResponse = devResponses(0)
        val secondResponse = devResponses(1)

        firstResponse.deviceKey must beEqualTo(key1)
        firstResponse.channels.length must beEqualTo(33)

        secondResponse.deviceKey must beEqualTo(key2)

        //mallon otan autoi oi inverters koimountai tou kalou kairou olo to vradi den dinoun plirofories!
        //secondResponse.channels(0).value must beEqualTo("2110281158");
        secondResponse.channels(0).meta must beEqualTo("Serial Number")

        success
      }
    }
    //RE ENABLE THESE TWO TESTS WHEN YOU HAVE THE SENSOR BOX DEVICE ID OF ALIARTOS ENERGEIAKH
    /*"return responses when asking all parameter channels for solar irradiation device and the lcd number for the other dev" in {
      implicit val application = MyFakeApp()
      running(application) {
        val key1 = "serial number of sensor box here"
        val key2 = "WRTP4Q75:2110456810"

        val firstRequest = WebboxDeviceDataRequest(key1)
        val secondRequest = WebboxDeviceDataRequest("WRTP2S42:2110281158", Seq("LCD.SerNum"));
        val requests = Seq[WebboxDeviceDataRequest](firstRequest, secondRequest);

        //val responses = webbox.GetParameter(requests).value.get;
        val responses = webbox.GetParameter(requests).await(30, TimeUnit.SECONDS).get;
        val firstResponse = responses(0);
        val secondResponse = responses(1);

        firstResponse.deviceKey must beEqualTo("SENS0700:26680")
        firstResponse.channels must have length 33;
        firstResponse.channels(0).meta must beEqualTo("DevNam");

        secondResponse.deviceKey must beEqualTo("WRTP2S42:2110281158");
        secondResponse.channels must have length 1;
        secondResponse.channels(0).meta must beEqualTo("LCD.SerNum");

        success;
      }
    }*/
    /*"try to set the name of the solar irradiation sensor and succeed but fail when try to set the LCD serial number on inverter" in {
      implicit val application = MyFakeApp()
      running(application) {
        //for device "SENS0700:26680" we will test with param: "DevNam" which is read-write
        val firstRequest = new WebboxSetParamRequest("SENS0700:26680", Seq(new WebboxShortChannel("DevNam", "OsensorasMou")))
        //and for device "WRTP2S42:2110281158" we will test with param: "LCD.SerNum" which is read only! (original value: 288419)
        val secondRequest = new WebboxSetParamRequest("WRTP2S42:2110281158", Seq(new WebboxShortChannel("LCD.SerNum", "101010")))

        val responses = webbox.SetParameter(Seq(firstRequest, secondRequest)).await(30, TimeUnit.SECONDS).get
        val firstResponse = responses(0)
        val secondResponse = responses(1)

        firstResponse.deviceKey must be equalTo "SENS0700:26680"
        firstResponse.channels must have length 1
        firstResponse.channels(0).meta must beEqualTo("DevNam")
        firstResponse.channels(0).value must beEqualTo("OsensorasMou")

        secondResponse.deviceKey must be equalTo "WRTP2S42:2110281158"
        secondResponse.channels must have length 1
        secondResponse.channels(0).meta must beEqualTo("LCD.SerNum")
        secondResponse.channels(0).value must beEqualTo("288419"); //means it could not be overwritten since it is read only ;)

        success
      }
    }*/
  }
}