import models.WebboxResponse
import org.specs2.mutable.Specification
import play.api.libs.json.{JsNumber, JsNull, JsString, JsObject}

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
class WebboxResponseTest extends Specification {
  sequential;

  "webbox response" should {
    "contain all the compulsory objects referred to page 10 of reference" in {
      val jsonResponse = new JsObject(Seq(
        "version" -> JsString("1.0"),
        "proc" -> JsString("procedure_call"),
        "id" -> JsString("AF77AF88AF77AF88"),
        "result" -> JsObject(Seq("key" -> JsNumber(33))),
        "error" -> JsNull
      ));

      val response = new WebboxResponse(jsonResponse);

      response.error must beNone;

      response.result must beEqualTo(Some(JsObject(Seq("key" -> JsNumber(33)))));

      response.id must beEqualTo("AF77AF88AF77AF88");

      response.proc must beEqualTo("procedure_call");

      response.version must beEqualTo("1.0");

      //false;
    }
    "have an Option[String] if the error is a JsString" in {
      val jsonResponse = new JsObject(Seq(
        "version" -> JsString("1.0"),
        "proc" -> JsString("procedure_call"),
        "id" -> JsString("AF77AF88AF77AF88"),
        "result" -> JsObject(Seq("key" -> JsNumber(33))),
        "error" -> JsString("dummy error")
      ));

      val response = new WebboxResponse(jsonResponse);

      response.error must beSome[String]("dummy error");
    }
  }
}
