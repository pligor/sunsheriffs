import components.FixtureLoad
import components.MyModel.{NotSameClassException, NoDefinedIdsException}
import java.text.SimpleDateFormat
import java.util.Date
import models._
import mydata.MyFakeApp
import org.specs2.mutable.Specification
import play.api.db.DB
import play.api.test.Helpers._
import components.helpers.AnormHelper._
import scala.Some

/**
 * Created by pligor
 */
class MonitoringSystemTest extends Specification {
  sequential;
  "Monitoring System" should {
    "create a chart image file for its daily data" in {
      implicit val application = MyFakeApp();
      running(application) {
        withNoForeignKeys {
          implicit connection =>
            FixtureLoad();
        }

        DB.withConnection {
          implicit connection =>


          //even if here is actually pure because we have not defined any time
            val unpureDate = (new SimpleDateFormat("yyyy-MM-dd")).parse("2012-10-31");

            val system = MonitoringSystem.getById(1).get;

          //system.getDailyChartImageFile("titlos", unpureDate);
        }
      }

      success;
    }
  }

  "A webbox model" should {
    "retrieve a monitoring system by its id with its subclass" in {
      implicit val application = MyFakeApp();
      running(application) {
        withNoForeignKeys {
          implicit connection =>
            FixtureLoad();
        }

        val webbox = DB.withConnection {
          implicit connection =>
            val monsysOption = MonitoringSystem.getById(1)

            monsysOption.isDefined must beTrue;

            monsysOption.get match {
              case x: Webbox => x;
              case _ => throw new ClassCastException;
            }
        }

        webbox must beAnInstanceOf[Webbox];

        DB.withConnection {
          implicit connection =>
            val monsysOption = MonitoringSystem.getById(-123);

            monsysOption must beNone;
        }

        success;
      }
    }
    "be able to be retrieved via its serial number" in {
      implicit val application = MyFakeApp();
      running(application) {
        withNoForeignKeys {
          implicit connection =>
            FixtureLoad();
        }

        DB.withConnection {
          implicit connection =>

            val noWebboxFound = Webbox.getBySerialNum(4444444.toString); //invalid serial probably
            noWebboxFound must beNone;

            val validSerialNum = 150112854;

            val webboxOption = Webbox.getBySerialNum(validSerialNum.toString);
            webboxOption must beSome;

            val webbox = webboxOption.get;

            webbox.serialNum must be equalTo validSerialNum;
        }
        success;
      }
    }
    "get its own pv plant" in {
      implicit val application = MyFakeApp();
      running(application) {
        withNoForeignKeys {
          implicit connection =>
            FixtureLoad();
        }

        DB.withConnection {
          implicit connection =>
            val webbox = Webbox.getBySerialNum(150112854.toString).get;
            val pvPlantOption = webbox.getPvPlant;
            pvPlantOption must beSome;
            val pvPlant = pvPlantOption.get;
            pvPlant must beAnInstanceOf[PvPlant];
        }
        success;
      }
    }
    "compare itself with another monitoring system and be considered equal if they have same ids" in {
      implicit val application = MyFakeApp();
      running(application) {
        withNoForeignKeys {
          implicit connection =>
            FixtureLoad();
        }

        DB.withConnection {
          implicit connection =>
            val webbox = Webbox.getById(1).get;

            val draftWebbox = new Webbox(name = "", serialNum = 0, timeRegistered = new Date, serverUrl = "http://pligor.com");

            (draftWebbox == webbox) must throwAn[NoDefinedIdsException]("at least one of the models you are trying to compare for equality has no defined ids");

            (webbox == "this is a string") must throwAn[NotSameClassException]("you are trying to compare models which do not belong to the same class");

            val stupidWebbox = new Webbox(name = "", serialNum = 0, timeRegistered = new Date, serverUrl = "https://facebook.com/pligor", id = Some(-1));

            (webbox == stupidWebbox) must beFalse;

            val clevelWebbox = new Webbox(name = "", serialNum = 0, timeRegistered = new Date, serverUrl = "https://facebook.com/pligor", id = Some(1));

            (clevelWebbox == webbox) must beTrue;

            (webbox == webbox) must beTrue;
        }
        success;
      }
    }
  }
}