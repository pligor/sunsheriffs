import components.WebboxJsonRpcClient
import models.WebboxResponse
import mydata.MyFakeApp
import org.specs2.mutable.Specification
import play.api.libs.concurrent._
import play.api.libs.json.{JsObject, JsValue}
import play.api.test.Helpers._
import scala.concurrent.Future

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
class WebboxJsonRpcClientTest extends Specification {
  sequential

  "Webbox Json RPC Client" should {
    /* this became a protected method therefore there is no need to test it
    "temporary check that request is ok" in {
      val client = new WebboxJsonRpcClient("dummyUrl","dummyPass");
      client.buildRequest("procedure").keys must have size(6)
    }*/
    "get a valid json response on GetPlantOverview request with correct id" in {
      //"hello world" must have size (11)
      implicit val application = MyFakeApp()
      running(application) {
        val serverUrl = "http://aliartospark.dyndns.org:8888/rpc"
        val client = new WebboxJsonRpcClient(serverUrl, "sma", debug = false)

        val (id, promiseJsonOption) = client.execute("GetPlantOverview")
        val promiseJson: Future[JsValue] = promiseJsonOption.get
        val json = promiseJson.await.get
        //this is where the slow part happens!
        val jsObj = json match {
          case x: JsObject => x
          case _ => throw new ClassCastException
        }
        val response = new WebboxResponse(jsObj)

        response.error must beNone
        response.id must beEqualTo(id)
      }
    }
  }
}
