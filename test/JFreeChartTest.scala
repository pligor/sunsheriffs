import java.awt.{BasicStroke, Color}
import java.io.FileOutputStream
import java.util.{Date, Calendar}
import org.jfree.chart.axis.NumberAxis
import org.jfree.chart.renderer.xy.{XYAreaRenderer, StandardXYItemRenderer}
import org.jfree.chart.{ChartUtilities, ChartFactory}
import org.jfree.chart.plot.PlotOrientation
import org.jfree.data.time.{TimeSeriesCollection, Second, TimeSeries}
import org.jfree.data.xy.{XYSeries, XYSeriesCollection}
import org.specs2.mutable.Specification

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
class JFreeChartTest extends Specification {
  sequential;

  "JFreeChart" should {
    "create a chart with area color fill" in {
      val dates = genDates(10, new Date);
      val values = List(8, 19, 41, 55, 1, 2, 59, 39, 91, 84);

      val series = new TimeSeries("My Data", classOf[Second]);

      dates.zip(values).foreach(dateNvalue => series.add(new Second(dateNvalue._1), dateNvalue._2));

      val dataset = new TimeSeriesCollection();
      dataset.addSeries(series);

      val chart = ChartFactory.createTimeSeriesChart(
        "Area Chart", // chart title
        "Category", // domain axis label
        "Value", // range axis label
        dataset, // data
        true, // include legend
        false, // tooltips
        false // urls
      );

      //THIS IS actually the surrounding background of the chart
      chart.setBackgroundPaint(Color.CYAN);

      val plot = chart.getXYPlot;
      //val plot = chart.getCategoryPlot; //this throws an exception

      val areaRenderer = new XYAreaRenderer();
      areaRenderer.setSeriesPaint(0, Color.green);
      plot.setRenderer(areaRenderer);

      //AND this is the inner background of the actual chart
      plot.setBackgroundPaint(Color.lightGray);

      /*ChartUtilities.writeChartAsPNG(
        new FileOutputStream("filledchart.png"),
        chart,
        711,
        400
      );*/
    }
    "create a dual axis chart" in {
      val powerSeries = new TimeSeries("Power Data", classOf[Second]);

      //CREATE FIRST TIME SERIES
      val dates = genDates(10, new Date);

      val powers = List[Double](9, 11, 12, 14, 14.5, 14.5, 14, 12, 11, 9);

      val tuples = dates.zip(powers);

      tuples.foreach {
        tuple =>
          val (date, power) = tuple;
          powerSeries.add(new Second(date), power);
      }

      val dataset = new TimeSeriesCollection();
      dataset.addSeries(powerSeries);

      val chart = ChartFactory.createTimeSeriesChart(
        "Ημερήσια Αναφορά", //title
        "Χρόνος", //timeAxisLabel
        "Ισχύς", //valueAxisLabel
        dataset, //dataset
        false, //legend
        false, //tooltips
        false //urls
      );

      //CREATE SECOND AXIS
      val moneyAxis = new NumberAxis("Εισόδημα");

      //no need zero for this axis
      //Sets the flag that indicates whether or not the axis range, if automatically calculated, is forced to include zero
      moneyAxis.setAutoRangeIncludesZero(false);

      val plot = chart.getXYPlot;
      //EXCEPT index 0 all other axis are put on the right!
      plot.setRangeAxis(1, moneyAxis);

      val areaRenderer = new XYAreaRenderer();
      areaRenderer.setSeriesPaint(0, Color.green);
      plot.setRenderer(0, areaRenderer);

      //Create second dataset
      val moneys = List[Double](10, 20, 30, 50, 70, 90, 110, 120, 130, 140);
      val secondTuples = dates.zip(moneys);
      val moneySeries = new TimeSeries("Money Data", classOf[Second]);

      secondTuples.foreach {
        tuple =>
          val (date, power) = tuple;
          moneySeries.add(new Second(date), power);
      }

      val secondDataset = new TimeSeriesCollection();
      secondDataset.addSeries(moneySeries);

      //set the plot of this axis with the new dataset
      plot.setDataset(1, secondDataset);

      //tell explicitly that the new dataset belongs to the axis with index 1
      //Maps a dataset to a particular domain axis. All data will be plotted against axis zero by default, no mapping is required for this case.
      plot.mapDatasetToRangeAxis(1, 1);


      //TOOLTIPS
      /*val renderer = plot.getRenderer;
      renderer.setToolTipGenerator(StandardXYToolTipGenerator.getTimeSeriesInstance);
      if (renderer.isInstanceOf[StandardXYItemRenderer]) {
        val rr = renderer.asInstanceOf[StandardXYItemRenderer];
        //rr.setPlotShapes(true);
        rr.setBaseShapesFilled(true);
        // :/ den pernaei apo edw mesa!
      }*/

      val standardRenderer = new StandardXYItemRenderer();

      standardRenderer.setSeriesStroke(0, new BasicStroke(5));
      standardRenderer.setSeriesPaint(0, Color.black);
      standardRenderer.setPlotLines(true);

      //renderer2.setToolTipGenerator(StandardXYToolTipGenerator.getTimeSeriesInstance);

      plot.setRenderer(1, standardRenderer);

      plot.setForegroundAlpha(0.5f);

      //even we can override the format of the x-axis
      //val axis = plot.getDomainAxis.asInstanceOf[DateAxis];
      //axis.setDateFormatOverride(new SimpleDateFormat("MMM-yyyy"));

      /*ChartUtilities.writeChartAsPNG(
        new FileOutputStream("timeseries.png"),
        chart,
        400,
        200
      );*/


      //////////////////////////////////////////////////////////////////////////
      /*
            /** Line style: line */
            public static final String STYLE_LINE = "line";
            /** Line style: dashed */
            public static final String STYLE_DASH = "dash";
            /** Line style: dotted */
            public static final String STYLE_DOT = "dot";

            /**
             * Convert style string to stroke object.
             *
             * @param style One of STYLE_xxx.
             * @return Stroke for <i>style</i> or null if style not supported.
             */
            private BasicStroke toStroke(String style) {
              BasicStroke result = null;

              if (style != null) {
                float lineWidth = 0.2f;
                float dash[] = {5.0f};
                float dot[] = {lineWidth};

                if (style.equalsIgnoreCase(STYLE_LINE)) {
                  result = new BasicStroke(lineWidth);
                } else if (style.equalsIgnoreCase(STYLE_DASH)) {
                  result = new BasicStroke(lineWidth, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10.0f, dash, 0.0f);
                } else if (style.equalsIgnoreCase(STYLE_DOT)) {
                  result = new BasicStroke(lineWidth, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 2.0f, dot, 0.0f);
                }
              }//else: input unavailable

              return result;
            }//toStroke()

            /**
             * Set color of series.
             *
             * @param chart JFreeChart.
             * @param seriesIndex Index of series to set color of (0 = first series)
             * @param style One of STYLE_xxx.
             */
            public void setSeriesStyle(JFreeChart chart, int seriesIndex, String style) {
              if (chart != null && style != null) {
                BasicStroke stroke = toStroke(style);

                Plot plot = chart.getPlot();
                if (plot instanceof CategoryPlot) {
                  CategoryPlot categoryPlot = chart.getCategoryPlot();
                  CategoryItemRenderer cir = categoryPlot.getRenderer();
                  try {
                    cir.setSeriesStroke(seriesIndex, stroke); //series line style
                  } catch (Exception e) {
                    System.err.println("Error setting style '"+style+"' for series '"+seriesIndex+"' of chart '"+chart+"': "+e);
                  }
                } else if (plot instanceof XYPlot) {
                  XYPlot xyPlot = chart.getXYPlot();
                  XYItemRenderer xyir = xyPlot.getRenderer();
                  try {
                    xyir.setSeriesStroke(seriesIndex, stroke); //series line style
                  } catch (Exception e) {
                    System.err.println("Error setting style '"+style+"' for series '"+seriesIndex+"' of chart '"+chart+"': "+e);
                  }
                } else {
                  System.out.println("setSeriesColor() unsupported plot: "+plot);
                }
              }//else: input unavailable
            }//setSeriesStyle()*/


      //////////////////////////////////////////////////////////////////////////

      success;
    }
    "create a god damn simple line chart with x-y series" in {
      //http://javaconfessions.com/2008/10/generate-charts-and-graphs-in-java.html
      val dataset = new XYSeriesCollection();
      val powerSeries = new XYSeries("Power");

      powerSeries.add(1, 10);
      powerSeries.add(2, 4);
      powerSeries.add(3, 6);
      powerSeries.add(4, 12);
      powerSeries.add(5, 11);
      powerSeries.add(6, 29);
      powerSeries.add(17, 33);

      val moneySeries = new XYSeries("Money");
      moneySeries.add(1, 23);
      moneySeries.add(2, 15);
      moneySeries.add(3, 18);
      moneySeries.add(4, 5);
      moneySeries.add(5, 52);
      moneySeries.add(6, 66);
      moneySeries.add(17, 83);

      dataset.addSeries(powerSeries);
      dataset.addSeries(moneySeries);

      val chart = ChartFactory.createXYLineChart(
        "Ημερήσια Αναφορά", //title
        "Χρόνος", //xAxisLabel
        "Ισχύς", //yAxisLabel
        dataset, //dataset
        PlotOrientation.VERTICAL, //orientation
        true, //legend
        false, //tooltips
        false //urls
      );

      /*ChartUtilities.writeChartAsPNG(
        new FileOutputStream("simplechart.png"),
        chart,
        711,
        400
      );*/

      success;
    }
  }

  def genDates(count: Int, prev: Date): List[Date] = {
    if (count <= 0) {
      Nil;
    }
    else {
      val cal = Calendar.getInstance();
      cal.setTime(prev);
      cal.add(Calendar.MINUTE, 5);
      val newTime = cal.getTime;
      newTime :: genDates(count - 1, newTime);
    }
  }
}