import controllers.routes
import generic.FileHelper
import io.Source
import java.io._
import java.util.zip.ZipInputStream
import mydata.MyFakeApp
import org.specs2.mutable.Specification
import play.api.test.FakeApplication
import play.api.test.Helpers._
import FileHelper._
import components.helpers.UrlHelper._

/**
 * Created by pligor
 */
class FileTest extends Specification {
  sequential;
  "The File helper" should {
    "be able to append contents to an existing file" in {
      implicit val application = MyFakeApp()
      running(application) {

        val path = application.path.getCanonicalPath + "/test/raw"

        path must beAnExistingPath

        val file = new File(path + File.separator + "testing.txt")

        val str1 = "kati grafw edw."

        printToFile(file) {
          printer =>
            printer.write(str1)
        }

        val str2 = "kai symplirono efoson eimai append"

        append2file(file) {
          printer =>
            printer.write(str2)
        }

        val result = Source.fromFile(file).getLines().toList.head

        result must be equalTo str1 + str2

        success
      }
    }
    "grab the content of a file even if it is given from a url" in {
      //THIS TEST REQUIRES SERVER TO BE RUNNING
      //new Uri()
      //http://localhost/assets/stylesheets/dailyEmail.css

      val urlPath = routes.Assets.at("stylesheets/dailyEmail.css")

      urlPath.toString() must be equalTo "/assets/stylesheets/dailyEmail.css"

      //val url = "http://" + getHostname + urlPath
      val url = "http://127.0.0.1:9000" + urlPath

      val contents = getFileContents(url)

      //contents must contain('{');

      success
    }
    "be able to list at least one xml file in the emails folder (one is generated temporarily)" in {
      val fakeApp = FakeApplication();
      running(fakeApp) {
        val emailPath = fakeApp.path.getCanonicalPath + "/private/emails";

        val dummyFp = new File(emailPath + "/dummyFile.xml");

        val data2write = <xml>xml stuff</xml>;

        printToFile(dummyFp) {
          pw =>
            pw.write(data2write.toString());
        }

        val emailFolder = new File(emailPath);

        emailFolder.isDirectory must beTrue;

        //val hello: (File, String) => Boolean;

        val files = emailFolder.list((dir: File, name: String) => name.takeRight(4) == ".xml");

        files.length must beGreaterThan(0);

        dummyFp.delete() must beTrue;
        success;
      }
    }
  }
  "The java util package" should {
    "be able to zip a couple of files" in {
      implicit val application = MyFakeApp();
      running(application) {
        val path = application.path.getCanonicalPath;
        val emailPath = path + "/private/emails";

        val dummy1fp = new File(emailPath + "/dummyFile1.txt");
        val dummy2fp = new File(emailPath + "/dummyFile2.txt")

        printToFile(dummy1fp) {
          pw =>
            pw.write("this is the dummy one file");
        }

        printToFile(dummy2fp) {
          pw =>
            pw.write("this is the dummy two file");
        }

        val files = List[File](dummy1fp, dummy2fp);

        val zipFilepath = emailPath + "/dummyCompressed.zip"

        compress(zipFilepath, files);

        val zipIn = new ZipInputStream(new FileInputStream(zipFilepath));

        //obviously this sets the file pointer to a place where only this entry is visible from the readers
        val zipEntry = zipIn.getNextEntry;
        def readByte(bufferedReader: InputStream): Stream[Int] = {
          bufferedReader.read() #:: readByte(bufferedReader);
        }
        val bytesRead = readByte(zipIn).takeWhile(_ > -1).toList

        bytesRead must be equalTo "this is the dummy one file".toCharArray.map(_.toInt).toList

        dummy1fp.delete() must beTrue;
        dummy2fp.delete() must beTrue;
        (new File(zipFilepath)).delete() must beTrue;
        success;
      }
    }
    "read bytes and create a stream of ints with the bytes" in {
      val fakeApp = FakeApplication();
      running(fakeApp) {
        val privatePath = fakeApp.path.getCanonicalPath + "/private";

        val dummyFp = new File(privatePath + "/dummyFile.txt");

        val data2write = "this a the dummy file";

        printToFile(dummyFp) {
          pw =>
            pw.write(data2write);
        }

        val in = Source.fromFile(dummyFp.getCanonicalPath).bufferedReader();

        def readByte(bufferedReader: BufferedReader): Stream[Int] = {
          bufferedReader.read() #:: readByte(bufferedReader);
        }
        val streamOfInts = readByte(in);

        val listInts = streamOfInts.takeWhile(_ > -1);

        listInts must have length data2write.length

        dummyFp.delete() must beTrue;
        success;
      }
    }
  }
}