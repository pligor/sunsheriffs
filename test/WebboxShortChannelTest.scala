import models.WebboxShortChannel
import org.specs2.mutable.Specification
import play.api.libs.json.{JsObject, JsString}
;

/**
 * Created by pligor
 */
class WebboxShortChannelTest extends Specification {
  sequential;

  "Webbox Short channel" should {
    "parse a json object correctly and then export itself as the same json" in {
      val obj = JsObject(Seq(
        "meta" -> JsString("myMeta"),
        "value" -> JsString("myValue")
      ));

      val shortChannel = new WebboxShortChannel(obj);
      shortChannel.meta must be equalTo ("myMeta");
      shortChannel.value must be equalTo ("myValue");

      shortChannel.toJsObject must be equalTo(obj);
    }
  }
}