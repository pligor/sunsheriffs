import components.helpers.EmailHelper._
import jodd.mail.ReceivedEmail
import org.specs2.mutable.Specification;
import scala.collection.JavaConversions._

/**
 * Created by pligor
 */
class EmailHelperTest extends Specification {
  sequential;
  "EmailHelper" should {
    "convert a received email to an email correctly" in {
      val recEmail = new ReceivedEmail;
      recEmail.setSubject("no subject");
      recEmail.setPriority(104);

      recEmail.addMessage("pare na exeis","text/plain");
      recEmail.addMessage("pare kiallo","dummy/mime");

      val email = receivedEmail2Email(recEmail);

      email.getSubject must be equalTo "no subject";

      email.getPriority must be equalTo 104;

      val messages = email.getAllMessages.toList;

      messages must have length 2;

      messages.head.getContent must be equalTo "pare na exeis";
      messages.head.getMimeType must be equalTo "text/plain";

      email.getAttachments must beNull

      success;
    }
  }
  "check is an email has an officially correct format according to official spec" in {
    isValidEmailAddress("fafawera") must beFalse;
    isValidEmailAddress("george@pligor.com") must beTrue;
  }
}