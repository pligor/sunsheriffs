import generic.GeoPosition
import models.GeoPositionModel
import org.specs2.mutable.Specification

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
class GeoPositionTest extends Specification {
  sequential
  "GeoPositionModel" should {
    "be stringified to a google maps format" in {
      val lat = 38.148515
      val lon = 23.1155

      val pos = new GeoPositionModel(GeoPosition(lat, lon))

      pos.toString must be equalTo (lat.formatted("%.6f") + "," + lon.formatted("%.6f"))

      success
    }
  }
}