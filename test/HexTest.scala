import generic.HexHelper
import org.specs2.mutable.Specification
import play.api.test._


/**
 * Created by pligor
 */
class HexTest extends Specification {
  sequential;
  "HexHelper" should {
    "be able to check if a string actually contains a hexadecimal number" in {

      HexHelper.containsOnly("ssss") must beFalse;

      HexHelper.containsOnly("") must beFalse;

      HexHelper.containsOnly("F38257AAA") must beTrue;

      HexHelper.containsOnly("F38257AAA ") must beFalse;

      HexHelper.containsOnly(" F38257AAA") must beFalse;

      HexHelper.containsOnly("AC5ab9a5afd5f4b2d115163871d8aef323") must beTrue;
      HexHelper.containsOnly("5c5c575450b78d3166f6b411e500de16") must beTrue;

      HexHelper.containsOnly("AC3fd72dabb0e168f7d34e0f6cad3c955d") must beTrue;
      HexHelper.containsOnly("1e822fb1d781d5986cddd57e6b9b8950") must beTrue;

      success;
    }
  }
}