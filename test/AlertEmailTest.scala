import collection.JavaConversions._
import components._
import java.text.SimpleDateFormat
import java.util.TimeZone
import models._
import mydata.MyFakeApp
import org.specs2.mutable.Specification;
import play.api.test.Helpers._
import jodd.mail.{CommonEmail, ReceivedEmail}
import play.api.db.DB
import generic.TimeHelper._
import components.helpers.AnormHelper._

/**
 * Created by pligor
 */
class AlertEmailTest extends Specification {
  sequential;
  "AlertEmail" should {
    "return a webbox alert email if the 'from' is valid" in {
      implicit val application = MyFakeApp();
      running(application) {
        val anEmail = new ReceivedEmail;
        anEmail.setFrom("noreply@Sunny-WebBox.com");

        DB.withConnection {
          implicit connection =>

        }

        success;
      }
    }
    "return no email object if the email is invalid" in {
      implicit val application = MyFakeApp();
      running(application) {
        val anEmail = new ReceivedEmail;
        anEmail.setFrom("mplampla@bloublou.com");

        DB.withConnection {
          implicit connection =>
            AlertEmail(anEmail) must beNone
        }

        success;
      }
    }
  }
  "Webbox Alert Email object" should {
    "parse an error alert email from the webbox" in {
      implicit val application = MyFakeApp();
      running(application) {
        withNoForeignKeys {
          implicit connection =>
            FixtureLoad();
        }

        DB.withConnection {
          implicit connection =>
            val recEmail = new ReceivedEmail;
            recEmail.setFrom("noreply@Sunny-WebBox.com");

            //TODO we still do not know how a subject of an SMA error email looks like or if there is a specific priority
            recEmail.setSubject("a dummy subject");
            recEmail.setPriority(CommonEmail.PRIORITY_NORMAL);

            recEmail.addMessage(
              """Sunny WebBox error report for webboxname
                |Sunny WebBox serial number: 150112854
                |
                |------------------------------------------------------------------------------
                |29.07.2012 08:15 WRTP2S42:2110304299
                |Fehler / DevFlt
                |------------------------------------------------------------------------------
                |
                |created on:: 29.07.2012 09:35
                |This e-mail has been generated automatically. Please do not reply.
                | """.stripMargin, MimeType.TEXT_PLAIN.toString);


            val alertEmailOption = AlertEmail(recEmail);
            alertEmailOption must beSome;
            val alertEmail = alertEmailOption.get;
            alertEmail must beAnInstanceOf[WebboxAlertEmail];

            val webboxAlertEmail = alertEmail match {
              case x: WebboxAlertEmail => x
              case _ => throw new ClassCastException
            }

            webboxAlertEmail.webboxName must be equalTo "webboxname"

            val sdf = new SimpleDateFormat(WebboxAlertEmail.timedatePattern)
            sdf.setTimeZone(TimeZone.getTimeZone("Europe/Athens"))

            webboxAlertEmail.timeCreated must be equalTo sdf.parse("29.07.2012 09:35")

            webboxAlertEmail.errors must have size 1

            val error = webboxAlertEmail.errors.get(0)

            val sdf4error = new SimpleDateFormat(WebboxAlertEmailError.timedatePattern)
            sdf4error.setTimeZone(TimeZone.getTimeZone("Europe/Athens"))

            error.timeOccured must be equalTo sdf4error.parse("29.07.2012 08:15")
        }
        success;
      }
    }
  }
  "Webbox Alert Email Error object" should {
    "parse the list of strings to a real Webbox Alert Email Error" in {
      implicit val application = MyFakeApp();
      running(application) {
        DB.withConnection {
          implicit connection =>

            val lines = List[String](
              "29.07.2012 08:15 WRTP2S42:2110304299",
              "Fehler / DevFlt"
            );

            val webboxOption = Webbox.getBySerialNum(150112854.toString);
            webboxOption must beSome;
            val webbox = webboxOption.get;
            val error = WebboxAlertEmailError(lines, webbox).get;

            error.device.key must be equalTo "WRTP2S42:2110304299";
            error.errorCode must be equalTo "Fehler / DevFlt";
        }
        success;
      }
    }
  }
}