import models.{OnAddedToSleepingWebboxesEvent, OnAddedToSleepingWebboxesCallbackHolder}
import org.specs2.mutable.Specification

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
class RaisesEventsTest extends Specification {
  sequential;

  "A callback holder inside a Set" should {
    "be found by its corresponding event class" in {

      val callbackHolderClasses: Set[Class[_]] = Set(
        classOf[OnAddedToSleepingWebboxesCallbackHolder]
      );

      val callbackHolders = callbackHolderClasses.map(callbackHolderClass => callbackHolderClass.newInstance());

      val webboxId = 1;
      val cancellor = null;

      val event = new OnAddedToSleepingWebboxesEvent(webboxId, cancellor);

      val callbackHolderOption = callbackHolders.find(callbackHolder => callbackHolder.getClass == event.callbackHolderClass);

      callbackHolderOption.isDefined must beTrue;

      success;
    }
  }
}