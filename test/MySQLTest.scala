import components.FixtureLoad
import models.Webbox
import mydata.MyFakeApp
import org.specs2.mutable.Specification
import java.util.Date
import play.api.db.DB
import anorm._
import anorm.SqlParser._
import play.api.test.Helpers._
import generic.TimeHelper._
import components.helpers.AnormHelper._

/**
 * Created by pligor
 */
class MySQLTest extends Specification {
  sequential;

  private def toSecs(msecs: Long) = (msecs / 1000).toLong;

  "MySQL" should {
    "delete a model" in {
      implicit val application = MyFakeApp();
      running(application) {
        withNoForeignKeys {
          implicit connection =>
            FixtureLoad();
        }

        DB.withConnection {
          implicit connection =>
            def countUsers() = {
              SQL("SELECT COUNT(*) FROM User").as(scalar[Long].single);
            }

            val countBefore = countUsers();

            val bool1 = SQL("DELETE FROM User WHERE id = 1").execute();

            val countAfter = countUsers();

            countBefore must be equalTo countAfter+1;

            val bool2 = SQL("DELETE FROM User WHERE id = -1").execute();

            val countAfterDummyDel = countUsers();

            countAfterDummyDel must be equalTo countAfter;
            //TODO find out why both bool1 and bool2 are false! one of them should have been true
        }

        success;
      }
    }
    /*"get all the users from db ordered by timeCreated and then we should get their index (one-based)" in {
      implicit val application = MyFakeApp();
      running(application) {
        withNoForeignKeys {
          implicit connection =>
            FixtureLoad();
        }

        DB.withConnection {
          implicit connection =>
            val users = SQL("SELECT * FROM User ORDER BY timeCreated").as(User.parser *);
        }
        success;
      }
    }*/
    "be able to have a parametric factor on a column (for instance energy * {factor})" in {
      implicit val application = MyFakeApp();
      running(application) {
        withNoForeignKeys {
          implicit connection =>
            FixtureLoad();
        }

        val dailyEnergySample = BigDecimal(DB.withConnection {
          implicit connection =>
            SQL("SELECT dailyEnergy FROM WebboxPlantOverview LIMIT 1").as(scalar[java.math.BigDecimal].single);
        });

        val factor = 2;

        val doubleDailyEnergySample = BigDecimal(DB.withConnection {
          implicit connection =>
            SQL("SELECT dailyEnergy * {factor} FROM WebboxPlantOverview LIMIT 1").on(
              "factor" -> factor
            ).as(scalar[java.math.BigDecimal].single);
        });

        doubleDailyEnergySample must be equalTo (factor * dailyEnergySample);

        success;
      }
    }
    "delete all the rows of the User table on my command" in {
      implicit val application = MyFakeApp();
      running(application) {
        val counter: Long = DB.withConnection {
          implicit connection =>
            SQL("DELETE FROM User").execute();

            SQL("SELECT COUNT(*) FROM User").as(scalar[Long].single);
          //val row = SQL("SELECT COUNT(*) AS counter FROM User")().head;
          //val counter = row[Long]("counter");
          //counter must be equalTo 0;
        };

        counter must be equalTo 0;

        success;
      }
    }
    "insert a User just by its name" in {
      implicit val application = MyFakeApp();
      running(application) {
        DB.withConnection {
          implicit connection =>
          //SQL("DELETE FROM User").execute();  //it is already executed from the above test
            resetAutoIncrement("User");

            val id: Option[Long] = SQL("INSERT INTO User(name,cellphone) VALUES ({name},{cellphone})").on(
              "name" -> "geor",
              "cellphone" -> 0
            ).executeInsert();

            id must beSome(1);
        }
        success;
      }
    }
    "generate the created timestamp that matches the time of the server" in {
      implicit val application = MyFakeApp();
      running(application) {
        val (curName: String, curDate: Date) = DB.withConnection {
          implicit connection =>
            val rowParser = {
              (get[String]("name") ~ get[Date]("timeCreated")).map(flatten);
            }

            SQL("SELECT * FROM User WHERE id = 1").as(rowParser.single);
        }

        curName must be equalTo "geor";

        val dbTimeInSecs = toSecs(curDate.getTime);

        val serverTimeInSecs = toSecs(new Date().getTime);

        dbTimeInSecs must be closeTo(serverTimeInSecs, 1); //sometimes there is a delay !!

        success;
      }
    }
    "insert a timestamp in a database table and then read it and verify is the same" in {
      implicit val application = MyFakeApp();
      running(application) {
        val writtenTime = new Date;

        val readTime = DB.withConnection {
          implicit connection =>
            val id: Option[Long] = SQL("INSERT INTO User(name,timeCreated,cellphone) VALUES ({name},{timeCreated},{cellphone})").on(
              "name" -> "mwro",
              "timeCreated" -> writtenTime,
              "cellphone" -> 1
            ).executeInsert();

            SQL("SELECT timeCreated FROM User WHERE name={name}").on("name" -> "mwro").as(scalar[Date].single)
        }

        toSecs(writtenTime.getTime) must be equalTo toSecs(readTime.getTime)

        success;
      }
    }
    "get all the rows of the webboxes with the given parser" in {
      implicit val application = MyFakeApp();
      running(application) {
        withNoForeignKeys {
          implicit connection =>
            FixtureLoad();
        }

        //val webbox = ;

        val webboxOption = DB.withConnection {
          implicit connection =>
          /*FixtureLoad("User");

          resetAutoIncrement("PvPlant");
          resetAutoIncrement("MonitoringSystem");

          FixtureLoad("PvPlant");
          FixtureLoad("MonitoringSystem");

          SQL("SELECT id, serverUrl, password FROM MonitoringSystem WHERE id = {id}").on(
            "id" -> 1
          ).as(MonitoringSystem.parser.single);*/
            Webbox.getById(1);
        }

        val webbox = webboxOption.get;

        webbox.serverUrl must contain("/rpc");
        webbox.password must be equalTo "sma";

        success
      }
    }
  }
}