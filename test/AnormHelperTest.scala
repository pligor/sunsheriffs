import components.FixtureLoad
import mydata.MyFakeApp
import org.specs2.mutable.Specification
import play.api.db.DB
import anorm._
import generic.TimeHelper._
import play.api.test.Helpers._
import anorm.SqlParser._
import components.helpers.AnormHelper._

/**
 * Created by pligor
 */
class AnormHelperTest extends Specification {
  sequential;
  "Anorm helper" should {
    "insert a row in an arbitrary table given the table name and a map" in {
      implicit val application = MyFakeApp();
      running(application) {
        val randomName = "A Random Name";
        val str = DB.withConnection {
          implicit connection =>
            val tableName = "User"
            SQL("DELETE FROM " + tableName).execute();
            resetAutoIncrement(tableName);

            val idOption: Option[Long] = insertInto(tableName, row = Map(
              "name" -> randomName,
              "cellphone" -> 0
            ));

            idOption must beSome(1);

            SQL("SELECT name FROM " + tableName + " WHERE id = " + idOption.get).as(scalar[String].single);
        }

        str must be equalTo randomName;
        success;
      }
    }
    "insert a plant overview provided a given map" in {
      implicit val application = MyFakeApp();
      running(application) {
        DB.withConnection {
          implicit connection =>
            withNoForeignKeys {
              implicit connection =>
                FixtureLoad();
            }

            val dataMap = Map(
              "instantaneousPower" -> 123456789,
              "dailyEnergy" -> 144.100,
              "totalEnergy" -> 25821.0002,
              "status" -> "",
              "errorMessage" -> ""
            );

            val mymap = dataMap ++ Map(
              "timeRecorded" -> new Date,
              "Webbox_MonitoringSystem_id" -> 1
            );

            /*val row = mymap;
            val keys = row.keys.toSeq;
            val columns = keys.mkString(",");
            println(columns);
            val placeholders = keys.map("{" + _ + "}").mkString(",");
            println(placeholders);*/

            insertInto("WebboxPlantOverview", mymap);
        }
        success;
      }
    }
  }
}