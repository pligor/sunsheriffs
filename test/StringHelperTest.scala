import org.specs2.mutable.Specification
import play.api.test.Helpers._
import components.helpers.StringHelper._

/**
 * Created by pligor
 */
class StringHelperTest extends Specification {
  sequential;
  "The splitLinesByDelimiterLine method" should {
    "take a list of strings which are typically lines that are seperated by a delimiter line and return each portion" in {
      val mychar = '*';
      val charStr = mychar.toString;
      val lines = List(
        charStr * 5,
        "first portion",
        charStr * 22,
        "second portion",
        "has lets say two lines",
        charStr,
        "last portion has again one line"
      );

      val blocks = splitLinesByDelimiterLine(lines, mychar);

      blocks must have length 3;

      blocks.head must have length 1;
      blocks.tail.head must have length 2;
      blocks.reverse.head must have length 1;

      success;
    }
  }
}