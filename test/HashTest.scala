import generic.Hash
import java.security.NoSuchAlgorithmException
import org.specs2.mutable.Specification;

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
class HashTest extends Specification {
  sequential;

  "By default we hash a value with MD5. And the returned string" should {
    "have length of 32 when hashed" in {
      val str = "something";
      val md5 = Hash(str).toString;
      md5 must have length 32;
      success;
    }
    "the returned string should only contain hexadecimal characters" in {
      val str = "something else";
      val md5 = Hash(str).toString;
      md5 must beMatching("^[0-9A-F]{32}$".r);
      success;
    }
  }
  "Hash with another algorithm" should {
    "be ok as long as the algorithm is valid" in {
      val str = "whatever";
      Hash(str, "SHA-1").toString must be equalTo Hash(str, "SHA1").toString;
      Hash(str, "invalid") must throwA[NoSuchAlgorithmException];
      success;
    }
  }
}
