import components.FixtureLoad
import java.sql.Connection
import java.util.Date
import models.Webbox
import mydata.MyFakeApp
import org.specs2.mutable.Specification
import play.api.db.DB
import play.api.test.Helpers._
import components.helpers.AnormHelper._
/**
 * Created by pligor
 */
class ForwardEmailTest extends Specification {
  sequential;
  "A webbox system" should {
    "retrieve its forward email from the database" in {
      implicit val application = MyFakeApp();
      running(application) {

        DB.withConnection {
          implicit connection: Connection =>
            FixtureLoad("User");
            FixtureLoad("PvPlant");
            FixtureLoad("MonitoringSystem");
            FixtureLoad("Webbox");

            val newWebbox = new Webbox("", 0, new Date, "http://www.in.gr");
            newWebbox.getForwardEmail must throwAn[AssertionError];

            insertInto("ForwardEmail", Map("MonitoringSystem_id" -> 1, "mail" -> "email@mail.com"));

            val oldWebbox = new Webbox("", 0, new Date, "http://www.in.gr", id = 1L.toOption);
            oldWebbox.getForwardEmail must beSome("email@mail.com");
        }

        success;
      }
    }
  }
}