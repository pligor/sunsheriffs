import models.WebboxSetParamRequest
import org.specs2.mutable.Specification;

/**
 * Created by pligor
 */
class WebboxSetParamRequestTest extends Specification {
  sequential;

  "A webbox set parameter request" should {
    "not allow for empty values to be set" in {
      (new WebboxSetParamRequest("dummy", Seq())) must throwAn(new IllegalArgumentException("requirement failed: you want to set some values. it doesnt make sense to set using no values!"));
    }
  }
}