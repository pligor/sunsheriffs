import googleImageChart.{GoogleImageChart, GoogleImageChartDataEncoder}
import org.specs2.mutable.Specification

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
class GoogleImageChartTest extends Specification {
  sequential;

  /**
   * from javascript we got these two encodings:
   */
  val leftDataSet = Seq[Double](59.057, 59.676, 51.749, 43.351, 35.299, 37.765, 48.129, 53.515, 53.091, 51.532,
    53.34, 47.278, 51.859, 46.244, 43.182, 49.81, 57.659, 52.174, 50.996, 52.815);

  val rightDataSet = Seq[Double](34.582, 36.493, 48.072, 49.284, 44.276, 30.104, 22.424, 22.473, 20.157, 12.668, 10.32,
    5.31, 7.008, 10.326, 14.306, 16.777, 23.69, 36.506, 39.365, 49.783);

  val timelineSet = Seq[Double](7.07, 7.45, 8.14, 8.99, 9.26, 9.55, 9.64, 10.59, 11.55, 11.89,
    12.52, 13.60, 14.90, 15.01, 15.39, 16.84, 17.05, 17.79, 18.35, 18.97);

  "GoogleImageChartDataEncoder" should {
    "convert to simple encoding and extended encoding a random data set with the same result as the javascript provided by google" in {
      GoogleImageChartDataEncoder.simpleEncode(leftDataSet, leftDataSet.max) must
        //be equalTo "s:891sknx3213w1vsz7102";
        be equalTo "891sknx3213w1vsz7102";

      GoogleImageChartDataEncoder.extendedEncode(leftDataSet, leftDataSet.max) must
        //be equalTo "e:.V..3fufl2ogzn5Z483R5Nyt3nxmuT1a91392s4p";
        be equalTo ".V..3fufl2ogzn5Z483R5Nyt3nxmuT1a91392s4p";

      success;
    }
  }

  "GoogleImageChart" should {
    "create a string for a GET request to have a two line chart image" in {
      val str = GoogleImageChart.createTwoLineChart("Ημερήσια Αναφορά", 300, 200, leftDataSet, rightDataSet, timelineSet);

      println();
      println(str);
      println();

      success;
    }
  }
}