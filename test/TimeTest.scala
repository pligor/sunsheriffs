import java.text.SimpleDateFormat
import mydata.{MyFakeApp, HomePosition}
import org.specs2.mutable.Specification
import java.util._
import play.api.test.Helpers._
import play.api.db.DB
import anorm._
import anorm.SqlParser._
import generic.TimeHelper._
import java.util.concurrent.TimeUnit
import scala.concurrent.duration.Duration

/**
 * Created by pligor
 */
class TimeTest extends Specification {
  sequential

  private def toSecs(msecs: Long) = msecs / 1000

  "TimeHelper component" should {
    "retrieve today's and tomorrow's midnight of a date we provided but shifted in UTC" in {
      val timezoneId = "Europe/Athens"

      val sdf = new SimpleDateFormat(MySQLTimestampFormat)

      sdf.setTimeZone(TimeZone.getTimeZone(timezoneId))

      val now = sdf.parse("2012-11-27 23:07:50")
      val athensMidnight = sdf.parse("2012-11-27 00:00:00")

      val todayUTCmidnight = getUTCmidnight(now, timezoneId)

      println(todayUTCmidnight)

      val cal = Calendar.getInstance()
      cal.setTime(athensMidnight)
      cal.add(Calendar.HOUR_OF_DAY, -2) //xeimerinous mines h athina einai +2 apo GMT h UTC opws thes pesto

      cal.getTime.getTime must be equalTo todayUTCmidnight.getTime

      cal.add(Calendar.HOUR_OF_DAY, +24)
      val tomorrowUTCmidnight = getUTCtomorrowMidnight(now, timezoneId)
      tomorrowUTCmidnight.getTime must be equalTo cal.getTime.getTime

      success
    }
  }

  "Date and Calendar" should {
    "calculate the UTC midnight of the current day and the next one" in {
      val timezone = TimeZone.getTimeZone("Europe/Athens")

      val sdf = new SimpleDateFormat(MySQLTimestampFormat)
      sdf.setTimeZone(timezone)
      val now = sdf.parse("2012-11-04 18:17:25")

      val cal = Calendar.getInstance(timezone)
      cal.setTime(clearTime(now)); //clear time gives midnight by default for this timezone

      val midnight = cal.getTime

      cal.add(Calendar.DAY_OF_YEAR, 1)

      val tomorrowMidnight = cal.getTime

      val utcMidnight = convDate2GMT(midnight, timezone)
      val utcTomorrowMidnight = convDate2GMT(tomorrowMidnight, timezone)

      //println(utcMidnight);
      utcMidnight must be equalTo sdf.parse("2012-11-03 22:00:00")
      //println(utcTomorrowMidnight);
      utcTomorrowMidnight must be equalTo sdf.parse("2012-11-04 22:00:00")

      success
    }
  }

  "TimeHelper" should {
    "get the correct time after a certain duration (either positive or negative)" in {
      val dur = Duration(10, TimeUnit.SECONDS);
      val sometime = new SimpleDateFormat("HH:mm:ss").parse("07:34:00");
      val afterTime = getUTCtimeAfterDuration(dur, sometime);
      afterTime.getTime must be equalTo sometime.getTime + 10 * 1000;

      val durLonger = Duration(-3, TimeUnit.MINUTES);
      val beforeTime = getUTCtimeAfterDuration(durLonger, sometime);
      beforeTime.getTime must be equalTo sometime.getTime - 3 * 60 * 1000;

      val twoMins = Duration(2, TimeUnit.MINUTES);
      val beforeTwoMins = getUTCtimeAfterDuration(-twoMins, sometime);
      beforeTwoMins.getTime must be equalTo sometime.getTime - 2 * 60 * 1000;

      success;
    }
    "get a time later on the same day or on a later day with an offset" in {
      //      val cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
      //      cal.setTimeZone(TimeZone.getTimeZone("GMT")); //redundancy but better be sure :P
      val cal = Calendar.getInstance();

      cal.set(Calendar.SECOND, 0);
      cal.set(Calendar.MINUTE, 0);
      cal.set(Calendar.HOUR_OF_DAY, 14);
      cal.set(Calendar.DAY_OF_MONTH, 15);
      cal.set(Calendar.MONTH, Calendar.NOVEMBER);
      cal.set(Calendar.YEAR, 2012);

      val duration = getDurationUntilUTCtime(15, 25, date = cal.getTime);
      duration.toMinutes must be equalTo 84; //because some millisecs have passed we go below 85
      //println(duration);

      val moreDuration = getDurationUntilUTCtime(15, 25, daysOffset = 1, date = cal.getTime);
      moreDuration.toHours must be equalTo 25;
      //println(moreDuration.toHours);

      success;
    }
    "clear the hours, minutes, seconds and milliseconds from a Date object and leave only the day year and month" in {
      val date = new SimpleDateFormat(MySQLTimestampFormat).parse("2012-10-29 17:54:23");
      val onlyDate = new SimpleDateFormat("yyyy-MM-dd").parse("2012-10-29");

      val clearedDate = clearTime(date);

      onlyDate.getTime must be equalTo clearedDate.getTime;

      success;
    }

    "calculate the sunrise and the sunset of a given location at a given day" in {
      val day = 1
      val month = 1
      val year = 2013

      val position = HomePosition

      val dayDate: Date = new SimpleDateFormat("dd/MM/yyyy").parse(day + "/" + month + "/" + year)

      val Some(sunrise) = getUTCsunrise(position.geoPosition, dayDate)
      val Some(sunset) = getUTCsunset(position.geoPosition, dayDate)

      val sunriseSecs = toSecs(sunrise.getTime)
      val sunsetSecs = toSecs(sunset.getTime)

      val sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm")
      sdf.setTimeZone(TimeZone.getTimeZone("Europe/Athens"))
      val approxSunriseDate = sdf.parse(day + "/" + month + "/" + year + " 06:39")
      val approxSunsetDate = sdf.parse(day + "/" + month + "/" + year + " 18:18")

      val approxSunriseSecs = toSecs(approxSunriseDate.getTime)
      val approxSunsetSecs = toSecs(approxSunsetDate.getTime)

      sunriseSecs must be ~ (approxSunriseSecs +/- 60)

      sunsetSecs must be ~ (approxSunsetSecs +/- 60)

      success
    }

    "return the correct day of the year" in {
      val day = 29;
      val month = 10;
      val year = 2012;
      val dayDate: Date = new SimpleDateFormat("dd/MM/yyyy").parse(day + "/" + month + "/" + year);
      getYearDay(dayDate) must be equalTo getYearDay_old(dayDate);
      success;
    }

    "convert a decimal hour to hours, minutes and seconds" in {
      val (hours, minutes, seconds, milliseconds) = decimalHour2hoursMinsSecsMsecs(17.5);
      hours must be equalTo 17;
      minutes must be equalTo 30;
      seconds must be equalTo 0;
      milliseconds must be equalTo 0;
      success;
    }
  }

  "The offset from UTC" should {
    "give us the degrees from Greenwich" in {
      val curTimeZone = TimeZone.getTimeZone("Europe/Athens");

      val date = new Date();

      val msFromEpochGmt: Long = date.getTime;

      val offsetFromUTC: Int = curTimeZone.getOffset(msFromEpochGmt);
      //println(offsetFromUTC);

      //express offsetFromUTC in hours
      val offset = Duration(offsetFromUTC, TimeUnit.MILLISECONDS);

      //val offsetFromUTCinHours: Long = offset.toHours;
      val offsetFromUTCinHours: Double = offset.toHours;

      val degreesPerHour = 360 / 24; //15

      val LSTM = degreesPerHour * offsetFromUTCinHours;

      if (curTimeZone.inDaylightTime(date)) {
        LSTM must be equalTo 45;
      } else {
        LSTM must be equalTo 30;
      }

      success;
    }
  }

  "Timezone" should {
    "be valid if required" in {
      isValidTimezone("Europe/Athens") must beTrue;
      isValidTimezone("whateveritis") must beFalse;
      success;
    }
  }

  "Time" should {
    "be converted to GMT time even if it is coming from the NOW function of mysql" in {
      implicit val application = MyFakeApp();
      running(application) {
        val dbDate: Date = DB.withConnection {
          implicit connection =>

            SQL("SELECT NOW()").as(scalar[Date].single);
        }
        val dbSecs = toSecs(dbDate.getTime);

        val serverDate = new Date();
        val serverSecs = toSecs(serverDate.getTime);

        serverSecs must be closeTo(dbSecs, 1);
      }

      success;
    }
  }

  "Calendar" should {
    "add correctly the hours and days in a given date" in {
      val cal = Calendar.getInstance();
      //cal.setTime(new SimpleDateFormat("HH:mm:ss").parse("23:00:00"));  this gives epoch date!

      cal.set(Calendar.SECOND, 0);
      cal.set(Calendar.MINUTE, 0);
      cal.set(Calendar.HOUR_OF_DAY, 23);
      cal.set(Calendar.DAY_OF_MONTH, 30);
      cal.set(Calendar.MONTH, 11);
      cal.set(Calendar.YEAR, 2012);

      val elevenPm: Date = cal.getTime;

      //println(cal.getTime);

      cal.add(Calendar.HOUR_OF_DAY, 1);

      val oneHourLater: Date = cal.getTime;

      oneHourLater.getTime must be equalTo elevenPm.getTime + Duration(1, TimeUnit.HOURS).toMillis

      //println(cal.getTime);

      cal.add(Calendar.DAY_OF_YEAR, 1);

      //println(cal.getTime);

      cal.get(Calendar.MONTH) must be equalTo 0; //this is January
      cal.get(Calendar.YEAR) must be equalTo 2013;

      success;
    }
    "return the same hour if the timezone is changing at 3am early in the morning because of DST at 28 October 2012" in {
      val cal = Calendar.getInstance();

      cal.set(Calendar.SECOND, 0);
      cal.set(Calendar.MINUTE, 0);
      cal.set(Calendar.HOUR_OF_DAY, 2);
      cal.set(Calendar.DAY_OF_MONTH, 28);
      cal.set(Calendar.MONTH, Calendar.OCTOBER);
      cal.set(Calendar.YEAR, 2012);

      cal.add(Calendar.HOUR_OF_DAY, 1); //this adding is ok
      cal.get(Calendar.HOUR_OF_DAY) must be equalTo 3;
      cal.add(Calendar.HOUR_OF_DAY, 1); //now the hour remains the same since the timezone is changed
      cal.get(Calendar.HOUR_OF_DAY) must be equalTo 3;

      cal.clear();

      success;
    }
    "on a day where we have DST (timezone change) adding 1 day works correctly while adding 24 hours is wrong" in {
      val cal = Calendar.getInstance();

      cal.set(Calendar.SECOND, 0);
      cal.set(Calendar.MINUTE, 0);
      cal.set(Calendar.HOUR_OF_DAY, 2);
      cal.set(Calendar.DAY_OF_MONTH, 28);
      cal.set(Calendar.MONTH, Calendar.OCTOBER);
      cal.set(Calendar.YEAR, 2012);

      //println(cal.getTime);

      cal.add(Calendar.DAY_OF_YEAR, 1);
      cal.get(Calendar.HOUR_OF_DAY) must be equalTo 2;

      //println(cal.getTime);

      cal.set(Calendar.SECOND, 0);
      cal.set(Calendar.MINUTE, 0);
      cal.set(Calendar.HOUR_OF_DAY, 2);
      cal.set(Calendar.DAY_OF_MONTH, 28);
      cal.set(Calendar.MONTH, Calendar.OCTOBER);
      cal.set(Calendar.YEAR, 2012);

      cal.add(Calendar.HOUR_OF_DAY, 24);
      //println(cal.getTime);
      cal.get(Calendar.HOUR_OF_DAY) must be equalTo 1;

      success;
    }
  }
}