import components.JsonRpcClient
import mydata.MyFakeApp
import org.specs2.mutable.Specification
import play.api.libs.concurrent._
import play.api.libs.json.{JsNull, JsString, JsValue}
import play.api.test.Helpers.running
import scala.concurrent.Future
;

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
/*class JsonRpcClientTest extends Specification {
  sequential

  val serverUrl = "http://localhost:8080/jsonrpc/index.php"
  val client = new JsonRpcClient(serverUrl, debug = false)

  "the Json Rpc Client" should {
    "call the dummy procedure and get this string back: 'well done! you called the dummy procedure'" in {
      implicit val application = MyFakeApp()
      running(application) {
        val (id, promiseJsonOption) = client.execute("dummy")
        val promiseJson: Future[JsValue] = promiseJsonOption.get
        val json = promiseJson.await.get; //this is where the slow part happens!
        json \ "result" must beEqualTo(JsString("well done! you called the dummy procedure"))
      }
    }
    "call the dummy procedure and the error must be null" in {
      implicit val application = MyFakeApp()
      running(application) {
        val (id, promiseJsonOption) = client.execute("dummy")
        val promiseJson: Future[JsValue] = promiseJsonOption.get
        promiseJson.await.get \ "error" must beEqualTo(JsNull)
      }
    }
  }
}*/
