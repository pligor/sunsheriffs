import components._
import java.io.File
import java.text.SimpleDateFormat
import java.util.Date
import jodd.JoddCore
import jodd.mail.Email
import models.{SunsheriffsConfig, DailyReport, PvPlant}
import mydata.MyFakeApp
import myplay.PlayHelper
import org.specs2.mutable.Specification
import play.api.db.DB
import components.helpers.AnormHelper._
import components.helpers.UrlHelper._
import play.api.i18n.Lang
import play.api.test.Helpers._
import PlayHelper._
import components.helpers.EmailHelper._
import jodd.mail.EmailAttachment.attachment

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
class DailyEmailTest extends Specification {
  sequential;
  "Daily Report" should {
    "generate a random image file path to be used for chart that will be embedded in email" in {
      val path = DailyReport.genNewTemporaryPngPath;

      path.substring(0, 4) must be equalTo "/tmp";
      path.substring(path.length - 4) must be equalTo ".png";

      new File(path).exists() must beFalse;

      success;
    }
  }

  "Jodd" should {
    "send an email created dynamically from a view" in {
      implicit val application = MyFakeApp();
      running(application) {
        val sdf = new SimpleDateFormat("EEEE, dd 'of' MMMM yyyy");

        withNoForeignKeys {
          implicit connection =>
            FixtureLoad();
        }

        val plant1 = DB.withConnection {
          implicit connection =>
            PvPlant.getById(1);
        }
        val plant2 = DB.withConnection {
          implicit connection =>
            PvPlant.getById(2);
        }

        //val images = Seq[String]("sample_image_chart1.jpeg");
        val images = Seq[String]("sample_image_chart1.jpeg", "sample_image_chart2.png", "sample_image_chart3.png");

        //val chartImages = images.map(image => Hash.genUUID -> image).toMap;
        val embeddedImages = images.map(image => image -> image).toMap;

        val html = DB.withConnection {
          implicit connection =>

            implicit val lang = Lang("el");

            views.html.dailyEmail(
              absoluteLinkPrefix = "http://" + SunsheriffsConfig.hostname, //TODO later check for https
              dateToday = sdf.format(new Date),
              usernameKlitikh = "Κουμουτσάκο",
              timeSent = "18:30",
              plants = Seq[PvPlant](
                plant1.get,
                plant2.get
              ),
              dailyEnergy = 1500.241,
              totalEnergy = 358126.241,
              dailyProfit = 25.04,
              embeddedImages = embeddedImages
            )();
        }


        val sampleImagesPath = application.path.getCanonicalPath + File.separator +
          "public" + File.separator + "images" + File.separator + "dummies";

        JoddCore.encoding must be equalTo "UTF-8";

        val testDreamhost = (DreamhostPligorAuth.user, DreamhostPligor.getSmtpServer(Some(DreamhostPligorAuth)));

        val email = Email.create().
          from(testDreamhost._1).
          to("george@pligor.com").
          subject("testing daily email").
          addText("please use an email client that supports html").
          addHtml(html.toString());

        for ((contentId, image) <- embeddedImages) {
          val fullpath = sampleImagesPath + File.separator + image;
          fullpath must beAnExistingPath
          email.embed(attachment().setInline(contentId).file(fullpath))
        }

        //sendSingleMail(email, testDreamhost._2);

        success;
      }
    }
  }
}