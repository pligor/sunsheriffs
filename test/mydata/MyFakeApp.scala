package mydata

import play.api.test.FakeApplication

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object MyFakeApp {
  def apply() = FakeApplication(additionalConfiguration = Map[String, String](
    //"db.default.url" -> "jdbc:mysql://localhost/sunsheriffs_test?characterEncoding=UTF-8&useUnicode=true"
  ))
}
