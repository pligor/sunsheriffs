package mydata

import models.{WebboxPlantOverview, Webbox}
import java.util.Date
import scala.concurrent.Future

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
object FakeWebbox extends Webbox(
  name = "Fake Webbox",
  serialNum = 0,
  serverUrl = "http://pligor.com",
  timeRegistered = new Date,
  id = Some(100)) {

  override def GetPlantOverview(): Future[WebboxPlantOverview] = {

    //TODO
    null;
  }
}
