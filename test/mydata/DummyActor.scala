package mydata

import akka.actor._

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
class DummyActor extends Actor with ActorLogging {
  def receive = {
    case "motherfucker" => {
      sender ! "up your ass";
    }
    case "good boy" => {
      //log info "yeah ... right!!";
      sender ! "yeah right";
    }
    case "pingAndStop" => {
      sender ! "pong";
      context.stop(self); //this enables you to recreate the actor
    }
    case "createChild" => {
      val grandchild = context.actorOf(Props[DummyActor], name = "grandchild");
      sender ! grandchild;
    }
    case childName: String => {
      try {
        log info "ftasame edw re?";
        val grandchild = context.actorOf(Props[DummyActor], name = childName);
        sender ! Some(grandchild);
      }
      catch {
        case e: InvalidActorNameException => sender ! None;
      }
    }
  }
}
