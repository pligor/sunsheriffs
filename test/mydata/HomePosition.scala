package mydata

import models.GeoPositionModel
import generic.GeoPosition

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
object HomePosition extends GeoPositionModel(GeoPosition(latitude = 38.124083, longitude = 23.758127));
