import com.google.i18n.phonenumbers.PhoneNumberUtil
import com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat
import com.ning.http.client.Realm
import com.twilio.sdk.TwilioRestClient
import components.{TwilioClient, MyLogger}
import java.util.concurrent.TimeUnit
import models.TwilioPligor
import mydata.{TwilioPligorTest, MyFakeApp}
import org.specs2.mutable.Specification
import play.api.libs.ws.WS
import play.api.libs.ws.WS.WSRequestHolder
import play.api.test._
import Helpers._
import java.net.{ConnectException, URLEncoder}
import play.api.libs.concurrent._
import scala.concurrent.ExecutionContext
import ExecutionContext.Implicits.global

/**
 * Created by pligor
 */
class TwilioTest extends Specification {
  sequential;
  "Twilio Client" should {
    "send an sms using the official java library of twilio" in {
      implicit val application = MyFakeApp();
      running(application) {

        val chargeMe = false;
        val messenger = if (chargeMe) TwilioPligor else TwilioPligorTest;

        val sms = messenger.sendSMS(
          fromNumber = messenger.primaryPhoneNumber,
          toNumber = PhoneNumberUtil.getInstance().parse("+306956775925", ""),
          body = "ΕΛΛΗΝΙΚΟ ΚΕΙΜΕΝΟ 25 CHARSΕΛΛΗΝΙΚΟ ΚΕΙΜΕΝΟ 25 CHARSΕΛΛΗΝΙΚΟ ΚΕΙΜΕΝΟ 25 CHARS"
        );

        sms.getStatus must be equalTo TwilioClient.successSentStatus;
        success;
      }
    }
    "send an sms to any phone now that we have a registered account" in {
      implicit val application = MyFakeApp();
      running(application) {
        /*val promiseResponse = TwilioPligor.sendSMS_old(
          fromNumber = TwilioPligor.primaryPhoneNumber,
          //fromNumber = PhoneNumberUtil.getInstance().parse("6956775925", "GR"),
          //ANOTHER fromNumber didn't work :(
          toNumber = PhoneNumberUtil.getInstance().parse("6936151050", "GR"),
          body = "body to be sent μαζί με ελληνικό κείμενο"
        );*/
        success;
      }
    }
  }
  "Twilio" should {
    "be used only with url encoded string for form data" in {
      URLEncoder.encode("ti les re makala ?", "UTF-8") must be equalTo "ti+les+re+makala+%3F";

      URLEncoder.encode("+14245437260", "UTF-8") must be equalTo "%2B14245437260";

      val util = PhoneNumberUtil.getInstance();
      val phoneNumber = util.format(util.parse("6956775925", "GR"), PhoneNumberFormat.E164);

      URLEncoder.encode(phoneNumber, "UTF-8") must be equalTo "%2B306956775925";

      success;
    }
    "send an sms" in {
      implicit val application = MyFakeApp();
      running(application) {
        /*curl -X POST 'https://api.twilio.com/2010-04-01/Accounts/AC3fd72dabb0e168f7d34e0f6cad3c955d/SMS/Messages.json' \
        -d 'From=%2B15005550006' \
        -d 'To=%2B306956775925' \
        -d 'Body=one+more+try' \
        -u AC3fd72dabb0e168f7d34e0f6cad3c955d:1e822fb1d781d5986cddd57e6b9b8950*/

        val wsRequestHolder: WS.WSRequestHolder = WSRequestHolder(
          url = """https://api.twilio.com/2010-04-01/Accounts/AC5ab9a5afd5f4b2d115163871d8aef323/SMS/Messages.json""",
          headers = Map.empty[String, Seq[String]],
          queryString = Map.empty[String, Seq[String]],
          calc = None,
          auth = Some(("AC5ab9a5afd5f4b2d115163871d8aef323", "5c5c575450b78d3166f6b411e500de16", Realm.AuthScheme.BASIC)),

          followRedirects = None, //???
          timeout = None, //???
          virtualHost = None  //???
        )

        val request = Map[String, Seq[String]](
          "From" -> Seq("%2B14245437260"),
          "To" -> Seq("%2B306956775925"),
          "Body" -> Seq("one+more+try")
        )

        //val promiseResponse: Promise[ws.Response] = wsRequestHolder.post(request); //I WILL BE CHARGED HERE :P
        //val jsonResponse = promiseResponse.value.get.json;
        //println(jsonResponse);

        success
      }
    }
  }
  "Twilio Official Library" should {
    "pretend to have sent an sms with the test account" in {
      implicit val application = MyFakeApp();
      running(application) {
        val client = new TwilioRestClient(TwilioPligorTest.accountSid, TwilioPligorTest.authToken);
        val mainAccount = client.getAccount;
        val smsFactory = mainAccount.getSmsFactory;
        val smsParams = Map[String, String](
          //"From" -> TwilioPligor.primaryNumberDefaultFormat,
          "From" -> "+15005550006",
          "To" -> "+306956775925",
          "Body" -> "sending to myself"
        );

        //val sms = smsFactory.create(smsParams);

        //sms.getStatus must be equalTo "queued";

        success;
      }
    }
  }
  "curl" should {
    "be tested before used with twilio" in {
      implicit val application = MyFakeApp();
      running(application) {
        //so we have this request with curl:
        //curl -X POST 'http://localhost:9000/testCurl?kati=allo' -d 'malaka=ti malakies les re?' -d 'nai=esenaRotaw'

        //let's make it with WS library
        try {
          val wsRequestHolder: WS.WSRequestHolder = WSRequestHolder(
            url = """http://localhost:9000/testCurl?kati=allo""",
            headers = Map.empty[String, Seq[String]],
            queryString = Map[String, Seq[String]]("kati" -> Seq("allo")),
            calc = None,
            auth = None,

            followRedirects = None, //???
            timeout = None, //???
            virtualHost = None  //???
          );

          //val request = java.net.URLEncoder.encode("malaka=ti malakies les re?","UTF-8");
          val request = Map[String, Seq[String]]("hello" -> Seq("world"));
          val promiseResponse = wsRequestHolder.post(request);

          val timeoutErrorMsg = "error";

          val responseBody = promiseResponse.orTimeout(timeoutErrorMsg, 2, TimeUnit.SECONDS).map(
            eitherResponseOrString =>
              eitherResponseOrString.fold(
                response => response.body,
                timeoutMessage => timeoutMessage
              )
          ).await.get;

          println(responseBody);
        }
        catch {
          case e: ConnectException => MyLogger debug "the test is skipped since the corresponding server is not responding";
        }
        success;
      }
    }
  }
}