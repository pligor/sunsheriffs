import actorMessages.{EmailsFetched, PopEmails}
import akka.util.Timeout
import akka.pattern.ask
import components._
import java.util.concurrent.TimeUnit
import jodd.mail.Email
import org.specs2.mutable.Specification;
import akka.actor.ActorSystem
import akka.testkit.TestActorRef
import play.api.libs.concurrent._
import play.api.test.Helpers._
import components.helpers.EmailHelper._


/**
 * Created by pligor
 */
class EmailFetcherTest extends Specification {
  sequential;

  val popServer = DreamhostPligor.getPopServer(Some(DreamhostPligorAuth));
  val smtpServer = DreamhostPligor.getSmtpServer(Some(DreamhostPligorAuth));

  "Email fetcher" should {
    "respond with the number of emails popped by the PopEmails message (zero here)" in {
      Thread.sleep(1000); //almost redundant

      //first pop previous messages and discard them
      discardEmails(popServer);

      Thread.sleep(1000); //almost redundant

      implicit val actorSystem = ActorSystem("dummySystem");
      //implicit val actorSystem = Akka.system;

      val actorRef = TestActorRef(new EmailFetcher(DreamhostPligor, Some(DreamhostPligorAuth)));

       //val actor = actorRef.underlyingActor;
       implicit val timeout = Timeout(10, TimeUnit.SECONDS);

       //actor.receive(PopEmails(delete = true));
       val emailsFetchedPromise = (actorRef ? PopEmails(delete = true)).mapTo[EmailsFetched];

       val numberOption = emailsFetchedPromise.await(timeout.duration.toSeconds, TimeUnit.SECONDS).get.number;

       numberOption must beNone;
    }
  }

  private def sendEmailToItself() {
    val email = Email.create().
      from("testing@pligor.com").
      to("testing@pligor.com").
      subject("test subject").
      addText("test body");

    val session = smtpServer.createSession();
    try {
      session.open();
      session.sendMail(email);
    } finally {
      session.close();
    }
    success;
  }
}