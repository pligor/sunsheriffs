import components.{InverterErrorSeverity, InverterCompany}
import models.{SmaInverterError, InverterError}
import mydata.MyFakeApp
import org.specs2.mutable.Specification
import play.api.test.FakeApplication
import play.api.test.Helpers._

/**
 * Created by pligor
 */
class InverterErrorTest extends Specification {
  sequential;
  "Inverter Error Object" should {
    "return a list of objects from the corresponding error csv file of the company" in {
      implicit val application = MyFakeApp()
      running(application) {
        val errors = InverterError.getErrors(InverterCompany.SMA);

        val error: SmaInverterError = errors.head match {
          case x: SmaInverterError => x;
          case _ => throw new ClassCastException;
        }

        error.code must contain("Overvoltage");

        error.description must contain("Overvoltage");

        error.severity must be equalTo InverterErrorSeverity.LOW;

        error.correctiveMeasure must contain("disconnect the PV generator");

        success;
      }
    }
  }
}