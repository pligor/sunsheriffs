import components.helpers.JavaMailHelper._
import components._
import org.specs2.mutable.Specification

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
class JavaMailTest extends Specification {
  sequential;

  val htmlString = """<html>
                     |<head><meta content="text/html; charset=UTF-8" http-equiv="Content-Type"></head>
                     |<body>
                     |<div>I guess a simple linked image is easy to display</div>
                     |<div><img alt="" src="http://afhimelfarb.files.wordpress.com/2011/02/window.jpg"/></div>
                     |</body>
                     |</html>
                   """.stripMargin;

  val sampleImage = "/home/pligor/playProjects/workspace/sunsheriffs/public/images/dummies/sample_image_chart1.jpeg";

  "JavaMail library" should {
    "send a simple email" in {

      /*sendPlainTextMail(
        smtpServer = DreamhostPligor.smtpServer,
        fromEmail = DreamhostPligorAuth.user,
        toEmail = DreamhostPligorAuth.user, //self-send
        subject = "testing java mail library",
        body = "ena aplo body ti allo?",
        auth = Some(DreamhostPligorAuth)
      ) must beTrue;*/

      success;
    }
    "send an html email with no attachments" in {
      /*sendHtmlMail(
        smtpServer = DreamhostPligor.smtpServer,
        fromEmail = DreamhostPligorAuth.user,
        toEmail = DreamhostPligorAuth.user, //self-send
        subject = "testing html message with java mail library",
        body = htmlString,
        auth = Some(DreamhostPligorAuth)
      ) must beTrue;*/

      success;
    }
    "send an email with one attachment" in {
      sampleImage must beAnExistingPath

      /*sendHtmlMailWithAttachment(
        smtpServer = DreamhostPligor.smtpServer,
        fromEmail = DreamhostPligorAuth.user,
        toEmail = DreamhostPligorAuth.user, //self-send
        subject = "testing html message with java mail library",
        body = htmlString,
        auth = Some(DreamhostPligorAuth),
        filepath = sampleImage
      ) must beTrue;*/

      success;
    }
    "send an email with one embedded image" in {
      val htmlFunc = (contentId: String) => {
        """<html>
          |<head><meta http-equiv="content-type" content="text/html; charset=utf-8"></head>
          |<body>
          |<div><img alt="alternative text in case image is not found" src="cid:""".stripMargin +
          contentId + "\"" +
          """></div>
            |<div>I must see an embedded image above this line</div>
            |</body>
            |</html>
          """.stripMargin;
      }

      /*sendHtmlMailWithEmbeddedImage(
        smtpServer = DreamhostPligor.smtpServer,
        fromEmail = DreamhostPligorAuth.user,
        toEmail = DreamhostPligorAuth.user, //self-send
        subject = "html message must have an embedded image in it",
        body = htmlFunc,
        auth = Some(DreamhostPligorAuth),
        imagepath = sampleImage
      ) must beTrue;*/
      success;
    }
  }
}