import java.util.concurrent.TimeUnit._
import org.specs2.mutable.Specification
import play.api.test._
import scala.concurrent.duration.Duration

/**
 * Created by pligor
 */
class DurationTest extends Specification {
  sequential;
  "Duration" should {
    "be divided with other durations of any time unit" in {
      val secs = Duration(10, SECONDS);

      val moresecs = Duration(30, SECONDS);

      moresecs / secs must be equalTo 3.0;

      val twomins = Duration(2, MINUTES);

      twomins / moresecs must be equalTo 4.0;

      success;
    }
  }
}