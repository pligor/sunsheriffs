import components.FixtureLoad
import components.helpers.AnormHelper
import mydata.MyFakeApp
import org.specs2.mutable.Specification
import play.api.db.DB
import play.api.test._
import Helpers._
import anorm._
import anorm.SqlParser._
import components.helpers.AnormHelper._

/**
 * Created by pligor
 */
class FixtureLoadTest extends Specification {
  sequential;
  "Fixture Helper component" should {
    "set the values of a table that contains columns with of the BIT type of MySQL" in {
      implicit val application = MyFakeApp();
      running(application) {
        val count = DB.withConnection {
          implicit connection =>
          //insertInto("User",Map("name"->"otinanai","language"->"aa"));
            FixtureLoad("User");
            FixtureLoad("EmailAccount");
            SQL("SELECT COUNT(*) FROM EmailAccount").as(scalar[Long].single);
        }

        count must beGreaterThan(0L);

        success;
      }
    }
    "reset a db table with some predefined values found in json format in a js file" in {
      implicit val application = MyFakeApp();
      running(application) {
        val count = DB.withConnection {
          implicit connection =>
          //insertInto("User",Map("name"->"otinanai","language"->"aa"));
            FixtureLoad("User");
            SQL("SELECT COUNT(*) FROM User").as(scalar[Long].single);
        }

        count must beGreaterThan(0L);

        success;
      }
    }
    "apply itself on all files of the fixtures folder if no file is specified" in {
      implicit val application = MyFakeApp();
      running(application) {

        val count = withNoForeignKeys {
          implicit connection =>
            FixtureLoad();

            SQL("SELECT COUNT(*) FROM GeoPosition").as(scalar[Long].single);
        }

        count must beGreaterThan(0L);

        success;
      }
    }
  }
}