import java.util.Date
import models.{Webbox, WebboxDevice}
import mydata.MyFakeApp
import org.specs2.mutable.Specification
import play.api.libs.json._
import play.api.libs.json.JsObject
import play.api.libs.json.JsString
import play.api.test.Helpers._
import play.api.libs.concurrent._

/**
 * Created by pligor
 */
class WebboxDeviceTest extends Specification {
  sequential

  val webbox = new Webbox("", 0, new Date, "http://aliartospark.dyndns.org:8888/rpc")

  "Webbox Device MyModel" should {
    "be able to parse a json object correctly" in {
      val obj: JsObject = JsObject(Seq(
        "key" -> JsString("WRTP2S75:2110433112"),
        "name" -> JsString("Inverter left"),
        "channels" -> JsNull,
        "children" -> JsNull
      ))

      val device = new WebboxDevice(obj, webbox)

      device.key must beEqualTo("WRTP2S75:2110433112")
      device.name must beSome("Inverter left")
      device.channels must beNone
      device.children must beNone

      success
    }
    "retrieve process data channels from webbox and return a sequence of strings" in {
      implicit val application = MyFakeApp()
      running(application) {
        val obj: JsObject = JsObject(Seq(
          "key" -> JsString("WRTP2S75:2110433112"),
          "name" -> JsString("WRTP2S75:2110433112"),
          "channels" -> JsNull,
          "children" -> JsNull
        ))

        val device = new WebboxDevice(obj, webbox)

        val processDataChannels: Seq[String] = device.GetProcessDataChannels().await.get

        //this only works for sensors
        //processDataChannels must have length 6

        processDataChannels must have length 33

        success
      }
    }
    "retrieve its parameter channels from webbox as a sequence of strings" in {
      implicit val application = MyFakeApp()
      running(application) {
        val obj: JsObject = JsObject(Seq(
          "key" -> JsString("WRTP2S75:2110433112"),
          "name" -> JsString("WRTP2S75:2110433112"),
          "channels" -> JsNull,
          "children" -> JsNull
        ))

        val device = new WebboxDevice(obj, webbox)

        val parameterChannels: Seq[String] = device.GetParameterChannels.await.get

        //this only works for sensors
        //parameterChannels must have length 11

        parameterChannels must have length 20

        /*parameterChannels must contain(
          "DevNam",
          "DevRs",
          "ExlSolIrrCal",
          "ExlSolIrrFnc",
          "FwVer",
          "HwVer",
          "RS485Dl", //if this is not included then there is something wrong with the password
          "SmaNetBd",
          "SN",
          "TmpUnit",
          "WindUnit"
        )*/

        success
      }
    }
  }
}