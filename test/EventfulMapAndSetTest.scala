import akka.actor.Cancellable
import collection.mutable
import components.{EventfulSet, EventfulMap}
import org.specs2.mutable.Specification

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
class EventfulMapAndSetTest extends Specification {
  sequential;
  "EventfulSet" should {
    "be empty when initialized" in {

      val myset = new mutable.HashSet[Int] with EventfulSet[Int] {
        val onAdded = (a: Int) => {}
        val onRemoved = (a: Int) => {}
        val onBeforeClear = (a: EventfulSet[Int]) => false;
      }

      myset must beEmpty;

      success;
    }
    "have events when adding or removing or clearing" in {
      var counter = 0;

      val myset = new mutable.HashSet[Int] with EventfulSet[Int] {
        val onAdded = (a: Int) => {
          counter += 2;
        }
        val onRemoved = (a: Int) => {
          counter -= 3;
        }
        val onBeforeClear = (a: EventfulSet[Int]) => counter < 0;
      }

      myset += 481;
      myset += -155;

      myset -= 481;
      myset.clear();

      myset must not beEmpty;

      myset -= 481;
      myset.clear();

      myset must beEmpty;

      success;
    }
  }

  "EventfulMap" should {
    "be empty when initialized" in {

      val mymap = new mutable.HashMap[Long, Cancellable] with EventfulMap[Long, Cancellable] {
        val onAdded = (a: Long, b: Cancellable) => {}
        val onRemoved = (a: Long) => {}
      }

      mymap must beEmpty;

      success;
    }
    "have events executed when adding or removing" in {
      var counter = 0;

      val mymap = new mutable.HashMap[Long, String] with EventfulMap[Long, String] {
        val onAdded = (a: Long, b: String) => {
          counter += 2;
        }
        val onRemoved = (a: Long) => {
          counter -= 1;
        }
      }

      mymap += 10L -> null;

      mymap += 99L -> null;

      mymap -= 10L;

      counter must be equalTo (2 + 2 - 1);

      success;
    }
  }
}