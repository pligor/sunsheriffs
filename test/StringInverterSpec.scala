/*
import components.helpers.FileHelper
import components.helpers.FileHelper._
import io.Source
import java.io.{PrintWriter, File, FileOutputStream}
import java.util.Date
import models.{WebboxDevice, WebboxDeviceDataRequest, Webbox}
import mydata.MyFakeApp
import org.specs2.mutable.Specification
import play.api.test._
import play.api.test.Helpers._

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
class StringInverterSpec extends Specification {
  sequential;
  "A webbox request" should {
    "be able to get the current, voltage and power of three inverters" in {
      implicit val application = MyFakeApp();
      running(application) {
        val serverUrl = "http://aliartospark.dyndns.org:8888/rpc";
        val webbox = new Webbox("", 0, new Date, serverUrl);

        val inverter1serial = "2110457036";
        val inverter2serial = "2110456997";
        val inverter21serial = "2110457309";

        var devices = Seq.empty[WebboxDevice];
        while (try {
          devices = webbox.GetDevices().value.get.get;
          false;
        } catch {
          case e: Exception => {
            Thread.sleep(60 * 1000);
            true
          };
        }) {}

        devices.length must be equalTo 31;

        val inverter1 = devices.find(_.key.contains(inverter1serial));
        val inverter2 = devices.find(_.key.contains(inverter2serial));
        val inverter21 = devices.find(_.key.contains(inverter21serial));

        inverter1 must beSome;
        inverter2 must beSome;
        inverter21 must beSome;

        val BcurrentChannel = "B.Ms.Amp";
        val BvoltageChannel = "B.Ms.Vol";
        val BpowerChannel = "B.Ms.Watt";

        val processDataChannels = Seq(
          BcurrentChannel,
          BvoltageChannel,
          BpowerChannel
        );

        val requests = Seq(
          WebboxDeviceDataRequest(inverter1.get.key, processDataChannels),
          WebboxDeviceDataRequest(inverter2.get.key, processDataChannels),
          WebboxDeviceDataRequest(inverter21.get.key, processDataChannels)
        );

        val keys = Map(
          1 -> inverter1.get.key,
          2 -> inverter2.get.key,
          21 -> inverter21.get.key
        );

        val swappedKeys = keys.map(_.swap);

        val header = "Inverter1;;;;Inverter2;;;;Inverter21" + "\n" +
          "A;V;W;;A;V;W;;A;V;W" + "\n";

        val rawPath = application.path.getCanonicalPath + "/test/raw";
        val csvfile = new File(rawPath + File.separator + "aliartos_1_2_21_B.csv");

        printToFile(csvfile) {
          _.write(header);
        }

        for (counter <- 1 to 1440) {
          Thread.sleep(60 * 1000);
          try {
            val responses = webbox.GetProcessData(requests).value.get.get;

            val inv1response = responses.find(_.deviceKey == keys(1));
            val inv2response = responses.find(_.deviceKey == keys(2));
            val inv21response = responses.find(_.deviceKey == keys(21));

            inv1response must beSome;
            inv2response must beSome;
            inv21response must beSome;

            case class mydata(id: Int, current: String, voltage: String, power: String) {
              override def toString = current + ";" + voltage + ";" + power;
            }

            val data = Seq(inv1response, inv2response, inv21response).map {
              response =>
                mydata(
                  id = swappedKeys(response.get.deviceKey),
                  current = response.get.channels.find(_.meta == BcurrentChannel).get.value,
                  voltage = response.get.channels.find(_.meta == BvoltageChannel).get.value,
                  power = response.get.channels.find(_.meta == BpowerChannel).get.value
                );
            };

            val row = data.find(_.id == 1).get.toString + ";;" +
              data.find(_.id == 2).get.toString + ";;" +
              data.find(_.id == 21).get.toString + "\n";

            append2file(csvfile) {
              _.write(row);
            }

            println(row);
          } catch {
            case e: Exception => None;
          }
        }

        success;
      }
    }
  }
}*/
