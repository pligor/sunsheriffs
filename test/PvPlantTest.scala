import components.FixtureLoad
import components.helpers.AnormHelper._
import java.text.SimpleDateFormat
import models.PvPlant
import mydata.MyFakeApp
import org.specs2.mutable.Specification
import play.api.db.DB
import play.api.test._
import Helpers._
import generic.TimeHelper._

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
class PvPlantTest extends Specification {
  sequential
  "PvPlant" should {
    "get its own user" in {
      implicit val application = MyFakeApp()
      running(application) {

        DB.withConnection {
          implicit connection =>
            val plant = PvPlant.getById(1).get
            val user = plant.getUser.get
            (user.id must not).beNone
        }
        success
      }
    }
    "get its next " in {
      implicit val application = MyFakeApp()
      running(application) {
        withNoForeignKeys {
          implicit connection =>
            FixtureLoad()
        }
        DB.withConnection {
          implicit connection =>
            val plant = PvPlant.getById(1).get

            val date = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse("2012-10-31 19:43")

            val duration = plant.getDurationUntilNextDayBegin(date)

            duration.toHours must be equalTo 9

            duration.toMinutes must be equalTo 548
        }
        success
      }
    }
  }
}