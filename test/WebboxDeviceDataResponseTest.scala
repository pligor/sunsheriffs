import models.WebboxDeviceDataResponse
import org.specs2.mutable.Specification
import play.api.libs.json.{JsArray, JsString, JsObject}
;

/**
 * Created by pligor
 */
class WebboxDeviceDataResponseTest extends Specification {
  sequential;

  "A data response for a webbox device" should {
    "be able to parse the corresponding json" in {
      /*{
        "key":"WRTP2S42:2110281158",
        "channels":[
        {
          "unit":"",
          "meta":"Serial Number",
          "name":"Serial Number",
          "value":"2110281158"
        }
        ]
      }*/

      val obj = JsObject(Seq(
        "key" -> JsString("WRTP2S42:2110281158"),
        "channels" -> JsArray(Seq(
          JsObject(Seq(
            "unit" -> JsString(""),
            "meta" -> JsString("Serial Number"),
            "name" -> JsString("Serial Number"),
            "value" -> JsString("2110281158")
          ))
        ))
      ));

      val dataResponse = new WebboxDeviceDataResponse(obj);

      dataResponse.deviceKey must be equalTo ("WRTP2S42:2110281158");

      val oneAndOnlyChannelForNow = dataResponse.channels(0);
      oneAndOnlyChannelForNow.unit must beSome("");
      oneAndOnlyChannelForNow.meta must be equalTo "Serial Number";
      oneAndOnlyChannelForNow.name must beSome("Serial Number");
      oneAndOnlyChannelForNow.value must be equalTo "2110281158";
    }
  }
}