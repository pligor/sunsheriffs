import akka.util.Timeout
import java.util.concurrent.TimeUnit
import mydata.{MyFakeApp, DummyActor}
import org.specs2.mutable.Specification
import play.api.test._
import Helpers._
import akka.actor._
import akka.pattern.{AskTimeoutException, ask}
import play.api.libs.concurrent._
import components.helpers.AkkaHelper._
import scala.concurrent.duration._

/**
 * Created by pligor
 */
class AkkaTest extends Specification {
  sequential

  implicit val timeout = Timeout(2, SECONDS)

  "Akka" should {
    """not care about the uniqueness of the name if the actor belong to a different level in the hierarchy.
      |Meaning a child can have the same name as the parent
    """.stripMargin in {
      implicit val application = MyFakeApp()
      running(application) {
        val testactor = getOrCreateActor(Akka.system, "testactor", Props[DummyActor])

        //try to create a grandchild with the same name
        val grandchildPromise = (testactor ? "testactor").mapTo[Option[ActorRef]]
        //val grandchild = grandchildPromise.value.get;
        val grandchild = grandchildPromise.await.get
        grandchild must beSome
        grandchild.get.path.toString must be equalTo "akka://application/user/testactor/testactor"

        val gcPromise = (testactor ? "testactor").mapTo[Option[ActorRef]]
        gcPromise.await.get must beNone

        success
      }
    }
    "get or create an actor if it doesn't exist" in {
      implicit val application = MyFakeApp()
      running(application) {
        val name = "paidi"
        val actorRef = getOrCreateActor(Akka.system, name, Props[DummyActor])

        //cannot create a new one
        Akka.system.actorOf(Props[DummyActor], name = name) must throwAn[InvalidActorNameException]

        val actorRefCopy = getOrCreateActor(Akka.system, name, Props[DummyActor])

        actorRefCopy must be equalTo actorRef

        success
      }
    }
    "create a child as a system and then create a grandchild as a child of the current child" in {
      implicit val application = MyFakeApp()
      running(application) {
        val childActor = Akka.system.actorOf(Props[DummyActor], name = "child")

        val grandchildPromise = (childActor ? "createChild").mapTo[ActorRef]
        //val grandchild = grandchildPromise.value.get;
        val grandchild = grandchildPromise.await.get

        grandchild.path.toString must be equalTo "akka://application/user/child/grandchild"

        //let's try and retrieve this same child by path
        val gcPath = Akka.system / "child" / "grandchild"
        val gc: ActorRef = Akka.system.actorFor(gcPath)

        gc must be equalTo grandchild

        //lets try to get the path of the child if we know the path of the parent
        val gcPathCopy = childActor.path / "grandchild"
        val gcCopy = Akka.system.actorFor(gcPathCopy)

        gcCopy must be equalTo gc

        success
      }
    }
    "send a message to a fellow actor then be destroyed and not be retrievable unless recreated" in {
      implicit val application = MyFakeApp()
      running(application) {
        val supermanPath: ActorPath = Akka.system / "superman"
        val superman: ActorRef = Akka.system.actorFor(supermanPath); //"akka://application/user/superman"
        isActorExistant(superman) must beFalse

        val myar = Akka.system.actorOf(Props[DummyActor], name = "superman")

        val myarCopy: ActorRef = Akka.system.actorFor(supermanPath)
        isActorExistant(myarCopy) must beTrue

        val responsePromise = (myar ? "motherfucker").mapTo[String]
        responsePromise.value.get.get must be equalTo "up your ass"

        Thread.sleep(100)

        val answerPromise = (myar ? "good boy").mapTo[String]
        answerPromise.value.get.get must be equalTo "yeah right"

        Akka.system.actorOf(Props[DummyActor], name = "superman") must throwAn[InvalidActorNameException]

        Thread.sleep(100)

        val pongPromise = (myar ? "pingAndStop").mapTo[String]
        pongPromise.value.get.get must be equalTo "pong"

        Thread.sleep(100)

        val pongAgainPromise = (myar ? "pingAndStop").mapTo[String]
        pongAgainPromise.value.get.get must throwAn[AskTimeoutException]

        //myar ! Kill;

        //lets try to recreate the actor which will be successful
        Akka.system.actorOf(Props[DummyActor], name = "superman") must not(throwAn[InvalidActorNameException])

        success
      }
    }
  }
}