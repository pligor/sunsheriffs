import actorMessages._
import actorMessages.CountingDown
import actors.{PowerOffChecker, OverviewLogWorker}
import akka.actor.{ActorSystem, Props}
import akka.testkit._
import akka.util.Timeout
import java.util.Date
import models.Webbox
import mydata.MyFakeApp
import org.specs2.mutable.Specification
import play.api.test._
import Helpers._
import java.util.concurrent.TimeUnit._
import akka.pattern.ask
import play.api.libs.concurrent._
import components.helpers.AkkaHelper._
import scala.concurrent.duration.Duration

/**
 * Created by pligor
 */
class PowerOffCheckerTest extends Specification {
  sequential;
  "PowerOffChecker" should {
    "be created, destroyed and then recreated according to what the measurement says" in {
      implicit val application = MyFakeApp();
      running(application) {
        implicit val timeout = Timeout(2, SECONDS);
        implicit val system: ActorSystem = Akka.system;
        //preparations
        val dummyWebboxId = 123;
        val dummyWebbox = new Webbox(name = "dummyWebbox", serialNum = 0, timeRegistered = new Date, serverUrl = "http://pligor.com", id = Some(dummyWebboxId));

        /*val dummyOverview = getOrCreateActor(system = Akka.system, name = "dummyOverview", props = Props[OverviewLogWorker]);
        val dummyOverviewCopy = getOrCreateActor(system = Akka.system, name = "dummyOverview", props = Props[OverviewLogWorker]);
        dummyOverviewCopy must be equalTo dummyOverview;*/
        val dummyOverview = TestActorRef[OverviewLogWorker](Props[OverviewLogWorker], name = "dummyOverview");
        dummyOverview.path.toString must be equalTo "akka://application/user/dummyOverview";

        //Thread.sleep(1000);

        val powerOffCheckerName = PowerOffChecker.getActorNameByMonitoringSystem(dummyWebbox);
        powerOffCheckerName must be equalTo "poweroffcheckerwebbox" + dummyWebboxId;

        //first time .. so create
        var instantPower = 0;
        val powerOffChecker1 = dummyOverview.underlyingActor.getOrCreatePowerOffChecker(dummyWebbox);
        isActorExistant(powerOffChecker1) must beTrue;
        println(powerOffChecker1.path);
        //Thread.sleep(1000);

        //but now we got again positive power so delete if exists
        instantPower = 32;
        val powerOffChecker2 = dummyOverview.underlyingActor.getPowerOffChecker(dummyWebbox);
        isActorExistant(powerOffChecker2) must beTrue;
        powerOffChecker2 must be equalTo powerOffChecker1;
        (powerOffChecker2 ? TerminateEarly).mapTo[PowerOffCheckerMessage].await.get must be equalTo VerifyingEarlyTermination;
        //Thread.sleep(1000);

        //now we have again power and we attempt to delete a non existant
        instantPower = 11;
        val powerOffChecker3 = dummyOverview.underlyingActor.getPowerOffChecker(dummyWebbox);
        isActorExistant(powerOffChecker3) must beFalse;

        //then we have instant power zero again and we must recreate
        instantPower = 0;
        val powerOffChecker4 = dummyOverview.underlyingActor.getOrCreatePowerOffChecker(dummyWebbox);
        val promise = (powerOffChecker4 ? ZeroPowerRecorded).mapTo[PowerOffCheckerMessage];
        promise.await.get must beAnInstanceOf[CountingDown]; //if this fails means you have changed seconds available in PowerOffChecker object

        success;
      }
    }
    "respond with the correct message on each tick" in {
      implicit val application = MyFakeApp();
      running(application) {

        implicit val timeout = Timeout(2, SECONDS);

        val powerOffChecker = Akka.system.actorOf(Props(new PowerOffChecker(Duration(30, MINUTES))), name = "PowerOffChecker_Webbox_0");

        var response: PowerOffCheckerMessage = CountingDown(-1);
        while (response != PowerOffLongEnough) {
          response must beAnInstanceOf[CountingDown];
          response = (powerOffChecker ? ZeroPowerRecorded).mapTo[PowerOffCheckerMessage].await.get;
          Thread.sleep(10);
        }

        response must be equalTo PowerOffLongEnough;

        success;
      }
    }
  }
}