import components._
import java.io.File
import jodd.io.FileUtil
import jodd.mail.att.ByteArrayAttachment
import org.specs2.mutable.Specification;
import jodd.mail.Email
import jodd.mail.EmailAttachment.attachment

/**
 * Created by pligor
 *
 * addText IS ALWAYS NEEDED, EVEN FOR SIMPLE EMAILS WITH PLAIN HTML
 * also the usage of alt="" and if the text is the same as the text inside the html help to avoid spamness
 */
class JoddTest extends Specification {
  sequential;
  val sampleJpeg = "/home/pligor/playProjects/workspace/sunsheriffs/public/images/dummies/sample_image_chart1.jpeg";

  "Jodd" should {
    "send an email with an embedded image and a linked image" in {
      sampleJpeg must beAnExistingPath;

      val imageFile = new File(sampleJpeg);
      val imageFilename = imageFile.getName;

      val contentId = imageFilename;

      val htmlString =
        """<html>
          |<head><meta content="text/html; charset=UTF-8" http-equiv="Content-Type"></head>
          |<body>
          |<div><img alt="" src="http://afhimelfarb.files.wordpress.com/2011/02/window.jpg"/></div>
          |<div><img alt="" src="cid:""".stripMargin +
          contentId + "\"" +
          """/></div>
            |<div>We should see the embedded image above this line</div>
            |</body>
            |</html>
          """.stripMargin;

      val email = Email.create().
        from(DreamhostPligorAuth.user).
        to(DreamhostPligorAuth.user).
        subject("email με ενσωματωμένη and linked image").
        //addText("dont know if this is necessary or not").
        addHtml(htmlString);

      email.embed(attachment().setInline(true).file(imageFile));

      //sendSingleMail(email, DreamhostPligor.getSmtpServer(Some(DreamhostPligorAuth)));

      success;
    }
    "send an email with an attached jpg image of which the content type is image/jpeg" in {
      //val contentId = Hash.genUUID;
      val imageFile = new File(sampleJpeg);
      val imageFilename = imageFile.getName;

      val contentId = imageFilename;

      val htmlString =
        """<html>
          |<head><meta content="text/html; charset=UTF-8" http-equiv="Content-Type"></head>
          |<body>
          |<div>some random html body</div>
          |</body>
          |</html>
        """.stripMargin;

      val email = Email.create().
        from(DreamhostPligorAuth.user).
        to(DreamhostPligorAuth.user).
        subject("email with attached image").
        //addText("dont know if this is necessary or not").
        addHtml(htmlString);

      email.attach(new ByteArrayAttachment(FileUtil.readBytes(sampleJpeg), "image/jpeg", imageFilename, contentId));

      //sendSingleMail(email, DreamhostPligor.getSmtpServer(Some(DreamhostPligorAuth)));

      success;
    }
    /*"receive from pop the message that just sent to itself" in {
      Thread.sleep(10000);

      val popServer = DreamhostPligor.getPopServer(Some(DreamhostPligorAuth));
      val session: ReceiveMailSession = popServer.createSession();

      try {
        session.open();

        session.getMessageCount must beGreaterThan(0);
        //session.getNewMessageCount must beGreaterThan(0); //there is a known issue here

        val emailArray = session.receiveEmail(true);
        if (emailArray != null) {
          val emails = emailArray.toList;
          val recEmail = emails.reverse.head;

          recEmail.getFrom must be equalTo "testing@pligor.com";
          recEmail.getTo.toList.head must be equalTo "testing@pligor.com";
          recEmail.getSubject must be equalTo "boring subject";
          recEmail.getAllMessages.toList must have length 2;
          recEmail.getAttachments must beNull;
        }
      } finally {
        session.close();
      }

      success;
    }*/
  }
}