import anorm._
import anorm.SqlParser._
import com.google.i18n.phonenumbers.PhoneNumberUtil
import generic.FileHelper
import FileHelper._
import components.FixtureLoad
import components.helpers.AnormHelper
import generic.TimeHelper._
import java.text.SimpleDateFormat
import java.util.{TimeZone, Date}
import models.{DailyReport, User}
import mydata.MyFakeApp
import org.specs2.mutable.Specification
import play.api.db.DB
import play.api.libs.json.{JsNumber, JsObject, Json}
import play.api.Play._
import play.api.test.Helpers._
import AnormHelper._

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
class UserTest extends Specification {
  sequential;
  "User model" should {
    "be able to delete the daily data of all of its pv plants" in {
      implicit val application = MyFakeApp();
      running(application) {
        withNoForeignKeys {
          implicit connection =>
            FixtureLoad();
        }

        DB.withConnection {
          implicit connection =>
            val user = User.getById(1).get;

            def countOverviews() = {
              SQL("SELECT COUNT(*) FROM WebboxPlantOverview").as(scalar[Long].single);
            }

            countOverviews() must beGreaterThan(0L);

            val date = (new SimpleDateFormat("yyyy-MM-dd HH:mm")).parse("2012-10-31 19:43");

            user.deleteAllPlantsDailyData(date);

            countOverviews() must be equalTo 0;
        }

        success;
      }
    }
    "get its cellphone" in {
      implicit val application = MyFakeApp();
      running(application) {
        withNoForeignKeys {
          implicit connection =>
            FixtureLoad();
        }

        val user = DB.withConnection {
          implicit connnection =>
            User.getById(1).get;
        }
        user.getCellphone.getCountryCode mustEqual PhoneNumberUtil.getInstance().parse("+306956775925", "").getCountryCode

        success;
      }
    }
    "always have an email account as a convention (accounts, fb, google etc. are optional)" in {
      implicit val application = MyFakeApp()
      running(application) {
        withNoForeignKeys {
          implicit connection =>
            FixtureLoad()
        }

        DB.withConnection {
          implicit connnection =>
            val user1 = User.getById(1).get
            user1.getEmailAccount.user_id must be equalTo 1

            val user2 = User.getById(2).get
            user2.getEmailAccount.user_id must be equalTo 2
        }

        success
      }
    }
    "get the date of today and the time of now" in {
      implicit val application = MyFakeApp();
      running(application) {
        withNoForeignKeys {
          implicit connection =>
            FixtureLoad();
        }

        val user = DB.withConnection {
          implicit connection =>
            User.getById(2).get;
        }

        val now = new Date;

        val time = user.getTimeNow(now);

        val sdf = new SimpleDateFormat(DailyReport.timePattern, user.getLocale)
        sdf.setTimeZone(TimeZone.getTimeZone("Europe/Athens"))
        val time2 = sdf.format(convGMTdate2timezone(now, ))

        time must be equalTo time2;

        success;
      }
    }
    "get all its pv plants" in {
      implicit val application = MyFakeApp();
      running(application) {
        withNoForeignKeys {
          implicit connection =>
            FixtureLoad();
        }

        val plants = DB.withConnection {
          implicit connection =>
            User.getById(1).get.getPvPlants;
        }

        val fileContents = getFileContents(current.path.getCanonicalPath + "/" + FixtureLoad.fixturesPath + "/" + "PvPlant" + "." + FixtureLoad.fixtureExtension);

        val json = Json.parse(fileContents) match {
          case x: JsObject => x;
          case _ => throw new ClassCastException;
        }

        val hisKeys = json.keys.filter(key => (json \ key \ "User_id") == JsNumber(1));

        plants must have length hisKeys.size;

        success;
      }
    }
    "compare itself with another User model and be considered equal only if they have same ids" in {
      implicit val application = MyFakeApp();
      running(application) {
        //withNoForeignKeys("test") {
        withNoForeignKeys {
          implicit connection =>
            FixtureLoad();
        }

        DB.withConnection {
          implicit connection =>
            val user = User.getById(1).get;
            user.timeCreated must beAnInstanceOf[Date];

            (user == user) must beTrue;

            val user2 = User.getById(2).get;

            (user == user2) must beFalse;

            val dummyUser = new User(
              id = Some(2),
              name = "",
              language = "",
              rating = 0,
              timeCreated = new Date,
              timeVisited = None,
              timezone = "Europe/Athens",
              cookieKey = None,
              cellphone = 0
            );
            (dummyUser == user2) must beTrue;

            (user == dummyUser) must beFalse;
        }
        success;
      }
    }
  }
}