import com.ning.http.client.Realm
import components.MyLogger
import generic.FileHelper
import java.io.File
import org.apache.commons.codec.binary.Base64
import org.specs2.mutable.Specification
import play.api.libs.json.{JsArray, Json}
import play.api.libs.ws.WS
import play.api.libs.ws.WS.WSRequestHolder
import play.api.Logger
import play.api.test._
import Helpers._
import scala.io.Codec
import scala.collection.JavaConverters._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Success}
import scala.concurrent._
import FileHelper._


/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
class BingTest extends Specification /*with MyFakeApplication*/ {
  sequential

  val body =
    """{
      |  "d": {
      |    "results": [{
      |      "__metadata": {
      |        "uri": "https://api.datamarket.azure.com/Data.ashx/Bing/Search/Image?Query=\u0027vodafone\u0027&$skip=0&$top=1",
      |        "type": "ImageResult"
      |      },
      |      "ID": "5b39ff97-4754-4671-865e-5fa5da2f3571",
      |      "Title": "Vodafone to enter fixed-line market with NBN plans - Broadband News by ...",
      |      "MediaUrl": "http://www.comparebroadband.com.au/images/Pictures/Picture_202.jpg",
      |      "SourceUrl": "http://www.comparebroadband.com.au/article_1395_Vodafone-to-enter-fixed-line-market-with-NBN-plans.htm",
      |      "DisplayUrl": "www.comparebroadband.com.au/article_1395_Vodafone-to-enter-fixed...",
      |      "Width": "1280",
      |      "Height": "800",
      |      "FileSize": "110790",
      |      "ContentType": "image/jpeg",
      |      "Thumbnail": {
      |        "__metadata": {
      |          "type": "Bing.Thumbnail"
      |        },
      |        "MediaUrl": "http://ts3.mm.bing.net/th?id=H.4704059827291670&pid=15.1",
      |        "ContentType": "image/jpg",
      |        "Width": "300",
      |        "Height": "187",
      |        "FileSize": "8212"
      |      }
      |    }, {
      |      "__metadata": {
      |        "uri": "https://api.datamarket.azure.com/Data.ashx/Bing/Search/Image?Query=\u0027vodafone\u0027&$skip=1&$top=1",
      |        "type": "ImageResult"
      |      },
      |      "ID": "a3ba51a5-a2f1-41e4-8d1f-e4abdbd2621f",
      |      "Title": "Vodafone Beats Orange & T-Mobile The Buz Fairy",
      |      "MediaUrl": "http://buzfairy.com/wp-content/uploads/2011/10/Vodafone.jpg",
      |      "SourceUrl": "http://buzfairy.com/2011/10/21/vodafone-beats-orange-t-mobile/",
      |      "DisplayUrl": "buzfairy.com/2011/10/21/vodafone-beats-orange-t-mobile",
      |      "Width": "1920",
      |      "Height": "1200",
      |      "FileSize": "329540",
      |      "ContentType": "image/jpeg",
      |      "Thumbnail": {
      |        "__metadata": {
      |          "type": "Bing.Thumbnail"
      |        },
      |        "MediaUrl": "http://ts3.mm.bing.net/th?id=H.4891127105127958&pid=15.1",
      |        "ContentType": "image/jpg",
      |        "Width": "300",
      |        "Height": "187",
      |        "FileSize": "6869"
      |      }
      |    }],
      |    "__next": "https://api.datamarket.azure.com/Data.ashx/Bing/Search/Image?Query=\u0027vodafone\u0027&$skip=2&$top=2"
      |  }
      |}""".stripMargin


  //http://stackoverflow.com/questions/12648847/how-to-use-bing-search-api-in-java

  "Bing" should {
    "search for images" in {

      //credential when asked from a web browser is this:
      //username: account key
      //password: account key (yes exactly the same as above)

      /*val targetString = "This is an encoded string"
      val base64string = "VGhpcyBpcyBhbiBlbmNvZGVkIHN0cmluZw=="*/
      val base64string = "NmRQTW9JY1FpWGo1T1cwMjVqN2N0ZXpsVm94NjBLdjZ0N0Y4bXlXZjd3VT06NmRQTW9JY1FpWGo1T1cwMjVqN2N0ZXpsVm94NjBLdjZ0N0Y4bXlXZjd3VT0="

      //val credential = Base64.encodeBase64String((accountKey + ":" + accountKey).getBytes(Codec.UTF8.toString()))
      //credential must be equalTo base64string

      /*val consumer = ODataConsumers.newBuilder(rootUri).
        setClientBehaviors(OClientBehaviors.basicAuth(accountKey, accountKey)).
        build()

      //val request =
      val collection = consumer.getEntities(BingSearchApiEntities.Image.toString).

      println(s"collection size: ${collection.size}")
      println(s"collection has definite: ${collection.hasDefiniteSize}")

      collection foreach {
        entitySetInfo =>
          println(s"href: ${entitySetInfo.getHref}")
      }

      println(s"service root uri ${consumer.getServiceRootUri}")*/

      //"https://api.datamarket.azure.com/Data.ashx/Bing/Search/Web?Query=%27Datamarket%27&$top=10&$format=json"

      success
    }
    "extract the sizes of every image from the json response" in {

      /*BingSearchApiHelper.parseJsonWithImages(body).map {
        bingImages =>
        /*val bingImages = Seq(
          BingImage("a", "", "34", "395"),
          BingImage("b", "", "800", "600"),
          BingImage("c", "", "34", "484"),
          BingImage("d", "", "358", "395"),
          BingImage("e", "", "1920", "1000"),
          BingImage("f", "", "1921", "1920"),
          BingImage("g", "", "50", "50")
        )*/
        //g, f, d, b, e, a ,c

        /*println(
          bingImages.map(b => (b.ID, b.getRelativeAspectRatio))
        )*/

          val bingImageMostSquareLike = bingImages.maxBy(_.getRelativeAspectRatio)

          val bingImageAltered = bingImageMostSquareLike.copy(ID = "aaa")



          println(bingImageMostSquareLike)
      }*/

      success
    }
  }

}