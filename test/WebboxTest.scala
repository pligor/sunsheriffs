import anorm._
import anorm.SqlParser._
import collection.mutable.ArrayBuffer
import components.FixtureLoad
import java.text.SimpleDateFormat
import java.util.Date
import models.{WebboxPlantOverviewModel, WebboxPlantOverview, WebboxDevice, Webbox}
import mydata.MyFakeApp
import org.specs2.mutable.Specification
import play.api.db.DB
import play.api.test._
import Helpers._
import components.helpers.AnormHelper._

/**
 * Created by pligor
 */
class WebboxTest extends Specification {
  sequential;
  "Webbox model" should {
    "be able to retrieve get its total energy" in {
      implicit val application = MyFakeApp();
      running(application) {
        withNoForeignKeys {
          implicit connection =>
            FixtureLoad();
        }

        DB.withConnection {
          implicit connection =>
            val webbox = Webbox.getById(1).get;
            val totalEnergy = webbox.getTotalEnergy;
            totalEnergy must beGreaterThan(BigDecimal(0));
          //println(totalEnergy);
        }

        success;
      }
    }
    "insert a new webbox plant overview model as its child" in {
      implicit val application = MyFakeApp();
      running(application) {
        withNoForeignKeys {
          implicit connection =>
            FixtureLoad();
        }

        val model = new WebboxPlantOverviewModel(new Date, WebboxPlantOverview.invalidOverview);

        DB.withConnection {
          implicit connection =>

            val webbox = Webbox.getById(1).get;

            def countOverviews() = {
              SQL("SELECT COUNT(*) FROM WebboxPlantOverview").as(scalar[Long].single);
            }

            val before = countOverviews();

            val newidOption = webbox.insertNewWebboxPlantOverviewModel(model);

            val after = countOverviews();

            after must be equalTo before + 1;

            newidOption must beSome;
        }

        success;
      }
    }
    "create a chart with the daily data available" in {
      implicit val application = MyFakeApp();
      running(application) {
        withNoForeignKeys {
          implicit connection =>
            FixtureLoad();
        }

        DB.withConnection {
          implicit connection =>
            val webbox = Webbox.getById(1).get;

            val unpureDate = (new SimpleDateFormat("yyyy-MM-dd")).parse("2012-10-31");

            val (powerSeries, incomeSeries) = webbox.getDailyPowerAndIncomeSeries(unpureDate);

            powerSeries.getItemCount must be equalTo 15;
            incomeSeries.getItemCount must be equalTo 15;
        }

        success;
      }
    }
    "get its daily energy" in {
      implicit val application = MyFakeApp();
      running(application) {
        withNoForeignKeys {
          implicit connection =>
            FixtureLoad();
        }
        DB.withConnection {
          implicit connection =>

            val webbox = Webbox.getById(1).get;

            val aDate = (new SimpleDateFormat("yyyy-MM-dd")).parse("2012-10-31");

            webbox.getDailyEnergy(aDate) must be equalTo BigDecimal(582.436);
        }
        success;
      }
    }
    "be able to retrieve one of its devices from the database" in {
      implicit val application = MyFakeApp();
      running(application) {
        withNoForeignKeys {
          implicit connection =>
            FixtureLoad();
        }
        DB.withConnection {
          implicit connection =>

            val webbox = Webbox.getById(3).get;
            webbox.getDeviceByKey("WRTP2S42:2110304299").get must beAnInstanceOf[WebboxDevice];
        }
        success;
      }
    }
  }
  "Webbox object" should {
    "retrieve all the webboxes and check the equality among them" in {
      implicit val application = MyFakeApp();
      running(application) {
        withNoForeignKeys {
          implicit connection =>
            FixtureLoad();
        }
        DB.withConnection {
          implicit connection =>

            val webboxes = Webbox.getAll;

            val len = webboxes.length;

            val webbox = Webbox.getById(1).get;
            webbox must be equalTo webboxes.head;

            val wbs = ArrayBuffer(webboxes: _*);

            wbs -= webbox;

            wbs -= null; //it is cool to remove a null value, if does not exist

            wbs(0).id must beSome(2);
            wbs must have length (len - 1);

            wbs += null;
            wbs must have length len; //now the null value got added
            wbs -= null;
            wbs must have length (len - 1);


            val webbox2rem = wbs.find(webbox => (webbox.id.isDefined) && (webbox.id.get == 2)).getOrElse(null);
            wbs -= webbox2rem;
            wbs must have length (len - 2);
        }
        success;
      }
    }
  }
}