import com.thoughtworks.xstream.XStream
import generic.FileHelper
import java.io.{PrintWriter, File}
import jodd.mail.ReceivedEmail
import org.specs2.mutable.Specification
import play.api.test.FakeApplication
import play.api.test.Helpers._
import xml.{Elem, XML}
import FileHelper._

/**
 * Created by pligor
 */
class XstreamTest extends Specification {
  sequential;

  /**
   * XSTREAM ISSUES:
   * http://old.nabble.com/problem-with-marshalling-an-inner-class-td17115937.html
   * http://blog.marcuskwan.com/2011/01/xstream-cannot-marshal-xstream-instance.html
   */
  /*class TestClass(var point: Int, hello: String = "world") {
    def amethod() {
      println("kati");
    }
  }*/
  //val obj = new TestClass(23);

  "Xstream" should {
    val email = new ReceivedEmail;
    email.setFrom("from@from.com");
    email.setTo("to@to.com", "to2@to.com");
    email.setSubject("the subject");
    email.addMessage("body of the email", "mime/type");
    email.addMessage("second body", "type/mime");

    val xstream = new XStream();
    //xstream.alias("testclass", TestClass.class);

    var xmlString: String = null;

    var xmlLiteral: Elem = null;

    val test = "be able to serialize a received email object" in {
      xmlString = xstream.toXML(email);

      //converting to xmlLiteral to verify xml validity
      xmlLiteral = XML.loadString(xmlString);

      (xmlLiteral \ "from").text must be equalTo "from@from.com";

      //println(xmlString);

      success;
    }
    "create a real object with same values as the one previously serialized" in {
      val myXmlStr = xmlLiteral.toString();

      //println(xmlString);

      val myObj = xstream.fromXML(xmlString) match {
        case x: ReceivedEmail => x;
        case _ => throw new ClassCastException("Xstream library failed to do its job");
      };

      myObj.getSubject must be equalTo "the subject";

      success;
    }
    "save in a file, for example in the directory private/emails the xml string of an email" in {
      val application = FakeApplication();
      running(application) {
        //val playPath = application.path.getAbsolutePath;
        val playPath = application.path.getCanonicalPath;

        val emailsPath = playPath + "/private/emails";
        val filePath = emailsPath + "/emailSample.xml";

        printToFile(new File(filePath)) {
          pw => xstream.toXML(email, pw);
        }

        val myfile = new File(filePath)
        val theObj = xstream.fromXML(myfile) match {
          case x: ReceivedEmail => x;
          case _ => throw new ClassCastException("Xstream library failed to do its job");
        };

        theObj.getFrom must be equalTo "from@from.com";

        myfile.delete() must beTrue;

        success;
      }
    }
  }
}